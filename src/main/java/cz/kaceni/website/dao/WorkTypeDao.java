package cz.kaceni.website.dao;

import cz.devmint.springext.orm.dao.AbstractDao;
import cz.kaceni.website.model.WorkType;

public interface WorkTypeDao extends AbstractDao<WorkType, Long> {}
