package cz.kaceni.website.dao;

import cz.devmint.springext.orm.dao.AbstractDao;
import cz.kaceni.website.model.PriceOfferItem;

public interface PriceOfferItemDao extends AbstractDao<PriceOfferItem, Long> {}
