package cz.kaceni.website.dao;

import cz.devmint.springext.orm.dao.AbstractDao;
import cz.kaceni.website.model.Work;

public interface WorkDao extends AbstractDao<Work, Long> {}
