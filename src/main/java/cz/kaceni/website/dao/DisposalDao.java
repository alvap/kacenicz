package cz.kaceni.website.dao;

import cz.devmint.springext.orm.dao.AbstractDao;
import cz.kaceni.website.model.Disposal;

public interface DisposalDao extends AbstractDao<Disposal, Long> {}
