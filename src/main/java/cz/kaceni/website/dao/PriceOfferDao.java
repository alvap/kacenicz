package cz.kaceni.website.dao;

import cz.devmint.springext.orm.dao.AbstractDao;
import cz.kaceni.website.model.PriceOffer;

public interface PriceOfferDao extends AbstractDao<PriceOffer, Long> {}
