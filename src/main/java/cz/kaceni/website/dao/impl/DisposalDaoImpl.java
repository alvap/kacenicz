package cz.kaceni.website.dao.impl;

import org.springframework.stereotype.Repository;

import cz.devmint.springext.orm.dao.impl.AbstractDaoImpl;
import cz.kaceni.website.dao.DisposalDao;
import cz.kaceni.website.model.Disposal;
 
@Repository public class DisposalDaoImpl extends AbstractDaoImpl<Disposal, Long> implements DisposalDao {}
