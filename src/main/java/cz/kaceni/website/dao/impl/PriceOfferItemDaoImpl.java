package cz.kaceni.website.dao.impl;

import org.springframework.stereotype.Repository;

import cz.devmint.springext.orm.dao.impl.AbstractDaoImpl;
import cz.kaceni.website.dao.PriceOfferItemDao;
import cz.kaceni.website.model.PriceOfferItem;
 
@Repository public class PriceOfferItemDaoImpl extends AbstractDaoImpl<PriceOfferItem, Long> implements PriceOfferItemDao {}
