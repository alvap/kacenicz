package cz.kaceni.website.dao.impl;

import org.springframework.stereotype.Repository;

import cz.devmint.springext.orm.dao.impl.AbstractDaoImpl;
import cz.kaceni.website.dao.WorkDao;
import cz.kaceni.website.model.Work;
 
@Repository public class WorkDaoImpl extends AbstractDaoImpl<Work, Long> implements WorkDao {}
