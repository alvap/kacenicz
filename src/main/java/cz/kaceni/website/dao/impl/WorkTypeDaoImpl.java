package cz.kaceni.website.dao.impl;

import org.springframework.stereotype.Repository;

import cz.devmint.springext.orm.dao.impl.AbstractDaoImpl;
import cz.kaceni.website.dao.WorkTypeDao;
import cz.kaceni.website.model.WorkType;
 
@Repository public class WorkTypeDaoImpl extends AbstractDaoImpl<WorkType, Long> implements WorkTypeDao {}
