package cz.kaceni.website.dao.impl;

import java.util.UUID;

import org.springframework.stereotype.Repository;

import cz.devmint.springext.orm.dao.impl.AbstractDaoImpl;
import cz.kaceni.website.dao.PriceOfferDao;
import cz.kaceni.website.model.PriceOffer;
 
@Repository public class PriceOfferDaoImpl extends AbstractDaoImpl<PriceOffer, Long> implements PriceOfferDao {}
