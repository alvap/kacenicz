package cz.kaceni.website.dao.impl;

import java.util.UUID;

import org.springframework.stereotype.Repository;

import cz.devmint.springext.orm.dao.impl.AbstractDaoImpl;
import cz.kaceni.website.dao.InquiryDao;
import cz.kaceni.website.model.Inquiry;
 
@Repository public class InquiryDaoImpl extends AbstractDaoImpl<Inquiry, UUID> implements InquiryDao {}
