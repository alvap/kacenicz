package cz.kaceni.website.dao.impl;

import org.springframework.stereotype.Repository;

import cz.devmint.springext.orm.dao.impl.AbstractDaoImpl;
import cz.kaceni.website.dao.NewsDao;
import cz.kaceni.website.model.News;
 
@Repository public class NewsDaoImpl extends AbstractDaoImpl<News, Long> implements NewsDao {}
