package cz.kaceni.website.dao;

import java.util.UUID;

import cz.devmint.springext.orm.dao.AbstractDao;
import cz.kaceni.website.model.Inquiry;

public interface InquiryDao extends AbstractDao<Inquiry, UUID> {}
