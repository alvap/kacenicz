package cz.kaceni.website.dao;

import cz.devmint.springext.orm.dao.AbstractDao;
import cz.kaceni.website.model.News;

public interface NewsDao extends AbstractDao<News,Long> {}
