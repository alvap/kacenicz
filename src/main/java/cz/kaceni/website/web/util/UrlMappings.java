package cz.kaceni.website.web.util;

import java.util.UUID;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.devmint.springext.constants.Mapping;
import cz.devmint.springext.ui.Message;

public class UrlMappings {

	public static final String INQUIRY = "/poptavka";
	protected static final String ADMIN_INQUIRY_PREFIX = "/administrace/poptavky";
	public static final String INQUIRY_LIST = ADMIN_INQUIRY_PREFIX + "/prehled";
	public static final String REDIRECT_INQUIRY_LIST = Mapping.REDIRECT_PREFIX + ADMIN_INQUIRY_PREFIX + "/prehled";
	public static final String INQUIRY_DETAIL = ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}";
	public static final String INQUIRY_DETAIL_PRICE_OFFER = ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/cenova-nabidka";
	public static final String REDIRECT_INQUIRY_DETAIL = Mapping.REDIRECT_PREFIX + ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}";
	public static final String REDIRECT_INQUIRY_DETAIL_PRICE_OFFER = Mapping.REDIRECT_PREFIX + ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/cenova-nabidka";
	public static final String INQUIRY_CHANGE_STATE = ADMIN_INQUIRY_PREFIX + "/zmenit-stav-poptavky";
	public static final String INQUIRY_CHANGE_ADMIN_NOTE = ADMIN_INQUIRY_PREFIX + "/upravit-poznamku-k-poptavce/{inquiryId}";
	public static final String INQUIRY_REMOVE = ADMIN_INQUIRY_PREFIX + "/smazat-poptavku/{inquiryId}";
	public static final String INQUIRY_CREATE_PRICE_OFFER_ITEM = ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/nova-polozka-cenove-nabidky";
	public static final String REDIRECT_INQUIRY_PRICE_OFFER_ITEM_FORM = Mapping.REDIRECT_PREFIX + ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/nova-polozka-cenove-nabidky";
	public static final String INQUIRY_UPDATE_PRICE_OFFER_ITEM = ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/uprava-polozky-cenove-nabidky/{priceOfferItemId}";
	public static final String INQUIRY_REMOVE_PRICE_OFFER_ITEM = ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/smazat-polozku-cenove-nabidky/{priceOfferItemId}";
	public static final String INQUIRY_UPDATE_PRICE_OFFER = ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/upravit-cenovou-nabidku/{priceOfferId}";
	public static final String INQUIRY_REMOVE_PRICE_OFFER = ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/smazat-cenovou-nabidku/{priceOfferId}";
	public static final String INQUIRY_PRINT_PRICE_OFFER = ADMIN_INQUIRY_PREFIX + "/detail-poptavky/{inquiryId}/tisk-cenove-nabidky/{priceOfferId}";
	public static final String INQUIRY_UPLOAD = ADMIN_INQUIRY_PREFIX + "/nahrat-cenovou-nabidku";
	public static final String PRICE_OFFER_UPLOAD = ADMIN_INQUIRY_PREFIX + "/nahrat-cenovou-nabidku/{inquiryId}";
	
	public static final String INQUIRY_CREATE = ADMIN_INQUIRY_PREFIX + "/zadat-poptavku";
			
	public static final String PUBLIC_NEWS = "novinky";
	public static final String NEWS_LIST = "/administrace/novinky";
	public static final String REDIRECT_NEWS_LIST = Mapping.REDIRECT_PREFIX + NEWS_LIST;
	public static final String CREATE_NEWS = NEWS_LIST + "/nova-novinka";
	public static final String UPDATE_NEWS = NEWS_LIST + "/upravy-novinky/{newsId}";
	public static final String REMOVE_NEWS = NEWS_LIST + "/smazani-novinky/{newsId}";
	
	public static final String NEWS_LIST_EDUCATION = "/administrace/novinky-vzdelavani";
	public static final String REDIRECT_NEWS_LIST_EDUCATION = Mapping.REDIRECT_PREFIX + NEWS_LIST_EDUCATION;
	public static final String CREATE_NEWS_EDUCATION = NEWS_LIST + "/nova-novinka-vzdelavani";
	public static final String UPDATE_NEWS_EDUCATION = NEWS_LIST + "/upravy-novinky-vzdelavani/{newsId}";
	public static final String REMOVE_NEWS_EDUCATION = NEWS_LIST + "/smazani-novinky-vzdelavani/{newsId}";
	
	public static final String VIEW_NEWS_LIST = "admin/news/newsList";
	public static final String VIEW_NEWS_FORM = "admin/news/newsForm";
	
	public static final String VIEW_INQUIRY_ADMIN_NOTE_FORM = "admin/inquiry/updateAdminNoteForm";
	public static final String VIEW_INQUIRY_LIST= "admin/inquiry/inquiryList";
	public static final String VIEW_INQUIRY_DETAIL= "admin/inquiry/inquiryDetail";
	public static final String VIEW_INQUIRY_DETAIL_PRICE_OFFER = "admin/inquiry/inquiryDetailPriceOffer";
	public static final String VIEW_INQUIRY_PRICE_OFFER_ITEM_FORM = "admin/inquiry/priceOfferItemForm";
	public static final String VIEW_INQUIRY_PRICE_OFFER_FORM = "admin/inquiry/priceOfferForm";
	public static final String VIEW_INQUIRY_PRICE_OFFER_PRINT = "admin/inquiry/printPriceOffer";
	public static final String VIEW_INQUIRY_UPLOAD_FORM = "admin/inquiry/uploadForm";

}
