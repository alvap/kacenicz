package cz.kaceni.website.web.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cz.kaceni.website.web.controller.AdminInquiryController;

public class InquiryPaginationInterceptor extends HandlerInterceptorAdapter {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		if (!(handler instanceof HandlerMethod)) return true;
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		Object handlerBean = handlerMethod.getBean();
		if ( handlerBean instanceof AdminInquiryController) {
			ControllerUtil.getOrCreateSessionBoundBean(InquiryPaginationContext.class, request);
		}
		return true;
	}
	
}
