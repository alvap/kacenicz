package cz.kaceni.website.web.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.WebUtils;



/** 
 * Utility class with various static methods common to more than one controllers. 
 */
public class ControllerUtil {
			
	private static final Logger log =  LoggerFactory.getLogger(ControllerUtil.class);
	
	/** 
	 * Retrives a bean of given class from the session or create new instance.
	 * <p>
	 * Uses convention for attribute name under which is given bean stored:<br>
	 * <code>bean[className]</code> - e.g. <code>beanClientFilter</code> 
	 */
	public static <T> T getOrCreateSessionBoundBean(Class<T> clazz, HttpServletRequest request) {
		
		
		return getSessionBoundBean(clazz, request, generateSessionBeanName(clazz));
	}
	
	/** 
	 * Retrives a bean of given class from the session or create new instance.
	 * <p>
	 * 
	 */
	public static <T> T getSessionBoundBean(Class<T> clazz, HttpServletRequest request, String attributeName) {
		@SuppressWarnings("unchecked")
		T sessionFilter = (T) WebUtils.getSessionAttribute(request, attributeName);
		
		if (sessionFilter == null) {
			try {
				sessionFilter = clazz.newInstance();
				WebUtils.setSessionAttribute(request, attributeName, sessionFilter);
			} catch (Exception e) {
				// should not happen
				log.error("Cannot intantiate session scope bean for class: " + clazz + " and request: " + request, e);
			}
		}
		return sessionFilter;
	}
	
	
	/** 
	 * Stores given bean into the session under attribute name:<br>
	 * <code>[className]</code> - e.g. <code>clientFilter</code>  
	 */
	public static <T> void setSessionBoundBean(T bean, HttpServletRequest request) {
		
		
		setSessionBoundBean(bean, request, generateSessionBeanName(bean.getClass()));
		
	}
	
	/** 
	 * Stores given bean into the session under attribute name.<br>
	 */
	public static <T> void setSessionBoundBean(T bean, HttpServletRequest request, String attributeName) {
		WebUtils.setSessionAttribute(request, attributeName, bean);
		
	}
	
	private static String generateSessionBeanName(Class clazz) {
		
		return StringUtils.uncapitalize(clazz.getSimpleName());
		
	}
	
}
