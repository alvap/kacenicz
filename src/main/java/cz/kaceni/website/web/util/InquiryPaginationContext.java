package cz.kaceni.website.web.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class InquiryPaginationContext extends PageRequest {
	public InquiryPaginationContext() {
		super(1,50,Sort.Direction.DESC,"created");
	}
}
