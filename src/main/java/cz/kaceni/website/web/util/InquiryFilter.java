package cz.kaceni.website.web.util;

import java.io.Serializable;

import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.format.annotation.DateTimeFormat;

import cz.kaceni.website.model.Inquiry;
import cz.kaceni.website.model.enums.InquiryStateEnum;

public class InquiryFilter implements Serializable {

	@Past
	@DateTimeFormat(pattern = "dd.MM.yyyy")
	private DateTime dateFrom;

	@Past
	@DateTimeFormat(pattern = "dd.MM.yyyy")
	private DateTime dateTo;
	
	@Size(max=30)
	private String lastName;

	private InquiryStateEnum inquiryState = InquiryStateEnum.NEW;

	public DetachedCriteria getRestrictions() {

		DetachedCriteria criterions = DetachedCriteria.forClass(Inquiry.class);
		criterions.createAlias("customer", "customer"); // implicit join
		if (inquiryState != null) criterions.add(Restrictions.eq("state", inquiryState));
		if (dateFrom != null) criterions.add(Restrictions.ge("created", dateFrom.toDateTime(DateTimeZone.UTC)));
		if (dateTo != null) criterions.add(Restrictions.le("created", dateTo.plusDays(1).toDateTime(DateTimeZone.UTC))); // including dateTo
		if (lastName != null) criterions.add(Restrictions.like("customer.lastName", lastName, MatchMode.ANYWHERE));
		return criterions;
	}
	
	public void reset() {
		inquiryState = InquiryStateEnum.NEW;
		dateFrom = null;
		dateTo = null;
		lastName = null;
	}

	public DateTime getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(DateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public DateTime getDateTo() {
		return dateTo;
	}

	public void setDateTo(DateTime dateTo) {
		this.dateTo = dateTo;
	}

	public InquiryStateEnum getInquiryState() {
		return inquiryState;
	}

	public void setInquiryState(InquiryStateEnum inquiryState) {
		this.inquiryState = inquiryState;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InquiryFilter [dateFrom=");
		builder.append(dateFrom);
		builder.append(", dateTo=");
		builder.append(dateTo);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", inquiryState=");
		builder.append(inquiryState);
		builder.append("]");
		return builder.toString();
	}

}
