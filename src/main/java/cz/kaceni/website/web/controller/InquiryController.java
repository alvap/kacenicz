package cz.kaceni.website.web.controller;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.groups.Default;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.devmint.springext.ui.Message;
import cz.devmint.springext.validation.web.ValidationGroup;
import cz.devmint.springext.validation.web.ValidatorAdapter;
import cz.kaceni.website.model.Inquiry;
import cz.kaceni.website.model.enums.DisposalEnum;
import cz.kaceni.website.model.enums.TermEnum;
import cz.kaceni.website.model.enums.WorkTypeEnum;
import cz.kaceni.website.model.util.RequiredBillingDataFormChecks;
import cz.kaceni.website.service.InquiryService;
import cz.kaceni.website.service.MailService;
import cz.kaceni.website.web.util.UrlMappings;

@Controller
public class InquiryController {
	
	@Autowired private ValidatorAdapter validator;
	@Autowired private InquiryService inquiryService;
	@Autowired private MailService mailService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
	    binder.setAutoGrowNestedPaths(false);
	}
	
	@RequestMapping(value={"/poptavka", UrlMappings.INQUIRY_CREATE}, method=RequestMethod.GET)
	public ModelAndView inquiryForm(@ModelAttribute Inquiry inquiry, @ValidationGroup({Default.class}) String _inquiry, ModelMap modelMap) {
		// prefill form for authenticated users?
		inquiry.setTerm(TermEnum.NOT_IMPORTANT);
		return new ModelAndView("poptavka", addModelAttributes(modelMap));
		
	}
	
	@RequestMapping(value="/poptavka", method=RequestMethod.POST)
	public ModelAndView inquiryFormPublic(@ModelAttribute @Validated({Default.class}) Inquiry inquiry, BindingResult bindingResult, ModelMap modelMap, RedirectAttributes redirectAttributes) {
		return inquiryForm(inquiry, bindingResult, modelMap, redirectAttributes, true);
	}
	
	@RequestMapping(value=UrlMappings.INQUIRY_CREATE, method=RequestMethod.POST)
	public ModelAndView inquiryFormAdmin(@ModelAttribute @Validated({Default.class}) Inquiry inquiry, BindingResult bindingResult, ModelMap modelMap, RedirectAttributes redirectAttributes) {
		return inquiryForm(inquiry, bindingResult, modelMap, redirectAttributes, false);
	}
	
	
	
	private ModelAndView inquiryForm(@ModelAttribute @Validated({Default.class}) Inquiry inquiry, 
									 BindingResult bindingResult, 
									 ModelMap modelMap, 
									 RedirectAttributes redirectAttributes,
									 boolean isPublic) {
		
		if (inquiry.isRequiresBill()) {
			Set<ConstraintViolation<Object>> requiredBillErrors = validator.validateProperty((Object)inquiry, "customer.billingData.address.name", RequiredBillingDataFormChecks.class);
			requiredBillErrors.addAll(validator.validateProperty((Object)inquiry, "customer.billingData.address.street", RequiredBillingDataFormChecks.class));
			requiredBillErrors.addAll(validator.validateProperty((Object)inquiry, "customer.billingData.address.city", RequiredBillingDataFormChecks.class));
			requiredBillErrors.addAll(validator.validateProperty((Object)inquiry, "customer.billingData.address.zip", RequiredBillingDataFormChecks.class));
			validator.addValidationConstraints(requiredBillErrors, bindingResult);
		}
		if (bindingResult.hasErrors()) {
			addModelAttributes(modelMap);
			return new ModelAndView("poptavka", modelMap);
		} else {
			inquiryService.saveInquiry(inquiry);
			if (isPublic) {
				mailService.notifyNewInquiry(inquiry);
				redirectAttributes.addFlashAttribute(Message.getMessage("Poptávka byla úspěšně odeslána."));
				return new ModelAndView("redirect:/poptavka");
			} // else
			redirectAttributes.addFlashAttribute(Message.getMessage("Poptávka byla úspěšně vytvořena."));
			return new ModelAndView(UrlMappings.REDIRECT_INQUIRY_LIST);
			
		}
	}
	
	private ModelMap addModelAttributes(ModelMap modelMap) {
		modelMap.addAttribute(WorkTypeEnum.classValues());
		modelMap.addAttribute(DisposalEnum.classValues());
		modelMap.addAttribute(TermEnum.values());
		modelMap.addAttribute("randomFormName", RandomStringUtils.random(10));
		return modelMap;
	}
}
