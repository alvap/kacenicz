package cz.kaceni.website.web.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import cz.devmint.springext.constants.Mapping;
import cz.devmint.springext.pagination.SessionPageable;
import cz.devmint.springext.ui.Message;
import cz.devmint.springext.validation.web.ValidatorAdapter;
import cz.kaceni.website.dto.UploadForm;
import cz.kaceni.website.model.Customer;
import cz.kaceni.website.model.Inquiry;
import cz.kaceni.website.model.PriceOffer;
import cz.kaceni.website.model.PriceOfferItem;
import cz.kaceni.website.model.enums.InquiryStateEnum;
import cz.kaceni.website.service.InquiryService;
import cz.kaceni.website.validation.groups.AdminInquiryChecks;
import cz.kaceni.website.web.util.ControllerUtil;
import cz.kaceni.website.web.util.InquiryFilter;
import cz.kaceni.website.web.util.UrlMappings;

@Controller
public class AdminInquiryController {
	
	private static final String INQUIRY_PAGINATION_CONTEXT_ATTR_NAME = "inquiryPaginationContext";
	private static Logger log = LoggerFactory.getLogger(AdminInquiryController.class);
	
	@Autowired private InquiryService inquiryService;
	@Autowired private ValidatorAdapter validator;
	@Autowired private ConversionService conversionService;
	@Autowired private MessageSource messageSource;
		
	/** Displays inquiry list - may filter filtered by criteria and paginated. */
	@RequestMapping(UrlMappings.INQUIRY_LIST)
	public ModelAndView inquiryList(ModelMap modelMap, 
			   				HttpServletRequest request,
			   				@SessionPageable(INQUIRY_PAGINATION_CONTEXT_ATTR_NAME) Pageable pageable) {
		
		addInquiryListReferenceData(modelMap, true, request);
		addInquiryList(modelMap, request, pageable);
		return new ModelAndView(UrlMappings.VIEW_INQUIRY_LIST);
	}
	
	private void addInquiryListReferenceData(ModelMap modelMap, boolean includeFilter, HttpServletRequest request) {
		if (includeFilter) {
			InquiryFilter sessionFilter = ControllerUtil.getOrCreateSessionBoundBean(InquiryFilter.class, request);
			modelMap.addAttribute(sessionFilter);
			
		}
		modelMap.addAttribute(InquiryStateEnum.values());
		modelMap.addAttribute(new Inquiry()); // id wrapper
		
	}
	
	private void addInquiryList(ModelMap modelMap, HttpServletRequest request, Pageable pageable) {
		modelMap.addAttribute("inquiryListPage", inquiryService.getInquiryList(ControllerUtil.getOrCreateSessionBoundBean(InquiryFilter.class, request), pageable));
	}

	
	/** Filtering on inquiry list. */
	@RequestMapping(value=UrlMappings.INQUIRY_LIST, method=RequestMethod.POST, params="filter")
	public ModelAndView inquiryList(@ModelAttribute @Valid InquiryFilter inquiryFilter, 
								   BindingResult result, 
								   ModelMap modelMap,
								   HttpServletRequest request, 
								   @SessionPageable(INQUIRY_PAGINATION_CONTEXT_ATTR_NAME) Pageable pageable) {
		
	
		if (result.hasErrors()) {
			addInquiryListReferenceData(modelMap, false, request);
			addInquiryList(modelMap, request, pageable);
			// add explicitly form error message, because we need specific name for it
			modelMap.addAttribute("formErrorMessage", Message.getFormErrorMessage());
			return new ModelAndView(UrlMappings.VIEW_INQUIRY_LIST, modelMap);
		}
		
		ControllerUtil.setSessionBoundBean(inquiryFilter, request);
		return new ModelAndView(UrlMappings.REDIRECT_INQUIRY_LIST);
		
	}
	
	/** Reset filter in inquiry list. */
	@RequestMapping(value=UrlMappings.INQUIRY_LIST, method=RequestMethod.POST, params="resetFilter")
	public ModelAndView inquiryList(HttpServletRequest request) {
		ControllerUtil.getOrCreateSessionBoundBean(InquiryFilter.class, request).reset();
		return new ModelAndView(UrlMappings.REDIRECT_INQUIRY_LIST);
	}
	
	@RequestMapping(value=UrlMappings.INQUIRY_CHANGE_ADMIN_NOTE, method=RequestMethod.GET)
	public ModelAndView inquiryAdminNoteUpdate(@PathVariable UUID inquiryId, ModelMap modelMap) {
		Inquiry inquiry = inquiryService.findInquiryWithoutWorkList(inquiryId);
		modelMap.addAttribute(inquiry);
		return new ModelAndView(UrlMappings.VIEW_INQUIRY_ADMIN_NOTE_FORM, modelMap);
	}
	
	@RequestMapping(value=UrlMappings.INQUIRY_CHANGE_ADMIN_NOTE, method=RequestMethod.POST, params=Mapping.PARAM_SAVE)
	public String inquiryAdminNoteUpdate(@Validated(AdminInquiryChecks.class) Inquiry inquiry, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return UrlMappings.VIEW_INQUIRY_ADMIN_NOTE_FORM;
		}
		inquiryService.updateInquiryAdminNote(inquiry.getId(), inquiry.getAdminNote());
		redirectAttributes.addFlashAttribute(Message.getMessage("Poznámka byla úspěšně upravena."));
		return UrlMappings.REDIRECT_INQUIRY_LIST;
	}
	
	@RequestMapping(value=UrlMappings.INQUIRY_CHANGE_ADMIN_NOTE, method=RequestMethod.POST, params=Mapping.PARAM_CANCEL)
	public String inquiryAdminNoteUpdate() {
		return UrlMappings.REDIRECT_INQUIRY_LIST;
	}
	
	@RequestMapping(value=UrlMappings.INQUIRY_CHANGE_STATE, method=RequestMethod.POST)
	public String inquiryChangeState(@RequestParam(required=true) UUID inquiryId, @RequestParam(required=true) InquiryStateEnum inquiryState, RedirectAttributes redirectAttributes) {
		inquiryService.changeInquiryState(inquiryId, inquiryState);
		redirectAttributes.addFlashAttribute(Message.getMessage("Stav poptávky byl úspěšně změněn."));
	    return UrlMappings.REDIRECT_INQUIRY_LIST;
	}
	
	@RequestMapping(UrlMappings.INQUIRY_REMOVE)
	public String removeInquiry(@PathVariable UUID inquiryId,RedirectAttributes redirectAttributes) {
		inquiryService.removeInquiry(inquiryId);
		redirectAttributes.addFlashAttribute(Message.getMessage("Poptávka byla úspěšně smazána."));
	    return UrlMappings.REDIRECT_INQUIRY_LIST;
	}
			
	@RequestMapping(UrlMappings.INQUIRY_DETAIL)
	public ModelAndView inquiryDetail(@PathVariable UUID inquiryId, ModelMap model) {
		Inquiry inquiry = inquiryService.findInquiry(inquiryId);
		model.addAttribute(inquiry);
		return new ModelAndView(UrlMappings.VIEW_INQUIRY_DETAIL, model);
	}
	
	@RequestMapping(UrlMappings.INQUIRY_DETAIL_PRICE_OFFER)
	public ModelAndView inquiryDetailPriceOffer(@PathVariable UUID inquiryId, ModelMap model) {
		PriceOffer priceOffer = inquiryService.findInquiryPriceOffer(inquiryId);
		if (priceOffer != null) model.addAttribute(priceOffer);
		model.addAttribute("inquiryId", inquiryId);
		return new ModelAndView(UrlMappings.VIEW_INQUIRY_DETAIL_PRICE_OFFER, model);
	}
		
	@RequestMapping(value=UrlMappings.INQUIRY_CREATE_PRICE_OFFER_ITEM, method=RequestMethod.GET)
	public ModelAndView createPriceOfferItem(@PathVariable UUID inquiryId, ModelMap modelMap) {
		PriceOfferItem priceOfferItem = new PriceOfferItem();
		modelMap.addAttribute(priceOfferItem);
		PriceOffer priceOffer = inquiryService.findInquiryPriceOffer(inquiryId);
		if (priceOffer != null) modelMap.addAttribute(priceOffer);
		Inquiry inquiry = inquiryService.findInquiry(inquiryId);
		modelMap.addAttribute(inquiry);
		return new ModelAndView(UrlMappings.VIEW_INQUIRY_PRICE_OFFER_ITEM_FORM, modelMap);
	}
	
	@RequestMapping(value=UrlMappings.INQUIRY_UPDATE_PRICE_OFFER_ITEM, method=RequestMethod.GET)
	public ModelAndView updatePriceOfferItem(@PathVariable UUID inquiryId, @PathVariable Long priceOfferItemId, ModelMap modelMap) {
		PriceOfferItem priceOfferItem = inquiryService.getPriceOfferItem(priceOfferItemId);
		modelMap.addAttribute(priceOfferItem);
		PriceOffer priceOffer = inquiryService.findInquiryPriceOffer(inquiryId);
		modelMap.addAttribute(priceOffer);
		Inquiry inquiry = inquiryService.findInquiry(inquiryId);
		modelMap.addAttribute(inquiry);
		return new ModelAndView(UrlMappings.VIEW_INQUIRY_PRICE_OFFER_ITEM_FORM, modelMap);
	}
	
	@RequestMapping(value={UrlMappings.INQUIRY_CREATE_PRICE_OFFER_ITEM, UrlMappings.INQUIRY_UPDATE_PRICE_OFFER_ITEM}, method=RequestMethod.POST, params="saveAndContinue")
	public String savePriceOfferItemAndContinue(@Valid PriceOfferItem priceOfferItem, BindingResult bindingResult, RedirectAttributes redirectAttributes, @PathVariable UUID inquiryId, ModelMap model) {
		return doSavePriceOfferItem(priceOfferItem, bindingResult, redirectAttributes, inquiryId, true, model);
	}
	
	@RequestMapping(value={UrlMappings.INQUIRY_CREATE_PRICE_OFFER_ITEM, UrlMappings.INQUIRY_UPDATE_PRICE_OFFER_ITEM}, method=RequestMethod.POST, params=Mapping.PARAM_SAVE)
	public String savePriceOfferItem(@Valid PriceOfferItem priceOfferItem, BindingResult bindingResult, RedirectAttributes redirectAttributes, @PathVariable UUID inquiryId, ModelMap model) {
		return doSavePriceOfferItem(priceOfferItem, bindingResult, redirectAttributes, inquiryId, false, model);
	}
	
	private String doSavePriceOfferItem(PriceOfferItem priceOfferItem, BindingResult bindingResult, RedirectAttributes redirectAttributes, UUID inquiryId, boolean cont, ModelMap model) {
		if (bindingResult.hasErrors()) {
			PriceOffer priceOffer = inquiryService.findInquiryPriceOffer(inquiryId);
			model.addAttribute(priceOffer);
			Inquiry inquiry = inquiryService.findInquiry(inquiryId);
			model.addAttribute(inquiry);
			return UrlMappings.VIEW_INQUIRY_PRICE_OFFER_ITEM_FORM;
		}
		inquiryService.savePriceOfferItem(priceOfferItem, inquiryId);
		redirectAttributes.addFlashAttribute(Message.getMessage("Položka cenové nabídky byla úspěšně uložena."));
		return cont? redirectToPriceOfferItemForm(inquiryId) : redirectToInquiryDetail(inquiryId);
	}
	
	@RequestMapping(value={UrlMappings.INQUIRY_UPDATE_PRICE_OFFER_ITEM, UrlMappings.INQUIRY_CREATE_PRICE_OFFER_ITEM}, method=RequestMethod.POST, params=Mapping.PARAM_CANCEL)
	public String cancelPriceOfferItemForm(@PathVariable UUID inquiryId) {
		return redirectToInquiryDetail(inquiryId);
	}
	
	@RequestMapping(UrlMappings.INQUIRY_REMOVE_PRICE_OFFER_ITEM)
	public String removePriceOfferItem(@PathVariable UUID inquiryId, @PathVariable Long priceOfferItemId, RedirectAttributes redirectAttributes) {
		inquiryService.removePriceOfferItem(priceOfferItemId);
		redirectAttributes.addFlashAttribute(Message.getMessage("Položka cenové nabídky byla úspěšně smazána."));
	    return redirectToInquiryDetail(inquiryId);
	}
	
		
	@RequestMapping(value=UrlMappings.INQUIRY_UPDATE_PRICE_OFFER, method=RequestMethod.GET)
	public String updatePriceOffer(@PathVariable Long priceOfferId, ModelMap modelMap) {
		PriceOffer priceOffer = inquiryService.getPriceOffer(priceOfferId);
		modelMap.addAttribute(priceOffer);
		return UrlMappings.VIEW_INQUIRY_PRICE_OFFER_FORM;
	}
	
	@RequestMapping(value=UrlMappings.INQUIRY_UPDATE_PRICE_OFFER, method=RequestMethod.POST, params=Mapping.PARAM_SAVE)
	public String updatePriceOffer(@ModelAttribute PriceOffer priceOffer, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) return UrlMappings.VIEW_INQUIRY_PRICE_OFFER_FORM;
		PriceOffer priceOfferMerged = inquiryService.savePriceOffer(priceOffer);
		redirectAttributes.addFlashAttribute(Message.getMessage("Doplňující údaje cenové nabídky byly úšpěšně upraveny."));
		return redirectToInquiryDetail(priceOfferMerged.getInquiry().getId());
	}
	
	@RequestMapping(value=UrlMappings.INQUIRY_UPDATE_PRICE_OFFER, method=RequestMethod.POST, params=Mapping.PARAM_CANCEL)
	public String updatePriceOffer(@PathVariable UUID inquiryId) {
		return redirectToInquiryDetail(inquiryId);
	}
	
	@RequestMapping(UrlMappings.INQUIRY_REMOVE_PRICE_OFFER)
	public String removePriceOffer(@PathVariable Long priceOfferId, @PathVariable UUID inquiryId, RedirectAttributes redirectAttributes) {
		inquiryService.removePriceOffer(priceOfferId);
		redirectAttributes.addFlashAttribute(Message.getMessage("Cenová nabídka byla úspěšně smazána."));
		return redirectToInquiryDetail(inquiryId);
	}
	
	@RequestMapping(UrlMappings.INQUIRY_PRINT_PRICE_OFFER)
	public String printPriceOffer(@PathVariable Long priceOfferId, ModelMap model) {
		model.addAttribute(inquiryService.getPriceOfferForPrint(priceOfferId));
		return UrlMappings.VIEW_INQUIRY_PRICE_OFFER_PRINT;
	}
	
	@RequestMapping(value = UrlMappings.INQUIRY_UPLOAD, method = RequestMethod.GET)
    public String uploadFormInquiry(ModelMap model) {
        UploadForm uploadForm = new UploadForm();
        model.addAttribute(uploadForm);
        return UrlMappings.VIEW_INQUIRY_UPLOAD_FORM;
    }
	
	@RequestMapping(value = UrlMappings.PRICE_OFFER_UPLOAD, method = RequestMethod.GET)
    public String uploadFormPriceOffer(ModelMap model, @PathVariable() UUID inquiryId) {
        UploadForm uploadForm = new UploadForm();
        uploadForm.setInquiryId(inquiryId);
        model.addAttribute(uploadForm);
        return UrlMappings.VIEW_INQUIRY_UPLOAD_FORM;
    }
 
	@RequestMapping(value = {UrlMappings.INQUIRY_UPLOAD, UrlMappings.PRICE_OFFER_UPLOAD}, params=Mapping.PARAM_SAVE)
    public String upload(@ModelAttribute UploadForm uploadForm, ModelMap model, RedirectAttributes redirectAttributes)  {

		MultipartFile multipartFile = uploadForm.getFile();
		if (multipartFile.getSize() == 0) {
			model.addAttribute(Message.getErrorMessage("Není vybrán žádný soubor."));
			return UrlMappings.VIEW_INQUIRY_UPLOAD_FORM;
		}
			
		List<PropertyValue> propertyValueList = null;
		try {
		  propertyValueList = parseCSVFile(multipartFile, uploadForm.getInquiryId() != null);
		} catch (Exception e) {
			log.error("Error parsing csv file.", e);
			model.addAttribute(Message.getErrorMessage("Chyba při načítání souboru: " + e.getMessage()));
			return UrlMappings.VIEW_INQUIRY_UPLOAD_FORM;
		}
				
		// create model objects, bind and validate data
		Inquiry inquiry = new Inquiry();
		if (uploadForm.getInquiryId() != null) {inquiry.setId(uploadForm.getInquiryId());}
		inquiry.setCustomer(new Customer());
		PriceOffer priceOffer = new PriceOffer();
		List<PriceOfferItem> priceOfferItemList = new AutoPopulatingList<PriceOfferItem>(PriceOfferItem.class);
		priceOffer.setPriceOfferItemList(priceOfferItemList);	
		inquiry.setPriceOffer(priceOffer);
		
		// create data binder
		DataBinder dataBinder = new DataBinder(inquiry);
		dataBinder.setConversionService(conversionService);
		dataBinder.setValidator(validator);
		dataBinder.bind(new MutablePropertyValues(propertyValueList));
		dataBinder.validate();
		BindingResult bindingResult = dataBinder.getBindingResult();
		List<String> resolvedErrorMessages = new ArrayList<>();
		if (bindingResult.hasErrors()) {
			for (ObjectError error : bindingResult.getAllErrors())
			 resolvedErrorMessages.add(messageSource.getMessage(error, LocaleContextHolder.getLocale()));
			 model.addAttribute("errorMessages", resolvedErrorMessages);
			 return UrlMappings.VIEW_INQUIRY_UPLOAD_FORM;
		}
		
		if (uploadForm.getInquiryId() != null) {
			inquiryService.importPriceOffer(inquiry);
			redirectAttributes.addFlashAttribute(Message.getMessage("Nahrání cenové nabídky k poptávce proběhlo úspěšně."));
			return redirectToInquiryDetail(uploadForm.getInquiryId());
		} else {
			inquiryService.importInquiry(inquiry);
			redirectAttributes.addFlashAttribute(Message.getMessage("Nahrání poptávky/cenové nabídky proběhlo úspěšně."));
			return UrlMappings.REDIRECT_INQUIRY_LIST;
		}
    }
	
	@RequestMapping(value = UrlMappings.INQUIRY_UPLOAD, method = RequestMethod.POST, params=Mapping.PARAM_CANCEL)
    public String uploadCancel(@ModelAttribute UploadForm uploadForm, ModelMap model) {
		return UrlMappings.REDIRECT_INQUIRY_LIST;
	}
	
	private List<PropertyValue> parseCSVFile(MultipartFile multipartFile, boolean optionalValues) throws IOException	{
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(multipartFile.getInputStream(), StandardCharsets.UTF_8))) {
		//try (BufferedReader br = new BufferedReader(new InputStreamReader(new BOMInputStream(multipartFile.getInputStream()), StandardCharsets.UTF_16LE))) {
			
			final CSVParser parser = new CSVParser(br, CSVFormat.EXCEL.withHeader().withDelimiter('\t'));
			List<PropertyValue> propertyValueList = new ArrayList<PropertyValue>();
			String name = null;
			int counter = 0;
			
			// fill data binder data
			for (CSVRecord record : parser) {
				if (counter == 0 && optionalValues) {
					addOptionalPropertyValue("customer.firstName",record,"Jméno",propertyValueList);
					addOptionalPropertyValue("customer.lastName", record, "Příjmení", propertyValueList);
					addOptionalPropertyValue("customer.phoneNumber",record, "Telefon", propertyValueList);
					addOptionalPropertyValue("customer.userAccount.email", record, "Email", propertyValueList);
					addOptionalPropertyValue("message", record, "Zpráva", propertyValueList);
				} else if (counter == 0) {
					addPropertyValue("customer.firstName",record,"Jméno",propertyValueList);
					addPropertyValue("customer.lastName", record, "Příjmení", propertyValueList);
					addPropertyValue("customer.phoneNumber",record, "Telefon", propertyValueList);
					addPropertyValue("customer.userAccount.email", record, "Email", propertyValueList);
					addPropertyValue("message", record, "Zpráva", propertyValueList);
				}
				name = record.get(0);
				if (StringUtils.isBlank(name)) break; // do not parse empty records
				propertyValueList.add(new PropertyValue("priceOffer.priceOfferItemList[" + counter + "].name", record.get(0)));
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].amount", record, "Množství", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].amountUnit", record, "Jednotka množství", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].type", record, "Typ zásahu (Z,B,R)", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].priceChipping", record, "Cena štěpkování", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].priceDisposal", record, "Cena likvidace dřeva", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].priceTrimming", record, "Cena ořezu", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].priceFelling", record, "Cena kácení", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].priceStumpsMilling", record, "Cena frézování pařezu", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].priceBinding", record, "Cena vazby", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].priceOther", record, "Cena ostatní", propertyValueList);
				addPropertyValue("priceOffer.priceOfferItemList[" + counter + "].vatRate", record, "Sazba DPH [%]", propertyValueList);
				counter++;
			}
			
			return propertyValueList;
		}
		
	}
	
	private void addPropertyValue(String property, CSVRecord record, String columnName, List<PropertyValue> propertyValueList) {
		propertyValueList.add(new PropertyValue(property, record.get(columnName)));
	}
	
	private void addOptionalPropertyValue(String property, CSVRecord record, String columnName, List<PropertyValue> propertyValueList) {
		String value = record.get(columnName);
		if (StringUtils.isBlank(value)) {
			value = Inquiry.NOT_SET_FIELD;
		}
		propertyValueList.add(new PropertyValue(property, value));
	}
	
	
	private String redirectToInquiryDetail(UUID inquiryId) {
		return UriComponentsBuilder.fromUriString(UrlMappings.REDIRECT_INQUIRY_DETAIL_PRICE_OFFER)
				.build()
				.expand(inquiryId.toString())
				.toUriString();
	}
	
	private String redirectToPriceOfferItemForm(UUID inquiryId) {
		return UriComponentsBuilder.fromUriString(UrlMappings.REDIRECT_INQUIRY_PRICE_OFFER_ITEM_FORM)
				.build()
				.expand(inquiryId.toString())
				.toUriString();
	}
}
