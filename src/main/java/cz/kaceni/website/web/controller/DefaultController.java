package cz.kaceni.website.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import cz.kaceni.website.web.util.UrlMappings;

@Controller
public class DefaultController {

	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView staticPage(HttpServletRequest request) {
		return ("/".equals(request.getServletPath())) ? new ModelAndView("index") : new ModelAndView();
	}
	
	@RequestMapping("/admin")
	public String admin(HttpServletRequest request) {
		return UrlMappings.REDIRECT_INQUIRY_LIST;
	}
	
}
