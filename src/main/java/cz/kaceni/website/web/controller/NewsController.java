package cz.kaceni.website.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cz.devmint.springext.constants.Mapping;
import cz.devmint.springext.ui.Message;
import cz.kaceni.website.model.News;
import cz.kaceni.website.model.enums.NewsType;
import cz.kaceni.website.service.NewsService;
import cz.kaceni.website.web.util.UrlMappings;

@Controller
public class NewsController {
	
	@Autowired private NewsService newsService;
	
	/** Displays public news list */
	@RequestMapping("/sluzby/skoleni-a-vzdelavani")
	public String publicEducation(ModelMap modelMap) {
		List<News> newsList = newsService.listEducationNews();
		replaceLinks(newsList);
		modelMap.addAttribute(newsList);
		return "sluzby/skoleni-a-vzdelavani";
	}
	
	/** Displays public news list */
	@RequestMapping(UrlMappings.PUBLIC_NEWS)
	public String publicNewsList(ModelMap modelMap) {
		List<News> newsList = newsService.listNews();
		replaceLinks(newsList);
		modelMap.addAttribute(newsList);
		return "novinky";
	}
	
	/** Displays news list */
	@RequestMapping(UrlMappings.NEWS_LIST)
	public String newsList(ModelMap modelMap) {
		List<News> newsList = newsService.listNews();
		replaceLinks(newsList);
		modelMap.addAttribute(newsList);
		return UrlMappings.VIEW_NEWS_LIST;
	}
			
	@RequestMapping(value=UrlMappings.CREATE_NEWS, method=RequestMethod.GET)
	public String createNews(ModelMap modelMap) {
		modelMap.addAttribute(new News(NewsType.NEWS));
		return UrlMappings.VIEW_NEWS_FORM;
	}
	
	@RequestMapping(value=UrlMappings.UPDATE_NEWS, method=RequestMethod.GET)
	public ModelAndView updateNews(@PathVariable Long newsId, ModelMap modelMap) {
		News news = newsService.getNews(newsId);
		modelMap.addAttribute(news);
		return new ModelAndView(UrlMappings.VIEW_NEWS_FORM, modelMap);
	}
	
	@RequestMapping(value={UrlMappings.CREATE_NEWS, UrlMappings.UPDATE_NEWS}, method=RequestMethod.POST, params=Mapping.PARAM_SAVE)
	public String saveNews(@Valid News news, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) return UrlMappings.VIEW_NEWS_FORM;
		newsService.saveNews(news);
		redirectAttributes.addFlashAttribute(Message.getMessage("Aktualita byla úspěšně uložena."));
		return UrlMappings.REDIRECT_NEWS_LIST;
	}
		
	@RequestMapping(value={UrlMappings.CREATE_NEWS, UrlMappings.UPDATE_NEWS}, method=RequestMethod.POST, params=Mapping.PARAM_CANCEL)
	public String cancelNewsForm() {
		return UrlMappings.REDIRECT_NEWS_LIST;
	}
	
	@RequestMapping(UrlMappings.REMOVE_NEWS)
	public String removeNews(@PathVariable Long newsId, RedirectAttributes redirectAttributes) {
		newsService.removeNews(newsId);
		redirectAttributes.addFlashAttribute(Message.getMessage("Aktualita byla úspěšně smazána."));
	    return UrlMappings.REDIRECT_NEWS_LIST;
	}
		
	/** Displays news list */
	@RequestMapping(UrlMappings.NEWS_LIST_EDUCATION)
	public String newsListEducation(ModelMap modelMap) {
		List<News> newsList = newsService.listEducationNews();
		replaceLinks(newsList);
		modelMap.addAttribute(newsList);
		modelMap.addAttribute("educationNews", true);
		return UrlMappings.VIEW_NEWS_LIST;
	}
			
	@RequestMapping(value=UrlMappings.CREATE_NEWS_EDUCATION, method=RequestMethod.GET)
	public String createNewsEducation(ModelMap modelMap) {
		modelMap.addAttribute(new News(NewsType.EDUCATION));
		return UrlMappings.VIEW_NEWS_FORM;
	}
	
	@RequestMapping(value=UrlMappings.UPDATE_NEWS_EDUCATION, method=RequestMethod.GET)
	public ModelAndView updateNewsEducation(@PathVariable Long newsId, ModelMap modelMap) {
		News news = newsService.getNews(newsId);
		modelMap.addAttribute(news);
		return new ModelAndView(UrlMappings.VIEW_NEWS_FORM, modelMap);
	}
	
	@RequestMapping(value={UrlMappings.CREATE_NEWS_EDUCATION, UrlMappings.UPDATE_NEWS_EDUCATION}, method=RequestMethod.POST, params=Mapping.PARAM_SAVE)
	public String saveNewsEducation(@Valid News news, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) return UrlMappings.VIEW_NEWS_FORM;
		newsService.saveNews(news);
		redirectAttributes.addFlashAttribute(Message.getMessage("Aktualita byla úspěšně uložena."));
		return UrlMappings.REDIRECT_NEWS_LIST_EDUCATION;
	}
		
	@RequestMapping(value={UrlMappings.CREATE_NEWS_EDUCATION, UrlMappings.UPDATE_NEWS_EDUCATION}, method=RequestMethod.POST, params=Mapping.PARAM_CANCEL)
	public String cancelNewsFormEducation() {
		return UrlMappings.REDIRECT_NEWS_LIST_EDUCATION;
	}
	
	@RequestMapping(UrlMappings.REMOVE_NEWS_EDUCATION)
	public String removeNewsEducation(@PathVariable Long newsId, RedirectAttributes redirectAttributes) {
		newsService.removeNews(newsId);
		redirectAttributes.addFlashAttribute(Message.getMessage("Aktualita byla úspěšně smazána."));
	    return UrlMappings.REDIRECT_NEWS_LIST_EDUCATION;
	}
	
	
	
	private static void replaceLinks(List<News> newsList) {
		String[] links;
		String[] textAndLink;
		String htmlLink;
		for (News news : newsList) {
			links = StringUtils.substringsBetween(news.getText(), "[", "]");
			if (links == null) continue;
			for (String link : links) {
				textAndLink = link.split(",");
				if (textAndLink.length == 2) {
					htmlLink = "<a href='" + textAndLink[1] + "' title='" + textAndLink[0] + "'>" + textAndLink[0] + "</a>";
					news.setText(StringUtils.replace(news.getText(), "[" + link + "]", htmlLink, 1));
				}
			}
		}
	}
			
}
