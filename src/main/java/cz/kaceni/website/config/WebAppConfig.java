package cz.kaceni.website.config;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.validation.Validator;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;

import cz.devmint.springext.config.support.AbstractWebI18nConfig;
import cz.devmint.springext.conversion.EntityFormatter;
import cz.devmint.springext.pagination.SessionPageableArgumentResolver;
import cz.devmint.springext.security.web.support.ForgottenLoginController;
import cz.devmint.springext.security.web.support.ForgottenLoginService;
import cz.devmint.springext.service.UserAccountService;
import cz.devmint.springext.service.UserAccountServiceImpl;
import cz.devmint.springext.validation.web.ValidationGroupInterceptor;
import cz.devmint.springext.validation.web.ValidatorAdapter;
import cz.devmint.springext.web.interceptor.AccessTokenInterceptor;
import cz.devmint.springext.web.tags.form.TagMessageCodeResolver;
import cz.kaceni.website.model.Disposal;
import cz.kaceni.website.model.WorkType;
import cz.kaceni.website.model.util.ListAnnotationFormatterFactory;
import cz.kaceni.website.web.controller.CustomerRegistrationController;
import cz.kaceni.website.web.controller.PackageInfo;
import cz.kaceni.website.web.util.InquiryPaginationInterceptor;
import cz.kaceni.website.web.util.UrlMappings;

@Configuration
@ComponentScan(basePackageClasses=PackageInfo.class)
public class WebAppConfig extends AbstractWebI18nConfig {

	@Autowired private ValidatorAdapter validator;
	@Autowired private UserAccountService userAccountService;
		
	@Bean
    public StandardServletMultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		super.addResourceHandlers(registry);
		registry.setOrder(-1);
	}
	
	@Override
	protected Validator getValidator() {
		return validator;
	}
	
	@Bean
	public LocaleResolver localeResolver() {
		return new FixedLocaleResolver(new Locale("cs"));
	}

	@Bean
	public TagMessageCodeResolver tagMessageCodeResolver() {
		return new TagMessageCodeResolver();
	}


	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new SessionPageableArgumentResolver());
	}
	
	@Override
	@Bean
	public ForgottenLoginController forgottenLoginController() throws Exception {
		return new CustomerRegistrationController();
	}
	

	@Override
	protected ForgottenLoginService getForgottenLoginService() {
		return (UserAccountServiceImpl) userAccountService;
	}
	
	@Override
	protected void addInterceptors(InterceptorRegistry registry) {
		super.addInterceptors(registry);
		ValidationGroupInterceptor interceptor = new ValidationGroupInterceptor();
		interceptor.setValidator(validator);
		Set<Class<? extends Annotation>> annotations = new HashSet<Class<? extends Annotation>>();
		annotations.add(NotNull.class);
		annotations.add(Size.class);
		interceptor.setAppFilter(annotations);
		try {
			interceptor.afterPropertiesSet();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		registry.addInterceptor(interceptor);
		registry.addInterceptor(accessTokenInterceptor()).addPathPatterns(UrlMappings.INQUIRY, UrlMappings.INQUIRY_DETAIL, UrlMappings.INQUIRY_CREATE);
		registry.addInterceptor(new InquiryPaginationInterceptor()).addPathPatterns(UrlMappings.INQUIRY_LIST);
	}
	
	@Bean
	public AccessTokenInterceptor accessTokenInterceptor() {
		return new AccessTokenInterceptor();
	}
	
	@Override
	protected void addViewControllers(ViewControllerRegistry registry) {
		super.addViewControllers(registry);
		registry.addViewController("/").setViewName("index");
	}
	
	@Override
	protected void addFormatters(FormatterRegistry registry) {
	    super.addFormatters(registry);
		registry.addFormatterForFieldAnnotation(new ListAnnotationFormatterFactory());
		registry.addFormatterForFieldType(WorkType.class, new EntityFormatter(WorkType.class, "id", "workType"));
		registry.addFormatterForFieldType(Disposal.class, new EntityFormatter(Disposal.class, "id", "disposal"));
	}

}
