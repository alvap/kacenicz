package cz.kaceni.website.config;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import cz.devmint.springext.config.support.WebAppInitializer;

/**
 * Web app initialization.
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
public class AppWebAppInitializer extends WebAppInitializer {

	private static final String LOCATION = System.getProperty("java.io.tmpdir"); // Temporary location where files will be stored
    private static final long MAX_FILE_SIZE = 50 * 1024 * 1024; // 5242880; // 50MB : Max file size, beyond that size spring will throw exception.
    private static final long MAX_REQUEST_SIZE = 20971520; // 20MB : Total request size containing Multi part.
    private static final int FILE_SIZE_THRESHOLD = 5 * 1024 * 1024; // Size threshold after which files will be written to disk
	
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { AppConfig.class, AppSecurityConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { WebAppConfig.class };
	}
	
	@Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setMultipartConfig(getMultipartConfigElement());
    }
 
    private MultipartConfigElement getMultipartConfigElement() {
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(LOCATION, MAX_FILE_SIZE, MAX_REQUEST_SIZE, FILE_SIZE_THRESHOLD);
        return multipartConfigElement;
    }
    
    @Override
    protected void setupListeners(ServletContext container) {}
	
}
