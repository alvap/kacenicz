package cz.kaceni.website.config;

import cz.devmint.springext.config.support.SpringSecurityInitializer;

/**
 * Spring Security initialization. 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
public class AppSpringSecurityInitializer extends SpringSecurityInitializer {
	
}
