package cz.kaceni.website.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import cz.devmint.springext.config.support.SpringSecurityBeans;
import cz.devmint.springext.service.UserAccountService;
import cz.kaceni.website.security.util.Roles;

/**
 * Spring security web application configuration.
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@EnableWebSecurity
@Configuration
@Import({AppConfig.class})
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired private MessageSource messageSource;
	@Autowired private UserAccountService userService;
	@Autowired private AuthenticationManager authenticationManager;
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Autowired
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService((UserDetailsService)userService).passwordEncoder(new BCryptPasswordEncoder());
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		SpringSecurityBeans.ignoreResources(web);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		SpringSecurityBeans.configure(http, messageSource, "kaceni.cz", authenticationManager);
		http.csrf().disable();
		http.authorizeRequests()
		.antMatchers("/administrace/**").hasAuthority(Roles.ROLE_ADMIN)
		.antMatchers("/**").permitAll();
	}
		
}
	