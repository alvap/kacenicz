package cz.kaceni.website.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.client.RestTemplate;

import cz.devmint.springext.config.support.BaseAppConfig;
import cz.devmint.springext.config.support.BaseMailConfig;
import cz.devmint.springext.config.support.BasePersistenceConfig;
import cz.devmint.springext.config.support.BaseValidatorConfig;
import cz.devmint.springext.config.support.ConfigProperties;
import cz.devmint.springext.orm.dao.GenericDao;
import cz.devmint.springext.orm.dao.UserAccountDao;
import cz.devmint.springext.orm.dao.impl.GenericDaoImpl;
import cz.devmint.springext.orm.dao.impl.UserAccountDaoImpl;
import cz.devmint.springext.security.oauth.DefaultAccessTokenHolder;
import cz.devmint.springext.security.token.PersistentRepositoryTokenService;
import cz.devmint.springext.security.token.support.PersistentTokenDao;
import cz.devmint.springext.security.token.support.PersistentTokenDaoImpl;
import cz.devmint.springext.security.token.support.TokenPersistenceServiceImpl;
import cz.devmint.springext.service.UserAccountService;
import cz.devmint.springext.service.UserAccountServiceImpl;

@Configuration
@Import(value={
		BasePersistenceConfig.class, 
		BaseValidatorConfig.class, 
		BaseMailConfig.class})
@PropertySource(value={"classpath:config/application.properties", "classpath:config/env.properties"})
@ComponentScan(basePackageClasses={cz.kaceni.website.service.impl.PackageInfo.class, cz.kaceni.website.dao.impl.PackageInfo.class})
public class AppConfig extends BaseAppConfig {
	
	@Bean public GenericDao genericDao() {	return new GenericDaoImpl(); }
	@Bean public UserAccountDao userAccountDao() {	return new UserAccountDaoImpl(); }
	@Bean public UserAccountService userAccountService() {	return new UserAccountServiceImpl(); }
	@Bean public RestTemplate restTemplate() { 	return new RestTemplate(); }
	@Bean public DefaultAccessTokenHolder accessTokenHolder() { return new DefaultAccessTokenHolder(); }
	@Bean public ConfigProperties configProperties() {return new ConfigProperties();}
	@Bean public PersistentRepositoryTokenService persistentRepositoryTokenService() {return new TokenPersistenceServiceImpl();}
	@Bean public PersistentTokenDao persistentTokenDao() {return new PersistentTokenDaoImpl();}
	@Bean public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() { return new PropertySourcesPlaceholderConfigurer(); }
	
}
