package cz.kaceni.website.dto;

import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

/**
 * Upload form inquiry / price offer import in admin section (file wrapper only). 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
public class UploadForm {
	
	private MultipartFile file;
	
	/** Optional - if set uploaded price offer is associated with existing inquiry.  */
	private UUID inquiryId;

	public UUID getInquiryId() {
		return inquiryId;
	}

	public void setInquiryId(UUID inquiryId) {
		this.inquiryId = inquiryId;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
}
