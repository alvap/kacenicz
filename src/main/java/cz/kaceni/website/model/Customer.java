package cz.kaceni.website.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.util.Assert;

import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.kaceni.website.model.embedded.BillingData;
import cz.kaceni.website.model.util.LengthConstraints;

/**
 * Entity representing data about customer who creates only inquiry.
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@Entity
public class Customer {

	@Id
	@GeneratedValue(generator = IdGenerator.GENERATOR_TABLE_NOOP_NAME)
	@Access(AccessType.PROPERTY)
	private Long id;

	@Version
	@Column(nullable = false)
	private Long version;
	
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@Valid
	private SystemUser userAccount = new SystemUser();
	
	@NotNull
	@Size(max = LengthConstraints.CUSTOMER_FIRST_NAME)
	private String firstName;

	@NotNull
	@Size(max = LengthConstraints.CUSTOMER_LAST_NAME)
	private String lastName;
	
	@Size(max = LengthConstraints.CUSTOMER_COMPANY_NAME)
	private String companyName;

	@NotNull
	@Size(max = LengthConstraints.CUSTOMER_PHONE_NUMBER)
	private String phoneNumber;


	/**
	 * Billing data, may be empty.
	 */
	@Embedded
	@Valid
	 @AttributeOverrides( {
	        @AttributeOverride(name="address.name", column = @Column(name="billingAddressName") ),
	        @AttributeOverride(name="address.street", column = @Column(name="billingAddressStreet")),
	        @AttributeOverride(name="address.city", column = @Column(name="billingAddressCity") ),
	        @AttributeOverride(name="address.zip", column = @Column(name="billingAddressZip")),
	        
	    })
	private BillingData billingData = new BillingData();

	/**
	 * Customer admin note.
	 */
	@Size(max = LengthConstraints.CUSTOMER_ADMIN_NOTE)
	private String adminNote;

	/** If set to false = user blocked by admin. */
	private Boolean active = true;

	/** Id of just created inquiry - registration after inquiry is sent. */
	@Transient
	private Long inquiryId;

	public String getName() {
		if (lastName == null && firstName == null) return null;
		if (firstName == null) return lastName;
		if (lastName == null) return firstName;
		return firstName + " " + lastName;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public SystemUser getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(SystemUser userAccount) {
		this.userAccount = userAccount;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public BillingData getBillingData() {
		return billingData;
	}

	public void setBillingData(BillingData billingData) {
		this.billingData = billingData;
	}

	public BillingData getbillingData() {
		return billingData;
	}

	public void setbillingData(BillingData billingData) {
		this.billingData = billingData;
	}

	public String getAdminNote() {
		return adminNote;
	}

	public void setAdminNote(String adminNote) {
		this.adminNote = adminNote;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getInquiryId() {
		return inquiryId;
	}

	public void setInquiryId(Long inquiryId) {
		this.inquiryId = inquiryId;
	}

	/**
	 * Based on id - no suitable business key.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * Based on id - no suitable business key.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!super.equals(obj)) return false;
		if (getClass() != obj.getClass()) return false;
		Customer other = (Customer) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Customer [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", userAccount=");
		builder.append(userAccount);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", companyName=");
		builder.append(companyName);
		builder.append(", phoneNumber=");
		builder.append(phoneNumber);
		builder.append(", billingData=");
		builder.append(billingData);
		builder.append(", adminNote=");
		builder.append(adminNote);
		builder.append(", active=");
		builder.append(active);
		builder.append(", inquiryId=");
		builder.append(inquiryId);
		builder.append("]");
		return builder.toString();
	}
	
}
