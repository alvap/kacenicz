package cz.kaceni.website.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import org.hibernate.annotations.Immutable;

import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.kaceni.website.model.enums.DisposalEnum;

@Immutable
@Entity
public class Disposal implements Serializable {

	@Id
	@GeneratedValue(generator = IdGenerator.ASSIGNED_GENERATOR_NAME)
	private Long id;
	
	@Version
	private Long version;
	
	private DisposalEnum disposal;

	public Disposal() {
	}

	public Disposal(DisposalEnum type) {
		this.id = new Long(type.ordinal());
		this.setDisposal(type);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public DisposalEnum getDisposal() {
		return disposal;
	}

	public void setDisposal(DisposalEnum disposal) {
		this.disposal = disposal;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Disposal other = (Disposal) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Disposal [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", disposal=");
		builder.append(disposal);
		builder.append("]");
		return builder.toString();
	}

}
