package cz.kaceni.website.model.enums;

public enum InquiryStateEnum {

	NEW, // newly created inquiry
	REJECTED, // rejected, not interesting for example 
	ACCEPTED, // accepted - price offer send to customer (and waiting for confirmation)
	CONFIRMED, // confirmed by customer
	FINISHED,
	
}
