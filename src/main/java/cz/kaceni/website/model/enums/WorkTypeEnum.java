package cz.kaceni.website.model.enums;

import java.util.ArrayList;
import java.util.List;

import cz.kaceni.website.model.WorkType;

/**
 * Represent types of work.
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 *
 */
public enum WorkTypeEnum {
	
	SINGLE_FELLING("Kácení - 1 strom"), // volne kaceni jednoho stromu 1
	SINGLE_RISK_FELLING("Rizikové kácení - 1 strom"), // rizikove kaceni jednoho stromu 2
	MULTIPLE_FELLING("Kácení - více stromů"),  // volne kaceni vice stromu 3 
	MULTIPLE_RISK_FELLING("Rizikové kácení - více stromů"), // rizikove kaceni vice stromu 4
	MILLING_OF_STUMPS("Frézování pařezů"),  // frezovani parezu 5
	GARDEN_MAINTANANCE("Komplexní údržba zahrady"), // udrzba zahrady 6
	TREE_TRIMMING("Ořez stromu"), // orez stromu 7
	PLANTING("Výsadba"), // vysadba 8
	DISPOSAL("Likvidace dřeva");  // likvidace 9

	private String text;
	
	WorkTypeEnum(String text) {
		this.text = text;
	}
	
	public static List<WorkType> classValues() {
		List<WorkType> workTypeList = new ArrayList<WorkType>(values().length);
		for (WorkTypeEnum value : values()) {
			workTypeList.add(new WorkType(value));
		}
		return workTypeList;
	}
	
	public String getText() {
		return text;
	}
	
	public static WorkTypeEnum getByOrdinal(Long id) {
		int intId = id.intValue();
		for (WorkTypeEnum item : WorkTypeEnum.values()) {
			if (item.ordinal() == intId) return item;
		}
		return null;
	}
	
}
