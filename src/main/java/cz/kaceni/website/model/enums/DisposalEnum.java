package cz.kaceni.website.model.enums;

import java.util.ArrayList;
import java.util.List;

import cz.kaceni.website.model.Disposal;

/**
 * Represent disposal types.
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
public enum DisposalEnum {
	
	WOOD, // drevo
	WOOD_INCL_CARTING, // drevo vcetne odvozu 
	BRANCHES, // vetve 
	BRANCHES_INCL_CARTING, // vetve vcetne odvozu 
	STUMPS, // parezy
	STUMPS_INCL_CARTING; // parezy vcetne odvozu
	
	public static List<Disposal> classValues() {
		List<Disposal> disposalList = new ArrayList<Disposal>(values().length);
		for (DisposalEnum value : values()) {
			disposalList.add(new Disposal(value));
		}
		return disposalList;
	}
}

