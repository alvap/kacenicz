package cz.kaceni.website.model.enums;

/**
 * Represent when the work is required / how fast it must be done.
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
public enum TermEnum {
	
	SHORT_TERM, // hurry 
	TWO_WEEKS, 
	NOT_IMPORTANT;
}
