package cz.kaceni.website.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.util.Assert;
import org.springframework.util.AutoPopulatingList;
import org.springframework.util.CollectionUtils;

import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.kaceni.website.model.embedded.Photo;
import cz.kaceni.website.model.enums.DisposalEnum;
import cz.kaceni.website.model.enums.WorkTypeEnum;
import cz.kaceni.website.model.util.ListFormat;

/**
 * Represents different kinds of required work specified in {@link Inquiry}
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@Entity
public class Work {

	@Id
	@GeneratedValue(generator = IdGenerator.GENERATOR_TABLE_NOOP_NAME)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
	private Long version;
	
	public Work() {};
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private WorkType workType;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(nullable=false)
	private Inquiry inquiry;
	
	/**
	 * Applicable only to certain {@link WorkTypeEnum}s. For example number of trees to fell for {@link WorkTypeEnum#MULTIPLE_RISK_FELLING}. 
	 */
	@Max(1000)
	private Integer numberOfItems;
	
	@Max(1000)
	private Integer additionalNumberOfItems;
	
	@Max(10000)
	private Integer diameter;
		
	/**
	 * Applicable only to certain {@link WorkType}s. For example number of trees to fell for {@link WorkType#MULTIPLE_RISK_FELLING}. 
	 */
	@Size(max=500)
	private String itemsDescription;
	
	/**
	 * Applicable to {@link WorkTypeEnum#DISPOSAL} only.
	 */
	@Fetch(FetchMode.SUBSELECT)
	@ManyToMany(cascade=CascadeType.REMOVE)
	private List<Disposal> disposalList = new AutoPopulatingList<Disposal>(Disposal.class);
	
	/** 
	 * Applicable only to {@link WorkTypeEnum#SINGLE_FELLING} or {@link WorkTypeEnum#SINGLE_RISK_FELLING} types.
	 * Height of the tree in meters. 
	 */
	@Max(100)
	private Integer height;
	
	/**
	 * Applicable only to {@link WorkTypeEnum#SINGLE_FELLING} or {@link WorkTypeEnum#SINGLE_RISK_FELLING} types.
	 * Girth of the tree in centimeters.
	 */
	@Max(3000)
	private Integer girth;
	
	/**
	 * Applicable only to {@link WorkTypeEnum#SINGLE_FELLING} or {@link WorkTypeEnum#SINGLE_RISK_FELLING} types.
	 * Girth of tree crown in meters. 
	 */
	@Max(50)
	private Integer crownWidth;
	
	/**
	 * Applicable only to {@link WorkTypeEnum#SINGLE_FELLING} or {@link WorkTypeEnum#SINGLE_RISK_FELLING} types.
	 * Description of tree type. 
	 */
	@Size(max=100)
	private String treeType;
	
	/**
	 * GPS location of the place of required work in form: [latitude],[longitude]
	 */
	@Size(max=100)
	private String gpsLocation;
	
	/** Google Drive id of image folder */
	@Column(length=50)
	private String imageFolderId;
	
	/**
	 * List of attached photos stored in Google Drive identified by file id.
	 * (only for temporary save during form reload on error)
	 */
	@ElementCollection(fetch=FetchType.EAGER)
	@ListFormat
	private List<Photo> photoList;
	
	public String shortDescription() {
		Assert.notNull(workType, "Failed to display short description of work, work type is null");
		Long id = workType.getId();
		return WorkTypeEnum.getByOrdinal(id).getText();
	}
	
	public List<Disposal> getFilteredDisposalList() {
		List<Disposal> filtered = new ArrayList<Disposal>();
		if (!CollectionUtils.isEmpty(disposalList)) {
			if (disposalList.contains(new Disposal(DisposalEnum.BRANCHES_INCL_CARTING))) {
				filtered.add(new Disposal(DisposalEnum.BRANCHES_INCL_CARTING));
			} else if (disposalList.contains(new Disposal(DisposalEnum.BRANCHES))) {
				filtered.add(new Disposal(DisposalEnum.BRANCHES));
			}
			if (disposalList.contains(new Disposal(DisposalEnum.WOOD_INCL_CARTING))) {
				filtered.add(new Disposal(DisposalEnum.WOOD_INCL_CARTING));
			} else if (disposalList.contains(new Disposal(DisposalEnum.WOOD))) {
				filtered.add(new Disposal(DisposalEnum.WOOD));
			}
			if (disposalList.contains(new Disposal(DisposalEnum.STUMPS_INCL_CARTING))) {
				filtered.add(new Disposal(DisposalEnum.STUMPS_INCL_CARTING));
			} else if (disposalList.contains(new Disposal(DisposalEnum.STUMPS))) {
				filtered.add(new Disposal(DisposalEnum.STUMPS));
			}
		}
		return filtered;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public WorkType getWorkType() {
		return workType;
	}

	public void setWorkType(WorkType workType) {
		this.workType = workType;
	}
	
	public Inquiry getInquiry() {
		return inquiry;
	}

	public void setInquiry(Inquiry inquiry) {
		this.inquiry = inquiry;
	}


	public Integer getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(Integer numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public Integer getAdditionalNumberOfItems() {
		return additionalNumberOfItems;
	}

	public void setAdditionalNumberOfItems(Integer additionalNumberOfItems) {
		this.additionalNumberOfItems = additionalNumberOfItems;
	}

	public Integer getDiameter() {
		return diameter;
	}

	public void setDiameter(Integer diameter) {
		this.diameter = diameter;
	}

	public String getItemsDescription() {
		return itemsDescription;
	}

	public void setItemsDescription(String itemsDescription) {
		this.itemsDescription = itemsDescription;
	}

	public List<Disposal> getDisposalList() {
		return disposalList;
	}

	public void setDisposalList(List<Disposal> disposalList) {
		this.disposalList = disposalList;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getGirth() {
		return girth;
	}

	public void setGirth(Integer girth) {
		this.girth = girth;
	}

	public Integer getCrownWidth() {
		return crownWidth;
	}

	public void setCrownWidth(Integer crownWidth) {
		this.crownWidth = crownWidth;
	}

	public String getTreeType() {
		return treeType;
	}

	public void setTreeType(String treeType) {
		this.treeType = treeType;
	}

	public String getGpsLocation() {
		return gpsLocation;
	}
	
	public List<Photo> getPhotoList() {
		return photoList;
	}

	public void setPhotoList(List<Photo> photoList) {
		this.photoList = photoList;
	}

	public String getImageFolderId() {
		return imageFolderId;
	}

	public void setImageFolderId(String imageFolderId) {
		this.imageFolderId = imageFolderId;
	}

	public void setGpsLocation(String gpsLocation) {
		this.gpsLocation = gpsLocation;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((workType == null) ? 0 : workType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Work other = (Work) obj;
		if (workType == null) {
			if (other.workType != null) return false;
		} else if (!workType.equals(other.workType)) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Work [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", workType=");
		builder.append(workType);
		builder.append(", numberOfItems=");
		builder.append(numberOfItems);
		builder.append(", additionalNumberOfItems=");
		builder.append(additionalNumberOfItems);
		builder.append(", diameter=");
		builder.append(diameter);
		builder.append(", itemsDescription=");
		builder.append(itemsDescription);
		builder.append(", disposalList=");
		builder.append(disposalList);
		builder.append(", height=");
		builder.append(height);
		builder.append(", girth=");
		builder.append(girth);
		builder.append(", crownWidth=");
		builder.append(crownWidth);
		builder.append(", treeType=");
		builder.append(treeType);
		builder.append(", gpsLocation=");
		builder.append(gpsLocation);
		builder.append(", imageFolderId=");
		builder.append(imageFolderId);
		builder.append(", photoList=");
		builder.append(photoList);
		builder.append("]");
		return builder.toString();
	}
	
	
}
