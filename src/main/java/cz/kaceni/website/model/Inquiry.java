package cz.kaceni.website.model;

import java.util.List;
import java.util.UUID;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.util.AutoPopulatingList;

import cz.devmint.springext.constants.TypeDefsNames;
import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.kaceni.website.model.embedded.Address;
import cz.kaceni.website.model.enums.InquiryStateEnum;
import cz.kaceni.website.model.enums.TermEnum;
import cz.kaceni.website.model.util.LengthConstraints;
import cz.kaceni.website.validation.groups.AdminInquiryChecks;

/**
 * Represents an inquiry (as filled by the customer)
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@Entity
public class Inquiry {

	public static final String NOT_SET_FIELD = "not.set@email.cz";
	
	@Id
	@GeneratedValue(generator = IdGenerator.ASSIGNED_GENERATOR_NAME)
	@Access(AccessType.PROPERTY)
	@Type(type="uuid-char")
	private UUID id;

	@Version
	@Column(nullable = false)
	private Long version;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(foreignKey = @ForeignKey(name = "inquiry_fk_customer"), nullable = false)
	@Valid
	private Customer customer = new Customer();

	/** Address of the place where the work is required. */
	@Embedded
	@Valid
	private Address address = new Address();
	
	@NotNull
	@Size(max = LengthConstraints.INQUIRY_MESSAGE)
	private String message;

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(mappedBy="inquiry")
	@Valid
	private List<Work> workList = new AutoPopulatingList<Work>(Work.class);

	/** selected work type list (checked checkboxes in inquiry form) */
	@ManyToMany(fetch = FetchType.LAZY)
	private List<WorkType> workTypeList = new AutoPopulatingList<WorkType>(WorkType.class);

	@Enumerated(EnumType.STRING)
	@Column(length=LengthConstraints.ENUM)
	private TermEnum term;

	private boolean requiresBill;

	private boolean vatPayer;
	
	@Size(max = 1000, groups=AdminInquiryChecks.class)
	@Column(length=1000)
	private String adminNote;
	
	@Enumerated(EnumType.STRING)
	@Column(length=LengthConstraints.ENUM)
	private InquiryStateEnum state = InquiryStateEnum.NEW;

	@Type(type=TypeDefsNames.JODA_DATE_TIME)
	@Column(nullable=false)
	private DateTime created;
	
	/** Google Drive id of image folder */
	@Column(length=50)
	private String imageFolderId;
	
	@Valid
	@Transient
	private PriceOffer priceOffer;
			
	public String getSubject() {
		StringBuilder subject = new StringBuilder("Požadované služby: ");
		for (Work work : workList) {
			subject.append(work.shortDescription());
			subject.append(" ;");
		}
		return subject.toString();
	}
	
	public String getAdminSubject() {
		StringBuilder subject = new StringBuilder();
		for (Work work : workList) {
			subject.append(work.shortDescription());
			subject.append("<br/>");
		}
		return subject.toString();
	}
	
	public Inquiry() {
		this.id = UUID.randomUUID();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Work> getWorkList() {
		return workList;
	}

	public void setWorkList(List<Work> workList) {
		this.workList = workList;
	}

	public List<WorkType> getWorkTypeList() {
		return workTypeList;
	}

	public void setWorkTypeList(List<WorkType> workTypeList) {
		this.workTypeList = workTypeList;
	}

	public TermEnum getTerm() {
		return term;
	}

	public void setTerm(TermEnum term) {
		this.term = term;
	}

	public boolean isRequiresBill() {
		return requiresBill;
	}

	public void setRequiresBill(boolean requiresBill) {
		this.requiresBill = requiresBill;
	}

	public boolean isVatPayer() {
		return vatPayer;
	}

	public void setVatPayer(boolean vatPayer) {
		this.vatPayer = vatPayer;
	}

	public String getAdminNote() {
		return adminNote;
	}

	public void setAdminNote(String adminNote) {
		this.adminNote = adminNote;
	}

	public InquiryStateEnum getState() {
		return state;
	}

	public void setState(InquiryStateEnum state) {
		this.state = state;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getImageFolderId() {
		return imageFolderId;
	}

	public void setImageFolderId(String imageFolderId) {
		this.imageFolderId = imageFolderId;
	}

	public PriceOffer getPriceOffer() {
		return priceOffer;
	}

	public void setPriceOffer(PriceOffer priceOffer) {
		this.priceOffer = priceOffer;
	}

	/** Based on id, no suitable business key. */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/** Based on id, no suitable business key. */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Inquiry other = (Inquiry) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Inquiry [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", customer=");
		builder.append(customer);
		builder.append(", address=");
		builder.append(address);
		builder.append(", message=");
		builder.append(message);
		builder.append(", workList=");
		builder.append(workList);
		builder.append(", workTypeList=");
		builder.append(workTypeList);
		builder.append(", term=");
		builder.append(term);
		builder.append(", requiresBill=");
		builder.append(requiresBill);
		builder.append(", vatPayer=");
		builder.append(vatPayer);
		builder.append(", adminNote=");
		builder.append(adminNote);
		builder.append(", state=");
		builder.append(state);
		builder.append(", created=");
		builder.append(created);
		builder.append(", imageFolderId=");
		builder.append(imageFolderId);
		builder.append("]");
		return builder.toString();
	}

}
