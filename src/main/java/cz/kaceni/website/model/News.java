package cz.kaceni.website.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import cz.devmint.springext.constants.TypeDefsNames;
import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.kaceni.website.model.enums.NewsType;

/**
 * News information. 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@Entity
public class News {

	@Id
	@GeneratedValue(generator = IdGenerator.GENERATOR_TABLE_NOOP_NAME)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
	private Long version;

	/**
	 * May contain hyperlinks - in form: [text of hyperlink,hyperlink]
	 */
	@NotNull
	@Size(max=2000)
	private String text;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable=false,length=20)
	private NewsType type;

	@NotNull
	@Type(type=TypeDefsNames.JODA_DATE_TIME)
	@DateTimeFormat(pattern="dd.MM.yyyy")
	private DateTime date;
	
	public News() {}
	
	public News(NewsType newsType) {
		type = newsType;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public NewsType getType() {
		return type;
	}

	public void setType(NewsType type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		News other = (News) obj;
		if (date == null) {
			if (other.date != null) return false;
		} else if (!date.equals(other.date)) return false;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		if (text == null) {
			if (other.text != null) return false;
		} else if (!text.equals(other.text)) return false;
		if (version == null) {
			if (other.version != null) return false;
		} else if (!version.equals(other.version)) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("News [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", text=");
		builder.append(text);
		builder.append(", type=");
		builder.append(type);
		builder.append(", date=");
		builder.append(date);
		builder.append("]");
		return builder.toString();
	}

}
