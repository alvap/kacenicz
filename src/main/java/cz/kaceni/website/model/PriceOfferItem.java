package cz.kaceni.website.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cz.devmint.springext.conversion.EntityFormat;
import cz.devmint.springext.orm.hibernate.IdGenerator;

@Entity
public class PriceOfferItem implements Serializable {
	
	@Id
	@GeneratedValue(generator = IdGenerator.GENERATOR_TABLE_NOOP_NAME)
	@Access(AccessType.PROPERTY)
	private Long id;

	@Version
	@Column(nullable = false)
	private Long version;
		
	@EntityFormat
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private PriceOffer priceOffer;
	
	@NotNull
	@Size(max = 200)
	private String name;

	@NotNull
	private BigDecimal amount;
	
	@Size(max = 20)
	private String amountUnit;

	@Size(max = 20)
	private String type;

	private BigDecimal priceChipping = BigDecimal.ZERO;
	private BigDecimal priceDisposal = BigDecimal.ZERO;
	private BigDecimal priceTrimming = BigDecimal.ZERO;
	private BigDecimal priceFelling = BigDecimal.ZERO;
	private BigDecimal priceStumpsMilling = BigDecimal.ZERO;
	private BigDecimal priceBinding = BigDecimal.ZERO;
	private BigDecimal priceOther = BigDecimal.ZERO;
	
	private Integer vatRate = 0;

	@Max(100)
	private BigDecimal vat;
	private BigDecimal priceWithoutVat;
	private BigDecimal priceWithVat;
	
	public void calculatePrice() {
		ensureZeroValues();
		priceWithoutVat = BigDecimal.ZERO;
		priceWithoutVat = priceWithoutVat.add(priceChipping);
		priceWithoutVat = priceWithoutVat.add(priceDisposal);
		priceWithoutVat = priceWithoutVat.add(priceTrimming);
		priceWithoutVat = priceWithoutVat.add(priceFelling);
		priceWithoutVat = priceWithoutVat.add(priceStumpsMilling);
		priceWithoutVat = priceWithoutVat.add(priceBinding);
		priceWithoutVat = priceWithoutVat.add(priceOther);
		priceWithoutVat = priceWithoutVat.multiply(amount).setScale(2, RoundingMode.HALF_UP);
		vat =  priceWithoutVat.multiply(new BigDecimal(vatRate).divide(new BigDecimal(100))).setScale(2, RoundingMode.HALF_UP);
		priceWithVat = priceWithoutVat.add(vat);
	}
	
	private void ensureZeroValues() {
		if (priceChipping == null) priceChipping = BigDecimal.ZERO;
		if (priceDisposal == null) priceDisposal = BigDecimal.ZERO;
		if (priceTrimming == null) priceTrimming = BigDecimal.ZERO;
		if (priceFelling == null) priceFelling = BigDecimal.ZERO;
		if (priceStumpsMilling == null) priceStumpsMilling = BigDecimal.ZERO;
		if (priceBinding == null) priceBinding = BigDecimal.ZERO;
		if (priceOther == null) priceOther = BigDecimal.ZERO;
		vat = BigDecimal.ZERO;
		if (vatRate == null) vatRate = 0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public PriceOffer getPriceOffer() {
		return priceOffer;
	}

	public void setPriceOffer(PriceOffer priceOffer) {
		this.priceOffer = priceOffer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getAmountUnit() {
		return amountUnit;
	}

	public void setAmountUnit(String amountUnit) {
		this.amountUnit = amountUnit;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getPriceChipping() {
		return priceChipping;
	}

	public void setPriceChipping(BigDecimal priceChipping) {
		this.priceChipping = priceChipping;
	}

	public BigDecimal getPriceDisposal() {
		return priceDisposal;
	}

	public void setPriceDisposal(BigDecimal priceDisposal) {
		this.priceDisposal = priceDisposal;
	}

	public BigDecimal getPriceTrimming() {
		return priceTrimming;
	}

	public void setPriceTrimming(BigDecimal priceTrimming) {
		this.priceTrimming = priceTrimming;
	}

	public BigDecimal getPriceFelling() {
		return priceFelling;
	}

	public void setPriceFelling(BigDecimal priceFelling) {
		this.priceFelling = priceFelling;
	}

	public BigDecimal getPriceStumpsMilling() {
		return priceStumpsMilling;
	}

	public void setPriceStumpsMilling(BigDecimal priceStumpsMilling) {
		this.priceStumpsMilling = priceStumpsMilling;
	}

	public BigDecimal getPriceBinding() {
		return priceBinding;
	}

	public void setPriceBinding(BigDecimal priceBinding) {
		this.priceBinding = priceBinding;
	}

	public BigDecimal getPriceOther() {
		return priceOther;
	}

	public void setPriceOther(BigDecimal priceOther) {
		this.priceOther = priceOther;
	}

	public Integer getVatRate() {
		return vatRate;
	}

	public void setVatRate(Integer vatRate) {
		this.vatRate = vatRate;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public BigDecimal getPriceWithoutVat() {
		return priceWithoutVat;
	}

	public void setPriceWithoutVat(BigDecimal priceWithoutVat) {
		this.priceWithoutVat = priceWithoutVat;
	}

	public BigDecimal getPriceWithVat() {
		return priceWithVat;
	}

	public void setPriceWithVat(BigDecimal priceWithVat) {
		this.priceWithVat = priceWithVat;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		PriceOfferItem other = (PriceOfferItem) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PriceOfferItem [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", name=");
		builder.append(name);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", amountUnit=");
		builder.append(amountUnit);
		builder.append(", type=");
		builder.append(type);
		builder.append(", priceChipping=");
		builder.append(priceChipping);
		builder.append(", priceDisposal=");
		builder.append(priceDisposal);
		builder.append(", priceTrimming=");
		builder.append(priceTrimming);
		builder.append(", priceFelling=");
		builder.append(priceFelling);
		builder.append(", priceStumpsMilling=");
		builder.append(priceStumpsMilling);
		builder.append(", priceBinding=");
		builder.append(priceBinding);
		builder.append(", priceOther=");
		builder.append(priceOther);
		builder.append(", vatRate=");
		builder.append(vatRate);
		builder.append(", vat=");
		builder.append(vat);
		builder.append(", priceWithoutVat=");
		builder.append(priceWithoutVat);
		builder.append(", priceWithVat=");
		builder.append(priceWithVat);
		builder.append("]");
		return builder.toString();
	}
	
	
}
