package cz.kaceni.website.model;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Immutable;

import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.kaceni.website.model.enums.WorkTypeEnum;
import cz.kaceni.website.model.util.LengthConstraints;

@Immutable
@Entity
public class WorkType implements Serializable {

	@Id
	@GeneratedValue(generator = IdGenerator.ASSIGNED_GENERATOR_NAME)
	@Access(AccessType.PROPERTY)
	private Long id;
	
	@Version
	private Long version;

	@Enumerated(EnumType.STRING)
	@Size(max=LengthConstraints.ENUM)
	private WorkTypeEnum workType;

	public WorkType() {}

	public WorkType(WorkTypeEnum type) {
		this.id = new Long(type.ordinal());
		this.setWorkType(type);
	}
	
	public WorkTypeEnum getWorkTypeById() {
		return WorkTypeEnum.getByOrdinal(getId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public WorkTypeEnum getWorkType() {
		return workType;
	}

	public void setWorkType(WorkTypeEnum workType) {
		this.workType = workType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((workType == null) ? 0 : workType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		WorkType other = (WorkType) obj;
		if (workType != other.workType) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WorkType [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", workType=");
		builder.append(workType);
		builder.append("]");
		return builder.toString();
	}

}
