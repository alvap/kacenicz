package cz.kaceni.website.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Entity;

import org.joda.time.DateTime;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import cz.devmint.springext.model.UserAccountImpl;
import cz.kaceni.website.security.util.Roles;

@Entity
public class SystemUser extends UserAccountImpl {
	
	public SystemUser() {
		password = "none";
		lastLoginDate = DateTime.now();
		registrationDate = DateTime.now();
	}
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<SimpleGrantedAuthority> authorities = new HashSet<SimpleGrantedAuthority>();
		if (getId().longValue() <= 0) authorities.add(new SimpleGrantedAuthority(Roles.ROLE_ADMIN));
		return authorities;
	}


}
