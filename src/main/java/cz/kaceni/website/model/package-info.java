@FetchProfiles({
	@FetchProfile(name = cz.kaceni.website.model.util.FetchProfileNames.WORK_WITH_WORK_TYPE, fetchOverrides = {
		@FetchProfile.FetchOverride(entity = Work.class, association = "workType", mode = FetchMode.JOIN),
	}),
	@FetchProfile(name = cz.kaceni.website.model.util.FetchProfileNames.PRICE_OFFER_ITEM_WITH_PRICE_OFFER, fetchOverrides = {
			@FetchProfile.FetchOverride(entity = PriceOfferItem.class, association = "priceOffer", mode = FetchMode.JOIN),
	})
})
@GenericGenerators({
	@GenericGenerator(name = AppIdGenerator.SHARED_PRIMARY_KEY_GENERATOR_INQUIRY, strategy = IdGenerator.FOREIGN_STRATEGY, parameters=@Parameter(name="property", value="inquiry"))
})
	

package cz.kaceni.website.model;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.GenericGenerators;
import org.hibernate.annotations.Parameter;

import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.kaceni.website.model.util.AppIdGenerator;








	
	




