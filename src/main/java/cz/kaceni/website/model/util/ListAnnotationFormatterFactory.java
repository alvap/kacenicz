package cz.kaceni.website.model.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.Parser;
import org.springframework.format.Printer;

public final class ListAnnotationFormatterFactory implements AnnotationFormatterFactory<ListFormat> {

	@Override
	public Set<Class<?>> getFieldTypes() {
		Set<Class<?>> domainObject = new HashSet<Class<?>>();
		domainObject.add(List.class);
		return domainObject;
	}

	@Override
	public Printer<?> getPrinter(ListFormat annotation, Class<?> fieldType) {
		return new ListFormatter();
	}

	@Override
	public Parser<?> getParser(ListFormat annotation, Class<?> fieldType) {
		return new ListFormatter();
	}

	
}
