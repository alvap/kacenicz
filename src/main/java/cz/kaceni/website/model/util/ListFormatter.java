package cz.kaceni.website.model.util;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.format.Formatter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import cz.kaceni.website.model.embedded.Photo;

/**
 * Enables to store and restore selected entity properties in form of single string.
 *
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@SuppressWarnings("rawtypes")
public class ListFormatter implements Formatter<List<?>> {

	private ObjectMapper mapper = new ObjectMapper();
	
	public ListFormatter() {}
	
	@Override
	public String print(List<?> list, Locale locale) {
		if (CollectionUtils.isEmpty(list)) return "";
		try {
			return mapper.writer().writeValueAsString(list);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<?> parse(String text, Locale locale) throws ParseException {
		if (StringUtils.isEmpty(text)) return Collections.EMPTY_LIST;
		try {
			return mapper.readValue(text, TypeFactory.defaultInstance().constructCollectionType(List.class, Photo.class));
		} catch (IOException e) {
			throw new ParseException(e.getMessage(), -1);
		}
	}

	

}
