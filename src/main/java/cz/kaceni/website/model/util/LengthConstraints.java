package cz.kaceni.website.model.util;

/**
 * Length constraints for various entity attributes.
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
public class LengthConstraints {

   public static final int ENUM = 30;
 
    // ****************************************************
    // Customer
    //*****************************************************
    public static final int CUSTOMER_FIRST_NAME = 30;
    public static final int CUSTOMER_LAST_NAME = 30;
    public static final int CUSTOMER_COMPANY_NAME = 30;
    public static final int CUSTOMER_PHONE_NUMBER = 30;
    public static final int CUSTOMER_ADMIN_NOTE = 200;
            
    // ****************************************************
    // Address and Billing data
    //*****************************************************
    public static final int ADDRESS_NAME = 50;
    public static final int ADDRESS_STREET = 50 ;
    public static final int ADDRESS_CITY = 30;
    public static final int ADDRESS_ZIP = 5; 
    public static final int VAT_NO = 20;
    public static final int REG_NO = 20;

    // ****************************************************
    // Inquiry
    //*****************************************************
	public static final int INQUIRY_MESSAGE = 500;

	// ****************************************************
    // Work
    //*****************************************************
	public static final int WORK_ITEMS_DESCRIPTION = 100;

}
