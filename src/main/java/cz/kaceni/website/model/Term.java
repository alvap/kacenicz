package cz.kaceni.website.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.kaceni.website.model.enums.TermEnum;

@Immutable
@Entity
public class Term {

	@Id
	@GeneratedValue(generator = IdGenerator.ASSIGNED_GENERATOR_NAME)
	private Long id;

	private TermEnum term;

	public Term() {
	}

	public Term(TermEnum type) {
		this.id = new Long(type.ordinal());
		this.setTerm(type);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TermEnum getTerm() {
		return term;
	}

	public void setTerm(TermEnum term) {
		this.term = term;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((term == null) ? 0 : term.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Term other = (Term) obj;
		if (term != other.term) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Term [id=");
		builder.append(id);
		builder.append(", term=");
		builder.append(term);
		builder.append("]");
		return builder.toString();
	}

}
