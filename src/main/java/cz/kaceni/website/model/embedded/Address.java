package cz.kaceni.website.model.embedded;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cz.kaceni.website.model.util.LengthConstraints;
import cz.kaceni.website.model.util.RequiredBillingDataFormChecks;

/**
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@Embeddable
public class Address {

	@Column(name="addressName")
    @Size(max=LengthConstraints.ADDRESS_NAME)
	@NotNull(groups=RequiredBillingDataFormChecks.class)
	private String name;
	
	@Column(name="addressStreet")
	@Size(max = LengthConstraints.ADDRESS_STREET)
	@NotNull(groups=RequiredBillingDataFormChecks.class)
    private String street;

	@Column(name="addressCity")
    @Size(max = LengthConstraints.ADDRESS_CITY)
	@NotNull(groups=RequiredBillingDataFormChecks.class)
    private String city;

	@Column(name="addressZip")
    @Size(max = LengthConstraints.ADDRESS_ZIP)
	@NotNull(groups=RequiredBillingDataFormChecks.class)
    private String zip;
	
	public void copy(Address target) {
		target.name = name;
		target.street = street;
		target.city = city;
		target.zip = zip;
	}
	
    public boolean isEmpty() {

        return  name == null &&
        		street == null &&
                city == null &&
                zip == null;
    }
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null) return false;
		} else if (!city.equals(other.city)) return false;
		if (name == null) {
			if (other.name != null) return false;
		} else if (!name.equals(other.name)) return false;
		if (street == null) {
			if (other.street != null) return false;
		} else if (!street.equals(other.street)) return false;
		if (zip == null) {
			if (other.zip != null) return false;
		} else if (!zip.equals(other.zip)) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Address [name=");
		builder.append(name);
		builder.append(", street=");
		builder.append(street);
		builder.append(", city=");
		builder.append(city);
		builder.append(", zip=");
		builder.append(zip);
		builder.append("]");
		return builder.toString();
	}

}
