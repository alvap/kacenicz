package cz.kaceni.website.model.embedded;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents inquiry photo uploaded to Google Drive. 
 *  
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 *
 */
@Embeddable
public class Photo {

	/** Google Drive file id. */
	private String id;
	/** Google Drive 'thumbnailLink' */
	private String thumbnailLink;
	/** Google Drive 'webContentLink' */
	private String webContentLink;
	/** Original file 'name' */
	private String originalFilename;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getThumbnailLink() {
		return thumbnailLink;
	}
	public void setThumbnailLink(String thumbnailLink) {
		this.thumbnailLink = thumbnailLink;
	}
	public String getWebContentLink() {
		return webContentLink;
	}
	public void setWebContentLink(String webContentLink) {
		this.webContentLink = webContentLink;
	}
	
	public String getOriginalFilename() {
		return originalFilename;
	}
	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Photo other = (Photo) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Photo [id=");
		builder.append(id);
		builder.append(", thumbnailLink=");
		builder.append(thumbnailLink);
		builder.append(", webContentLink=");
		builder.append(webContentLink);
		builder.append(", originalFilename=");
		builder.append(originalFilename);
		builder.append("]");
		return builder.toString();
	}
	
}
