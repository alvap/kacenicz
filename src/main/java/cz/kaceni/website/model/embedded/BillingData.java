package cz.kaceni.website.model.embedded;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import cz.kaceni.website.model.util.LengthConstraints;

/**
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@Embeddable
public class BillingData {

		// TODO 13. 5. 2016: some columns names overriding is needed? + validation checks
		@Embedded
		@Valid
		private Address address = new Address();
	
	    @Size(max = LengthConstraints.VAT_NO)
	    private String vatNo;

	    @Size(max = LengthConstraints.REG_NO)
	    private String regNo;

	    public BillingData() {}

	    public boolean isEmpty() {
	        return (address == null || address.isEmpty()) && regNo == null && vatNo == null;
	    }

		public Address getAddress() {
			return address;
		}

		public void setAddress(Address address) {
			this.address = address;
		}

		public String getVatNo() {
			return vatNo;
		}

		public void setVatNo(String vatNo) {
			this.vatNo = vatNo;
		}

		public String getRegNo() {
			return regNo;
		}

		public void setRegNo(String regNo) {
			this.regNo = regNo;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((address == null) ? 0 : address.hashCode());
			result = prime * result + ((regNo == null) ? 0 : regNo.hashCode());
			result = prime * result + ((vatNo == null) ? 0 : vatNo.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			BillingData other = (BillingData) obj;
			if (address == null) {
				if (other.address != null) return false;
			} else if (!address.equals(other.address)) return false;
			if (regNo == null) {
				if (other.regNo != null) return false;
			} else if (!regNo.equals(other.regNo)) return false;
			if (vatNo == null) {
				if (other.vatNo != null) return false;
			} else if (!vatNo.equals(other.vatNo)) return false;
			return true;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("BillingData [address=");
			builder.append(address);
			builder.append(", vatNo=");
			builder.append(vatNo);
			builder.append(", regNo=");
			builder.append(regNo);
			builder.append("]");
			return builder.toString();
		}
		
}
