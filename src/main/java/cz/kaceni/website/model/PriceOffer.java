package cz.kaceni.website.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.Assert;

import cz.devmint.springext.constants.TypeDefsNames;
import cz.devmint.springext.orm.hibernate.IdGenerator;
import cz.devmint.springext.validation.validator.ValidatorConstants;
import cz.kaceni.website.model.embedded.Address;
import cz.kaceni.website.model.util.LengthConstraints;

/**
 * Entity representing price offer for given inquiry.
 * 
 * @author Pavla Novakova [pavla.novakova(at)gmail.com]
 */
@Entity
public class PriceOffer implements Serializable {

	@Id
	@GeneratedValue(generator = IdGenerator.GENERATOR_TABLE_NOOP_NAME)
	@Access(AccessType.PROPERTY)
	private Long id;

	@Version
	@Column(nullable = false)
	private Long version;

	@OneToOne(fetch = FetchType.LAZY, optional=true)
	private Inquiry inquiry;
	
	/** Customer address that is part of price offer header. Default value is taken from inquiry address. */
	@Embedded
	@Valid
	private Address address = new Address();
	
	/** Customer phone number that is part of price offer header. Default value is taken from inquiry phone number. */
	@Size(max = LengthConstraints.CUSTOMER_PHONE_NUMBER)
	private String phoneNumber;
	
	/** Customer email that is part of price offer header. Default value is taken from inquiry email. */
	@Email
	@Size(max = ValidatorConstants.EMAIL_LENGTH)
	private String email;
		
	@OneToMany(fetch = FetchType.LAZY, mappedBy="priceOffer", cascade=CascadeType.REMOVE)
	@Valid
	private List<PriceOfferItem> priceOfferItemList;
	
	/** Note that may be printed in price offer for internal purposes (not visible to customer) */
	@Size(max=500)
	private String internalNote;

	@Column(nullable = false)
	private BigDecimal totalPriceWithoutVat = BigDecimal.ZERO;

	@Column(nullable = false)	
	private BigDecimal totalPriceWithVat = BigDecimal.ZERO;
	
	@Type(type=TypeDefsNames.JODA_DATE_TIME)
	@DateTimeFormat(pattern="dd.MM.yyyy")
	private DateTime date;

	public void calculatePrice() {
		totalPriceWithoutVat = BigDecimal.ZERO;
		totalPriceWithVat = BigDecimal.ZERO;
		for (PriceOfferItem priceOfferItem : priceOfferItemList) {
			totalPriceWithoutVat = totalPriceWithoutVat.add(priceOfferItem.getPriceWithoutVat());
			totalPriceWithVat = totalPriceWithVat.add(priceOfferItem.getPriceWithVat());
		}
	}
	
	public BigDecimal getTotalVat() {
		return totalPriceWithVat.subtract(totalPriceWithoutVat);
	}
	
	public BigDecimal getTotalPriceChipping() {
		Assert.notNull(priceOfferItemList);
		BigDecimal total = BigDecimal.ZERO;
		for (PriceOfferItem item : priceOfferItemList) {
			if (item.getPriceChipping() != null) {
				total = total.add(item.getAmount().multiply(item.getPriceChipping()));
			}
		}
		return total;
	}
	
	public BigDecimal getTotalPriceDisposal() {
		Assert.notNull(priceOfferItemList);
		BigDecimal total = BigDecimal.ZERO;
		for (PriceOfferItem item : priceOfferItemList) {
			if (item.getPriceDisposal() != null) {
				total = total.add(item.getAmount().multiply(item.getPriceDisposal()));
			}
		}
		return total;
	}
	
	public BigDecimal getTotalPriceTrimming() {
		Assert.notNull(priceOfferItemList);
		BigDecimal total = BigDecimal.ZERO;
		for (PriceOfferItem item : priceOfferItemList) {
			if (item.getPriceTrimming() != null) {
				total = total.add(item.getAmount().multiply(item.getPriceTrimming()));
			}
		}
		return total;
	}
	
	public BigDecimal getTotalPriceFelling() {
		Assert.notNull(priceOfferItemList);
		BigDecimal total = BigDecimal.ZERO;
		for (PriceOfferItem item : priceOfferItemList) {
			if (item.getPriceFelling() != null) {
				total = total.add(item.getAmount().multiply(item.getPriceFelling()));
			}
		}
		return total;
	}
	
	public BigDecimal getTotalPriceStumpsMilling() {
		Assert.notNull(priceOfferItemList);
		BigDecimal total = BigDecimal.ZERO;
		for (PriceOfferItem item : priceOfferItemList) {
			if (item.getPriceStumpsMilling() != null) {
				total = total.add(item.getAmount().multiply(item.getPriceStumpsMilling()));
			}
		}
		return total;
	}
	
	public BigDecimal getTotalPriceBinding() {
		Assert.notNull(priceOfferItemList);
		BigDecimal total = BigDecimal.ZERO;
		for (PriceOfferItem item : priceOfferItemList) {
			if (item.getPriceBinding() != null) {
				total = total.add(item.getAmount().multiply(item.getPriceBinding()));
			}
		}
		return total;
	}
	
	public BigDecimal getTotalPriceOther() {
		Assert.notNull(priceOfferItemList);
		BigDecimal total = BigDecimal.ZERO;
		for (PriceOfferItem item : priceOfferItemList) {
			if (item.getPriceOther() != null) {
				total = total.add(item.getAmount().multiply(item.getPriceOther()));
			}
		}
		return total;
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Inquiry getInquiry() {
		return inquiry;
	}

	public void setInquiry(Inquiry inquiry) {
		this.inquiry = inquiry;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInternalNote() {
		return internalNote;
	}

	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public List<PriceOfferItem> getPriceOfferItemList() {
		return priceOfferItemList;
	}

	public void setPriceOfferItemList(List<PriceOfferItem> priceOfferItemList) {
		this.priceOfferItemList = priceOfferItemList;
	}

	public BigDecimal getTotalPriceWithoutVat() {
		return totalPriceWithoutVat;
	}

	public void setTotalPriceWithoutVat(BigDecimal totalPriceWithoutVat) {
		this.totalPriceWithoutVat = totalPriceWithoutVat;
	}

	public BigDecimal getTotalPriceWithVat() {
		return totalPriceWithVat;
	}

	public void setTotalPriceWithVat(BigDecimal totalPriceWithVat) {
		this.totalPriceWithVat = totalPriceWithVat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		PriceOffer other = (PriceOffer) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PriceOffer [id=");
		builder.append(id);
		builder.append(", version=");
		builder.append(version);
		builder.append(", inquiry=");
		builder.append(inquiry);
		builder.append(", address=");
		builder.append(address);
		builder.append(", phoneNumber=");
		builder.append(phoneNumber);
		builder.append(", email=");
		builder.append(email);
		builder.append(", priceOfferItemList=");
		builder.append(priceOfferItemList);
		builder.append(", internalNote=");
		builder.append(internalNote);
		builder.append(", totalPriceWithoutVat=");
		builder.append(totalPriceWithoutVat);
		builder.append(", totalPriceWithVat=");
		builder.append(totalPriceWithVat);
		builder.append(", date=");
		builder.append(date);
		builder.append("]");
		return builder.toString();
	}
	
	

}
