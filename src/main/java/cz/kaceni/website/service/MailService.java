package cz.kaceni.website.service;

import cz.kaceni.website.model.Inquiry;

public interface MailService {

	void notifyNewInquiry(Inquiry inquiry);

}
