package cz.kaceni.website.service;

import java.util.List;

import cz.kaceni.website.model.News;

public interface NewsService {

	void saveNews(News news);
	void removeNews(Long newsId);
	List<News> listNews();
	News getNews(Long newsId);
	List<News> listEducationNews();

}
