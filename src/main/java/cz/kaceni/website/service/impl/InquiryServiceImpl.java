package cz.kaceni.website.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.hibernate.StaleObjectStateException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import cz.devmint.springext.orm.dao.GenericDao;
import cz.kaceni.website.dao.DisposalDao;
import cz.kaceni.website.dao.InquiryDao;
import cz.kaceni.website.dao.PriceOfferDao;
import cz.kaceni.website.dao.PriceOfferItemDao;
import cz.kaceni.website.dao.WorkDao;
import cz.kaceni.website.dao.WorkTypeDao;
import cz.kaceni.website.model.Disposal;
import cz.kaceni.website.model.Inquiry;
import cz.kaceni.website.model.PriceOffer;
import cz.kaceni.website.model.PriceOfferItem;
import cz.kaceni.website.model.Work;
import cz.kaceni.website.model.WorkType;
import cz.kaceni.website.model.enums.InquiryStateEnum;
import cz.kaceni.website.model.enums.WorkTypeEnum;
import cz.kaceni.website.service.InquiryService;
import cz.kaceni.website.web.util.InquiryFilter;

@Service
public class InquiryServiceImpl implements InquiryService {

	private static final Logger log = LoggerFactory.getLogger(InquiryServiceImpl.class);
	
	@Autowired private InquiryDao inquiryDao;
	@Autowired private WorkTypeDao workTypeDao;
	@Autowired private WorkDao workDao;
	@Autowired private DisposalDao disposalDao;
	@Autowired private GenericDao genericDao; 
	@Autowired private PriceOfferDao priceOfferDao;
	@Autowired private PriceOfferItemDao priceOfferItemDao;

	
	@Override
	@Transactional
	public void saveInquiry(Inquiry inquiry) {
		
		List<WorkType> workTypeList = inquiry.getWorkTypeList();
		List<Work> workList = inquiry.getWorkList();
		List<Work> newWorkList = new ArrayList<Work>();
		List<WorkType> newWorkTypeList = new ArrayList<WorkType>();
		Work newWork;
		WorkType managedWorkType;
		for (WorkType workType : workTypeList) {
			if (workType == null) continue;
			newWork = findWorkByWorkType(workList, workType);
			newWork.setInquiry(inquiry);
			managedWorkType = workTypeDao.getReference(workType.getId());
			newWork.setWorkType(managedWorkType);
			newWorkList.add(newWork);
			newWorkTypeList.add(managedWorkType);
		}
		inquiry.setWorkList(newWorkList);
		inquiry.setWorkTypeList(newWorkTypeList);
		inquiry.setCreated(new DateTime());
		inquiry.setState(InquiryStateEnum.NEW);
		inquiryDao.persist(inquiry);
		
		for (Work work : newWorkList) {
			if (WorkTypeEnum.DISPOSAL.equals(work.getWorkType().getWorkType())) {
				List<Disposal> newDisposalList = new ArrayList<Disposal>();
				for (Disposal disposal : work.getDisposalList()) {
					if (disposal == null) continue;
					newDisposalList.add(disposalDao.getReference(disposal.getId()));
				}
				work.setDisposalList(newDisposalList);
			}
			workDao.persist(work);  
			
		}
	}
	
	private Work findWorkByWorkType(List<Work> workList, WorkType workType) {
		for (Work work : workList) {
			if (work.getWorkType().getId().equals(workType.getId())) return work;
		}
		throw new IllegalStateException("Work not found.");
	}

	@Override
	@Transactional
	public Inquiry findInquiry(UUID inquiryId) {
		// genericDao.enableFetchProfile(FetchProfileNames.WORK_WITH_WORK_TYPE);
		Inquiry inquiry = inquiryDao.findRequired(inquiryId);
		inquiry.getWorkList().size(); // intialize
		for (Work work : inquiry.getWorkList()) {
			if (work.getWorkType().getId().equals(new Long(WorkTypeEnum.DISPOSAL.ordinal()))) { // Disposal
				work.getDisposalList().size(); // init
			}
		}
		return inquiry;
	}
	
	@Override
	@Transactional
	public Inquiry findInquiryWithoutWorkList(UUID inquiryId) {
		return inquiryDao.findRequired(inquiryId);
	}
	
	@Override
	@Transactional
	public void removeInquiry(UUID inquiryId) {
		Inquiry inquiry = inquiryDao.findRequired(inquiryId);
		inquiry.getWorkList().size(); // init
		for (Work work : inquiry.getWorkList()) {
			workDao.remove(work);
		}
		
		PriceOffer priceOffer = priceOfferDao.find("inquiry.id", inquiryId);
		if (priceOffer != null) {
			priceOffer.getPriceOfferItemList().size(); // init
			priceOfferDao.remove(priceOffer);
		}
		inquiryDao.remove(inquiry);
	}

	@Override
	@Transactional
	public void updateInquiryAdminNote(UUID inquiryId, String adminNote) {
		Inquiry inquiry = inquiryDao.findRequired(inquiryId);
		inquiry.setAdminNote(adminNote);
	}

	@Override
	@Transactional
	public Page<Inquiry> getInquiryList(InquiryFilter inquiryFilter, Pageable pageable) {
		Assert.notNull(inquiryFilter);
		Assert.notNull(pageable);
		log.debug("Loading inquiry list using filter: {} and pagination context: {}", new Object[] {inquiryFilter, pageable});
		Page<Inquiry> inquiryPage = inquiryDao.list(inquiryFilter.getRestrictions(), pageable);
		if (!inquiryPage.getContent().isEmpty()) {
			inquiryPage.getContent().get(0).getWorkList().size(); // initialize?
		}
		return inquiryPage;
	}

	@Override
	@Transactional
	public void changeInquiryState(UUID inquiryId, InquiryStateEnum inquiryState) {
		Inquiry inquiry = inquiryDao.findRequired(inquiryId);
		inquiry.setState(inquiryState);
	}

	@Override
	@Transactional
	public PriceOfferItem getPriceOfferItem(Long priceOfferItemId) {
		return priceOfferItemDao.findRequired(priceOfferItemId);
	}

	@Override
	@Transactional
	public void savePriceOfferItem(PriceOfferItem priceOfferItem, UUID inquiryId) {
		if (priceOfferItem.getId() != null) {
			PriceOffer priceOffer = priceOfferDao.findRequired(priceOfferItem.getPriceOffer().getId());
			priceOfferItem.setPriceOffer(priceOffer);
			priceOfferItem.calculatePrice();
			priceOfferItemDao.merge(priceOfferItem);
			genericDao.flush();
			priceOffer.getPriceOfferItemList().size(); // init
			priceOffer.calculatePrice();
			return;
		} 
		
		PriceOffer priceOffer = priceOfferDao.find("inquiry.id", inquiryId);
		if (priceOffer == null) {
			Inquiry inquiry = inquiryDao.findRequired(inquiryId);
			priceOffer = new PriceOffer();
			priceOffer.setInquiry(inquiry);
			if (inquiry.getAddress() != null && !inquiry.getAddress().isEmpty()) {
				inquiry.getAddress().copy(priceOffer.getAddress());
			}
			priceOffer.setPhoneNumber(inquiry.getCustomer().getPhoneNumber());
			priceOffer.setEmail(inquiry.getCustomer().getUserAccount().getEmail());
			priceOffer.setDate(new DateTime());
			priceOfferDao.persist(priceOffer);
			priceOfferItem.setPriceOffer(priceOffer);
			priceOfferItem.calculatePrice();
			priceOfferItemDao.persist(priceOfferItem);
			genericDao.flush();
			priceOffer.setPriceOfferItemList(Collections.singletonList(priceOfferItem));
			priceOffer.calculatePrice();
		} else {
			priceOfferItem.setPriceOffer(priceOffer);
			priceOfferItem.calculatePrice();
			priceOfferItemDao.persist(priceOfferItem);
			genericDao.flush();
			priceOffer.getPriceOfferItemList().size(); // init
			priceOffer.calculatePrice();
		}
	}
		
	@Override
	@Transactional
	public PriceOffer savePriceOffer(PriceOffer priceOfferData) {
		PriceOffer priceOffer = priceOfferDao.findRequired(priceOfferData.getId());
		if (!priceOfferData.getVersion().equals(priceOffer.getVersion())) throw new StaleObjectStateException(PriceOffer.class.getName(), priceOfferData.getId());
		priceOffer.setAddress(priceOfferData.getAddress());
		priceOffer.setInternalNote(priceOfferData.getInternalNote());
		priceOffer.setPhoneNumber(priceOfferData.getPhoneNumber());
		priceOffer.setEmail(priceOfferData.getEmail());
		return priceOffer;
 	} 

	@Override
	@Transactional
	public void removePriceOfferItem(Long priceOfferItemId) {
		PriceOfferItem priceOfferItem = priceOfferItemDao.findRequired(priceOfferItemId);
		PriceOffer priceOffer = priceOfferItem.getPriceOffer();
		priceOfferItemDao.remove(priceOfferItem);
		genericDao.flush();
		priceOffer.calculatePrice();
	}

	@Override
	@Transactional
	public PriceOffer findInquiryPriceOffer(UUID inquiryId) {
		PriceOffer priceOffer = priceOfferDao.find("inquiry.id", inquiryId);
		if (priceOffer == null) return null;
		priceOffer.getPriceOfferItemList().size(); // init
		return priceOffer;
	}

	@Override
	@Transactional
	public PriceOffer getPriceOffer(Long priceOfferId) {
		PriceOffer priceOffer = priceOfferDao.findRequired(priceOfferId);
		return priceOffer;
	}
	
	@Override
	@Transactional
	public PriceOffer getPriceOfferForPrint(Long priceOfferId) {
		PriceOffer priceOffer = priceOfferDao.findRequired(priceOfferId);
		priceOffer.getPriceOfferItemList().size();
		return priceOffer;
	}

	
	@Override
	@Transactional
	public void removePriceOffer(Long priceOfferId) {
		PriceOffer priceOffer = priceOfferDao.findRequired(priceOfferId);
		priceOffer.getPriceOfferItemList().size(); // init
		priceOfferDao.remove(priceOffer);
	}

	@Override
	@Transactional
	public void importInquiry(Inquiry inquiry) {
		DateTime now = new DateTime();
		inquiry.setCreated(now);
		inquiryDao.persist(inquiry);
		inquiry.getPriceOffer().setDate(now);
		inquiry.getPriceOffer().setPhoneNumber(inquiry.getCustomer().getPhoneNumber());
		inquiry.getPriceOffer().setEmail(inquiry.getCustomer().getUserAccount().getEmail());
		inquiry.getPriceOffer().getAddress().setName(inquiry.getCustomer().getName());
		PriceOffer priceOffer = priceOfferDao.persist(inquiry.getPriceOffer());
		priceOffer.setInquiry(inquiry);
		for (PriceOfferItem priceOfferItem : priceOffer.getPriceOfferItemList()) {
			priceOfferItem.setPriceOffer(priceOffer);
			priceOfferItem.calculatePrice();
			priceOfferItemDao.persist(priceOfferItem);
		}
		genericDao.flush();
		priceOffer.getPriceOfferItemList().size(); // init
		priceOffer.calculatePrice();
	}
	
	@Override
	@Transactional
	public void importPriceOffer(Inquiry inquiry) {
		DateTime now = new DateTime();
		Inquiry existingInquiry = inquiryDao.findRequired(inquiry.getId());
		inquiry.getPriceOffer().setDate(now);
		if (!Inquiry.NOT_SET_FIELD.equals(inquiry.getCustomer().getPhoneNumber())) { inquiry.getPriceOffer().setPhoneNumber(inquiry.getCustomer().getPhoneNumber()); }
		if (!Inquiry.NOT_SET_FIELD.equals(inquiry.getCustomer().getUserAccount().getEmail())) { inquiry.getPriceOffer().setEmail(inquiry.getCustomer().getUserAccount().getEmail()); }
		if (!Inquiry.NOT_SET_FIELD.equals(inquiry.getCustomer().getLastName())) {
			if (Inquiry.NOT_SET_FIELD.equals(inquiry.getCustomer().getFirstName())) {inquiry.getCustomer().setFirstName(null);}
			inquiry.getPriceOffer().getAddress().setName(inquiry.getCustomer().getName()); 
		};
		PriceOffer priceOffer = priceOfferDao.persist(inquiry.getPriceOffer());
		priceOffer.setInquiry(existingInquiry);
		for (PriceOfferItem priceOfferItem : priceOffer.getPriceOfferItemList()) {
			priceOfferItem.setPriceOffer(priceOffer);
			priceOfferItem.calculatePrice();
			priceOfferItemDao.persist(priceOfferItem);
		}
		genericDao.flush();
		priceOffer.getPriceOfferItemList().size(); // init
		priceOffer.calculatePrice();
	}

}
