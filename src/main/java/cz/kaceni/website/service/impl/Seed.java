package cz.kaceni.website.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import cz.kaceni.website.model.Disposal;
import cz.kaceni.website.model.SystemUser;
import cz.kaceni.website.model.WorkType;
import cz.kaceni.website.model.enums.DisposalEnum;
import cz.kaceni.website.model.enums.WorkTypeEnum;

// @Service
public class Seed implements InitializingBean {

	@PersistenceContext private EntityManager em;
    @Autowired private TransactionTemplate template;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		init();
	}
	
	private void init() {
		template.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				for (WorkTypeEnum workTypeEnum : WorkTypeEnum.values()) {
					em.persist(new WorkType(workTypeEnum));
				}
				for (DisposalEnum disposalEnum : DisposalEnum.values()) {
					em.persist(new Disposal(disposalEnum));
				}
				SystemUser admin = new SystemUser();
				admin.setEmail("kaceni");
				admin.setRegistrationDate(DateTime.now());
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				admin.setPassword(passwordEncoder.encode("admin"));
				em.persist(admin);				
			}
		});
		
	}
	

}
