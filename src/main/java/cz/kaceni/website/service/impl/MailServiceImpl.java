package cz.kaceni.website.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import cz.devmint.springext.mail.TemplateMailSender;
import cz.devmint.springext.security.token.SendTokenService;
import cz.devmint.springext.security.web.support.Hints;
import cz.kaceni.website.model.Inquiry;
import cz.kaceni.website.service.MailService;

@Service
public class MailServiceImpl implements SendTokenService, MailService {

	private static final Logger log = LoggerFactory.getLogger(MailServiceImpl.class);
	
	@Autowired TemplateMailSender mailSender;
	@Autowired MessageSource messageSource;
	@Value("${inquiryNotificationEmail}") String to;
	
	private static final String NEW_INQUIRY_TEMPLATE = "mailTemplates/newInquiry.vm";
 	
	@Override
	public void sendToken(UserDetails userDetails, String email, Token token, Hints hints) {
		// not used
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void notifyNewInquiry(final Inquiry inquiry) {
		Assert.notNull(inquiry, "Cannot perform: 'notifyNewInquiry' - inquiry is null.");
		log.debug("Sending new inquiry notification; inquiry: {}", inquiry);
		Map model = new HashMap();
		model.put("inquiry", inquiry);
		String subject =  "kaceni.cz - nová poptávka";
		try {
			log.debug("Sending new inquiry notification email to: {}", to);
			sendMessage(to, subject, model, NEW_INQUIRY_TEMPLATE);
		} catch (Exception e) {
			log.error("Sending 'notifyNewInquiry' failed for inquiry: " + inquiry, e);
		}
	}
		
	@SuppressWarnings("rawtypes")
	private void sendMessage(final String to, final String subjectKey, final Map model, final String template) {
		String subject = messageSource.getMessage(subjectKey, null, LocaleContextHolder.getLocale());
		mailSender.sendMessage(to, subject, model, template);
	}

}
