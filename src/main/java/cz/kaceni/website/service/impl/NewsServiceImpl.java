package cz.kaceni.website.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.kaceni.website.dao.NewsDao;
import cz.kaceni.website.model.News;
import cz.kaceni.website.model.enums.NewsType;
import cz.kaceni.website.service.NewsService;

@Service
public class NewsServiceImpl implements NewsService {

	@Autowired private NewsDao newsDao;
	
	@Override
	@Transactional
	public void saveNews(News news) {
		if (news.getId() == null) {
			newsDao.persist(news);
		} else {
			newsDao.merge(news);
		}
	}

	@Override
	@Transactional
	public void removeNews(Long newsId) {
		newsDao.removeById(newsId);
	}

	@Override
	@Transactional
	public List<News> listNews() {
		DetachedCriteria criteria = DetachedCriteria.forClass(News.class);
		criteria.add(Restrictions.eqOrIsNull("type", NewsType.NEWS));
		criteria.addOrder(Order.desc("date"));
		return newsDao.list(criteria);
	}
	
	@Override
	@Transactional
	public List<News> listEducationNews() {
		DetachedCriteria criteria = DetachedCriteria.forClass(News.class);
		criteria.add(Restrictions.eqOrIsNull("type", NewsType.EDUCATION));
		criteria.addOrder(Order.desc("date"));
		return newsDao.list(criteria);
	}

	@Override
	@Transactional
	public News getNews(Long newsId) {
		return newsDao.findRequired(newsId);
	}

}
