package cz.kaceni.website.service;

import java.util.UUID;

import org.springframework.data.domain.Pageable;

import cz.kaceni.website.model.Inquiry;
import cz.kaceni.website.model.PriceOffer;
import cz.kaceni.website.model.PriceOfferItem;
import cz.kaceni.website.model.enums.InquiryStateEnum;
import cz.kaceni.website.web.util.InquiryFilter;

public interface InquiryService {

	void saveInquiry(Inquiry inquiry);

	Inquiry findInquiry(UUID inquiryId);

	void removeInquiry(UUID inquiryId);

	void updateInquiryAdminNote(UUID id, String adminNote);

	Object getInquiryList(InquiryFilter inquiryFilter, Pageable pageable);

	void changeInquiryState(UUID inquiryId, InquiryStateEnum inquiryState);

	Inquiry findInquiryWithoutWorkList(UUID inquiryId);

	PriceOfferItem getPriceOfferItem(Long priceOfferItemId);

	void savePriceOfferItem(PriceOfferItem priceOfferItem, UUID inquiryId);

	void removePriceOfferItem(Long priceOfferItemId);

	PriceOffer findInquiryPriceOffer(UUID inquiryId);

	PriceOffer savePriceOffer(PriceOffer priceOfferData);

	PriceOffer getPriceOffer(Long priceOfferId);
	
	PriceOffer getPriceOfferForPrint(Long priceOfferId);

	void removePriceOffer(Long priceOfferId);

	void importInquiry(Inquiry inquiry);

	void importPriceOffer(Inquiry inquiry);

	
	
}
