/*
 * @author Pavla Novakova (pavla.novakova@gmail.com)
 */
/**
 * @file Global application namespace definition and initialization. 
 * <dl>
 * 	<dt>Pattern:</dt><dd>singleton</dd>
 * </dl>
 */
/**
 * @classdesc Creates global application namespace (wrapper for all js "packages" within the application).
 * <code>APP</code> is the only global js variable used by the application.
 * @class
 * @protected
 */
var APP_CLASS = function() {
	
	/**
	 * Creates a javascript package in the application namespace.
	 * 
	 * @example
	 * createPackage("service.validation.constraints"); 
	 * 
	 * @protected   
	 */	
	function createPackage(jspackage) {
		var parts = jspackage.split('.');
		var parent = APP;
		var currentPart = '';    
		for(var i = 0, length = parts.length; i < length; i++) {
		    currentPart = parts[i];
		    parent[currentPart] = parent[currentPart] || {};
		    parent = parent[currentPart];
		}
		return parent;
	};

	/**
	 * Creates all application javascript packages.
	 * @private
	 */
	this.initPackages = function() {
		createPackage("app");
		createPackage("apputils");
		createPackage("gui");
		createPackage("service");
		createPackage("service.fileupload");
		createPackage("service.validation");
		createPackage("service.validation.validators");
		createPackage("utils");
	};
		
	/**
	 * Checks weather the class name is already used within the package.</p>
	 * Use case: jsp page related javascript is usually wrapped in a standalone javascript class placed in a single package <code>APP.app</code>, 
	 * but javascript class files are located in different folders (similar to jsp structure) 
	 * under <code>/src/main/webapp/resources/js/app/pages</code> - it may lead to a name conflict (multiple javascript classes 
	 * of the same name in different folders but in single namespace <code>APP.app</code>).
	 * 
	 * @param {Object} packageObject package object, for example APP.app
	 * @param {string} className the simple name of the class to check 
	 * @return <code>true</code> if the class with given <code>className</code> is not defined within the given package
	 * 
	 * @example
	 * APP.canUse(APP.app, "PageChangePassword")
	 * APP.app.PageChangePassword = function() { ... }
	 */
	this.canUse = function(packageObject, className) {
		if (!packageObject) throw new Error("Argument 'packageObject' is required.");
		if (!className) throw new Error("Argument 'packageObject' is required.");
		if (typeof packageObject[className] === "undefined") return true;
		throw new Error("Class name: " + className + " is already used.");
	}; 
	
	/**
	 * Creates new class via <i>reflection</i>. 
	 * Use it only if you cannot create the class using <code>new MyClass(...)</code> constructor.
	 * Usage example: instantiation of validators by their name.
	 * 
	 * @param {string} packageName the name of the package, for example: <code>APP.service.validation.validators</code>
	 * @param {string} classSimpleName the name of the class to be created, for example: <code>NotNull</code>
	 * @param {Array} constructorArgs arguments of the constructor
	 * 
	 * @returns new instance of the class
	 */
	this.createNewInstanceViaReflection = function(packageName, classSimpleName, constructorArgs) {
		var classConstructor = eval(packageName + "." + classSimpleName + ".prototype.constructor");
		function Clazz() {
			return classConstructor.call(this, constructorArgs);
		}
		Clazz.prototype = classConstructor.prototype;
		return new Clazz();	
	};
};

var APP = new APP_CLASS();
APP.initPackages();