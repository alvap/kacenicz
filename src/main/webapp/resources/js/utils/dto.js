/*
 * @author Pavla Novakova (pavla.novakova@gmail.com)
 */
/**
 * @file Containers for passing parameters from JSP to related javascript <i>page class</i>.
 * <dl>
 * <dt>Pattern:</dt>
 * <dd>singleton</dd>
 * </dl>
 */
/**
 * @classdesc A wrapper around two key-value containers used for passing parameters from JSP to related javascript 'page class'.
 * @class
 * @tutorial passing-parameters-from-jsp
 * @protected
 */
APP.utils.DtoClass = function() {
	
	/** 
	 * The container for passing localized messages - key/value (string/string) pairs.
	 * Key naming convention: the key is equal to the one in the java resource bundle (or follows same naming conventions
	 * if the key is not present in the resource bundle).
	 * 
	 * @type Object 
	 */
	this.messages = {};
	
	/**
	 * The container for passing any kind of parameters (by convention except localized messages). 
	 * Key/value (string/any type) pairs. Key naming convention: camelcase.
	 * 
	 * @type Object
	 */
	this.params = {}
};

APP.utils.Dto = new APP.utils.DtoClass();