/*
 * @author Pavla Novakova (pavla.novakova@gmail.com)
 */
/**
 * @file Definition of application widgets like alert or confirmation dialog.
 * <dl>
 * <dt>Pattern:</dt>
 * <dd>singletons</dd>
 * <dt>Dependencies:</dt>
 * </dl>
 * 
 */
/**
 * @classdesc Dialog displaying Google map in readonly or edit mode.
 * use {@link APP.apputils.WidgetsUtilsClass#displayConfirmDialog}.
 * @class
 * @protected
 */
APP.gui.WidgetModalMapDialogClass = function() {
 
  /**
   * Customized Bootstrap modal dialog for displaying read only map.
   * @type jQuery 
   * @private
   */	
  var readOnlyMapDialog = null;
  /**
   * Customized Bootstrap modal dialog for displaying editable map.
   * @type jQuery 
   * @private
   */
  var editMapDialog = null;
   
  /**
   * Creates (hidden) in-memory readonly map dialog from internal HTML template.
   */
  function initReadOnlyMapDialog()  {
	readOnlyMapDialog = createModalMapDialog(false, true);
	readOnlyMapDialog.modal({show:false});
  }
  
  /**
   * Creates (hidden) in-memory editable map dialog from internal HTML template.
   */
  function initEditMapDialog () {
	editMapDialog = createModalMapDialog(true);
	editMapDialog = editMapDialog.modal({show:false, backdrop: 'static', keyboard: false});

  };
  
  /**
   * Displays read only map dialog.
   */
  this.displayReadOnlyMapDialog = function() {
	  if (!readOnlyMapDialog) initReadOnlyMapDialog();
	  readOnlyMapDialog.modal('show');
	  return readOnlyMapDialog;
  };
  
  /**
   * Displays editable map dialog.
   */
  this.displayEditMapDialog = function(headerTitleElement, okCallback) {
	  if (!editMapDialog) initEditMapDialog();
	  var header = getHeader(editMapDialog);
	  $('.header-text',header).remove();
	  headerTitleElement.addClass("header-text");
	  header.append(headerTitleElement); 
	  getSaveButton(editMapDialog).one("click", function() {
		  okCallback.resolve("save") }
	  );
	  editMapDialog.on('hidden.bs.modal', function(event) {
		  if (okCallback.state() == 'pending') okCallback.reject();
	  });
	  editMapDialog.modal('show');
	  return editMapDialog;
  };
  
  /**
   * Gets the dialog header.
   * @returns {jQuery} the text holder
   * @private
   */
  function getHeader(dialog) {
	  return $('.modal-header',dialog);
  }
  
  /**
   * Gets the 'ok' button from the component's HTML template.
   * @returns {jQuery} ok button
   * @private
   */
  function getSaveButton(dialog) {
	  return $('button.btn-primary',dialog);
  }
  
  /**
   * Creates modal dialog HTML structure.
   * 
   * @param {boolean} includeFooter buttons Save/Cancel are generated if set to true
   * @param {boolean} includeDismissBtn dismiss button is generated if set to true
   */
  function createModalMapDialog(includeFooter, includeDismissBtn) {
	  
	  var modalPane = $("<div>", {'class':'modal', tabindex:'-1'});
	  var modalPaneInner = $("<div>", {'class': 'modal-dialog map-modal-dialog'});
	  var modalContent = $("<div>", {'class': 'modal-content'});
	  var modalHeader=$("<div>", {'class': 'modal-header'});
	  if (includeDismissBtn) {
		  var dismissButton = $("<button>", {type:"button", 'class':"close", 'data-dismiss':"modal"}).html("&times;");
		  modalHeader.append(dismissButton);
	  }
	  // custom modal header text 
	  var modalBody = $("<div>", {'class': 'modal-body'});
	  var modalFooter = $("<div>", {'class' : 'modal-footer'});
	  var btnSave = $("<button>", {type: 'button', 'class': 'btn btn-primary', 'data-dismiss': 'modal'}).text("Uložit");
	  var btnClose = $("<button>", {type: 'button', 'class': 'btn btn-default', 'data-dismiss': 'modal'}).text("Zavřít/neukládat");
	  modalContent.append(modalHeader);
	  modalContent.append(modalBody);
	  if (includeFooter) {
		  modalFooter.append(btnSave);
		  modalFooter.append(btnClose);
		  modalContent.append(modalFooter);
	  }
	  modalPaneInner.append(modalContent);
	  modalPane.append(modalPaneInner);
	  return modalPane;
  };
};

/**
 * Modal dialog with 'loading' info message and progress bar supplied as loading gif. Adjusts default Bootstrap modal backdrop.
 * @param jQuery loadingGif loading img HTML element
 */
APP.gui.WidgetModalLoadingDialogClass = function(loadingGif) { 

	var loadingDialog = null;
	
	this.init = function() {
		loadingDialog = createLoadingModalDialog();
		loadingDialog.modal({show: false});
	}
	
	this.show = function() {
		if (loadingDialog == null) this.init();
		loadingDialog.modal('show');
		loadingDialog.data('bs.modal').$backdrop.css('background-color','#fff');
		return loadingDialog;
	}
	
	this.hide = function() {
		loadingDialog.modal('hide');
		return loadingDialog;
	}
	
	function createLoadingModalDialog() {
	  var modalPane = $("<div>", {'class':'modal loading-modal', tabindex:'-1'});
	  var verticalAlignmentWrapper = $("<div>", {'class': 'vertical-alignment-wrapper'});
	  var modalPaneInner = $("<div>", {'class': 'modal-dialog vertical-align-center'});
	  var modalContent = $("<div>", {'class': 'modal-content'});
	  var modalBody = $("<div>", {'class': 'modal-body'});
	  modalBody.append(loadingGif);
	  modalContent.append(modalBody);
	  modalPaneInner.append(modalContent);
	  verticalAlignmentWrapper.append(modalPaneInner);
	  modalPane.append(verticalAlignmentWrapper);
	  return modalPane;
  };
  
  
};