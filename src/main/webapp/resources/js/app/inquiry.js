$(function(){ 

	// activate upload photo link / create picker
	$("[href^='#upload-photo']").on('click', function(event) {
			var thisLink = $(this);
			APP.utils.Dto.params = $.extend(APP.utils.Dto.params, {
					parentFolderName: $("[name=id]").val(),
					subFolderName: thisLink.data("target-type"),
					imgsContainerId: "uploaded-photos-" + thisLink.data("target-type"),
					uploadButton: thisLink,
					photoListPath: thisLink.attr("data-photoList-path"),
					workImageFolderIdField: $("[name='" + thisLink.attr("data-imageFolderId-path") + "']")});
			var uploader = new APP.app.GooglePickerFileUploader(APP.utils.Dto.params);
			uploader.createPicker();
		});
	
	$("[href^='#remove-img']").each(function(index,item) {
		var removeLink = $(this);
		var uploadButton = $("[href^='#upload-photo']", removeLink.closest('fieldset'));
		var maxFilesPersistent = uploadButton.attr("data-max-files");
		if (!$.isNumeric(maxFilesPersistent)) maxFilesPersistent = 5;
		uploadButton.attr("data-max-files", --maxFilesPersistent);
		var fileId = removeLink.attr("data-file-id");
		removeLink.on("click" ,function(event) {
			if (!APP.app.GooglePickerFileUploader.pickerInitialized || !APP.app.GooglePickerFileUploader.driveInitialized) {
				alert("Na stránce se vyskytla chyba, zkuste, prosím, fotografii smazat znovu.");
			} else {
				gapi.auth.setToken({"access_token": APP.utils.Dto.params.accessToken});
				gapi.client.drive.files['delete']({fileId:fileId}).then(
					function(response) {
						$("a[data-file-id=" + fileId + "]").parent().remove();
						var maxFiles = uploadButton.attr("data-max-files");
						console.log("currentMaxFiles: " + maxFiles);
						if ($.isNumeric(maxFiles) && maxFiles < 5) uploadButton.attr("data-max-files", ++maxFiles);
						var photoListPath = uploadButton.data("target-path");
						var photoListField = $("[name='" + photoListPath + "']");
						var photoList = $.parseJSON(photoListField.val());
						var newPhotoList = [];
						for (var i=0; i<photoList.length; i++) {
							if (photoList[i].id != fileId) newPhotoList.push(photoList[i]);
						}
						photoListField.val(newPhotoList.length == 0 ? "" : JSON.stringify(newPhotoList));
					},
					function(error) {
						alert("Na stránce se vyskytla chyba.\n" + error.status + "\n" + error.body);
					}
				);
			}
		});
	});
	
	
	// activate mark location on map button
	$("[href^='#gps-location']").on('click', function(event) {
		/** @type APP.apputils.WidgetsUtilsClass */
		var WidgetsUtils = APP.apputils.WidgetsUtils; 
		var headerTitleElement = $("<p>").text("Kliknutím označte polohu na mapě (červená značka). Každé další kliknutí na jiné místo v mapě přesune značku do tohoto místa. Kliknutím přímo na značku značku smažete.");
		var okDeferred = $.Deferred();
		var dialog = WidgetsUtils.displayEditMapDialog(headerTitleElement, okDeferred);
		var mapCanvas = $(".modal-body", dialog);
		mapCanvas.height(window.innerHeight * 0.8);
		var fieldName = $(this).data("target-path");
		var gpsLocationField = $("[" + "name='" + fieldName + "']");
		var gpsLocation = gpsLocationField.val();
		var markerLocation = null;
		if (gpsLocation) {
			var latLng = gpsLocation.split(",");
			markerLocation = new google.maps.LatLng(latLng[0],latLng[1]);
		}
		/** @type APP.app.GoogleStreetViewLocator */
		var mapLocator = new APP.app.GoogleStreetViewLocator(mapCanvas,markerLocation,true);
		mapLocator.openMap(mapCanvas);
		okDeferred.then(function(data){
			var latLng = mapLocator.getMarkerLocation(); 
			var field = $("[" + "name='" + fieldName + "']");
			field.val( latLng ? latLng[0] + "," + latLng[1] : "");
			var link = $(event.target);
			if (latLng && $(".glyphicon-ok", link).length == 0) {
				link.append($("<i>", {'class': 'glyphicon glyphicon-ok'}));
			} else if (!latLng) {
				$(".glyphicon-ok", link).remove();
			}
		});
	});
	
	// billing data collapse functionality
	var billingData = $("#billing-data");
	var requiresBillRadio = $("#requiresBill2"); 
	requiresBillRadio.on('click', function(event) {
		if (!billingData.hasClass('in')) {
			billingData.addClass('in');
			billingData.show();
		}
	});
	if (requiresBillRadio.is(":checked")) {
		billingData.addClass("in");
		billingData.show();
	} else {
		billingData.hide();
	}
	$("#requiresBill1").on('click', function(event) {
		if (billingData.hasClass('in')) {
			billingData.removeClass('in');
			billingData.hide();
		}
	});
	
	$("[data-toggle=collapse]").each(function(index, item) {
		var elem = $(item);
		if (elem.is('input') && elem.is(':checked')) {
			$(elem.data("target")).addClass("in");
		}
	});
			
});