/*
 * @author Pavla Novakova (pavla.novakova@gmail.com)
 */
/**
 * @file File uploader using Google Picker API.
 * <dl>
 * <dt>Pattern:</dt>
 * <dd>class</dd>
 * <dt>Dependencies:</dt>
 * </dl>
 */
/**
 * @classdesc File uploader using Google Picker API. Uploads files to application owned Google Account Drive storage. Given instance is bound to a single Work.
 * @class
 * 
 * @param {options} instance options
 */
APP.app.GooglePickerFileUploader = function(options) {
	// root image folder in Google Drive
	this.rootFolderId = options.rootFolderId;
	// inquiry folder name (database id)
	this.parentFolderName = options.parentFolderName;
	// inquiry folde Google Drive id
	this.parentFolderId = null;
	// workType folder name
	this.subFolderName = options.subFolderName;
	// workType folder Google Drive id
	this.subFolderId = null;
	// div where uploaded images are displayed
	this.imgsContainer = $("#" + options.imgsContainerId);
	// oauth token taken from server side
	this.accessToken = options.accessToken;
 	this.uploadButton = options.uploadButton;
 	// photo list form path 
 	this.imageFolderIdField = options.imageFolderIdField;
 	this.workImageFolderIdField = options.workImageFolderIdField;
 	this.photoListField = $("[name='" + options.photoListPath + "']");
 	// path to loading gif
 	this.loadingGif = $("<img>", {src:options.loadingGif});
}

//static variables
APP.app.GooglePickerFileUploader.pickerInitialized = false;
APP.app.GooglePickerFileUploader.driveInitialized = false;
APP.app.GooglePickerFileUploader.gapiLoaded = false;
APP.app.GooglePickerFileUploader.gclientLoaded = false;


// static functions (google js libs callback functions)
function APP_app_GooglePickerFileUploader_onGapiLoaded() {
	APP.app.GooglePickerFileUploader.gapiLoaded = true;
	gapi.load("picker", {"callback" : function() { 
		APP.app.GooglePickerFileUploader.pickerInitialized = true;
	}});
}

function APP_app_GooglePickerFileUploader_onGclientLoaded() {
	APP.app.GooglePickerFileUploader.gclientLoaded = true;
	gapi.client.setApiKey("AIzaSyA7UYx2oC_P2zL4h8yU1_9Q9ngm0DfTKPg");
	gapi.client.load('drive', 'v3').then(function() {
		APP.app.GooglePickerFileUploader.driveInitialized = true;
	});
}



APP.app.GooglePickerFileUploader.prototype.createPicker = function() {
	if (!APP.app.GooglePickerFileUploader.pickerInitialized || !APP.app.GooglePickerFileUploader.driveInitialized) {
		alert("Komponenta pro nahrávání fotografií není připravena. Zkuste provedenou akci provést, prosím, znovu.");
	} else {
		gapi.auth.setToken({"access_token": this.accessToken});
		this.loadingDialog = new APP.gui.WidgetModalLoadingDialogClass(this.loadingGif);
		this.loadingDialog.show();
		this.beforeCreatePicker();
	}
}

/**
 * Creates / checks required directory structure in Google Drive and delegates to file picker building routine.
 */
APP.app.GooglePickerFileUploader.prototype.beforeCreatePicker = function() {
	this.createFolderIfNotExists(this.parentFolderName, this.rootFolderId, $.proxy(function(folderId) {
		this.imageFolderIdField.val(folderId);
		this.createFolderIfNotExists(this.subFolderName, folderId, $.proxy(this.doCreatePicker, this));
	}, this));
}

/**
 * Creates folder if folder with given name doesn't exist.
 * 
 * @param {String} folderName the name of the folder to be created 
 * @param {String} parentFolderId the Google Drive file id of the parent folder 
 * @param {function} callback the callback called when folder is available (exits or is created), passed parameters: created or existing folder id
 */
APP.app.GooglePickerFileUploader.prototype.createFolderIfNotExists = function(folderName, parentFolderId, callback) {
	var _this = this;
	// search for folder
	var query = "name = '" + folderName + "' and '" + parentFolderId + "' in parents";
	gapi.client.drive.files.list({pageSize: 5, fields: "files(id)", q: query})
	.then(
		// search successful	
		$.proxy(function(response) {
			// folder doesn't exits -> create folder
			if (response.result.files.length == 0) {
				  var data = {name: folderName, parents: [parentFolderId], mimeType: "application/vnd.google-apps.folder"};
				  gapi.client.drive.files.create({'resource': data}).then(
						  // folder created -> return id
						  function(response) { callback(response.result.id); },
						  $.proxy(function(reason) { this.handleError(reason); }, _this)
				  );		  
			} 
			// folder exists -> return id
			else {
				callback(response.result.files[0].id);
			}
			
		},_this),
		$.proxy(function(reason) { this.handleError(reason); }, _this)
	);		
}

/**
 * Handles Google Drive API call error.
 * @param error Google Drive error response.
 */
APP.app.GooglePickerFileUploader.prototype.handleError = function(error) {
	alert("Na stránce se vyskytla chyba.\n" + error.status + "\n" + error.body);
	if (this.loadingGif != null) this.loadingGif.hide();
}

/**
 * Creates files picker component.
 * 
 * @param parentId Google file id of a folder within Google Drive where files are to be uploaded.
 */
APP.app.GooglePickerFileUploader.prototype.doCreatePicker = function(parentId) {
		this.subFolderId = parentId;
		this.workImageFolderIdField.val(parentId);
		var uploadView = new google.picker.DocsUploadView();
		uploadView.setParent(parentId);
		var maxFiles = this.uploadButton.data("maxFiles");
		var picker = new google.picker.PickerBuilder().
					        addView(uploadView).
					        setOAuthToken(this.accessToken).
					        setCallback($.proxy(this.onFilePicked,this)).
					        setLocale('cs').
					        setMaxItems($.isNumeric(maxFiles) ? maxFiles : 5).
					        enableFeature(google.picker.Feature.MULTISELECT_ENABLED).
					        build();
		this.loadingDialog.hide();
		picker.setVisible(true);
}

/**
 * Callback when uploading selected files is finished.
 * 
 * @param data Google response
 */
APP.app.GooglePickerFileUploader.prototype.onFilePicked = function(data) {
	if (data[google.picker.Response.ACTION] != google.picker.Action.PICKED) return;
	var _this = this;
	// display all files in given folder
	var query = "'" + this.subFolderId + "' in parents";
    gapi.client.drive.files.list({pageSize: 5, fields: "files(id,thumbnailLink,webContentLink,originalFilename)", q: query})
		.then(
		    $.proxy(function(response) {
		    	var files = response.result.files;
		    	if (files.length === 0) return;
		    	var noOfFiles = files.length;
		    	this.imgsContainer.empty(); // remove all elements before refresh
		    	for (var i=0; i<noOfFiles; i++) {
		    		this.appendThumbnail(files[i]);
		    	}
		    	this.uploadButton.attr("data-max-files", 5 - noOfFiles);
		    	this.photoListField.val(JSON.stringify(files));
		    }, _this),
		    $.proxy(function(reason) { this.handlerError(reason);}, _this));
}

/**
 * Displays google drive file thumbnail.
 * 
 * @param fileResource Google File Resource
 */
APP.app.GooglePickerFileUploader.prototype.appendThumbnail = function(fileResource) {
	var p = $("<p>");
	var img = $("<span>", {'class': 'img'});
	img.css({'background-image': "url('" + fileResource.thumbnailLink + "')"});
	var removeLink = $("<a>", {href: '#remove-img', 'data-file-id': fileResource.id, title:'Smazat fotografii'}).html("<i class='glyphicon glyphicon-trash'><span class='sr-only'>Smazat fotografii</span></i> " + fileResource.originalFilename);
	removeLink.on("click", $.proxy(function(event) {
		this.removeFile(fileResource.id);
	}, this));
	p.append(img);
	p.append(removeLink);
	this.imgsContainer.append(p);
}

/**
 * Remove file handler.
 */
APP.app.GooglePickerFileUploader.prototype.removeFile = function(fileId) {
	var _this = this;
	gapi.client.drive.files['delete']({fileId:fileId}).then(
		$.proxy(function(response) {
			$("a[data-file-id=" + fileId + "]").parent().remove();
			var maxFiles = this.uploadButton.attr("data-max-files");
			if ($.isNumeric(maxFiles) && maxFiles < 5) this.uploadButton.attr("data-max-files", ++maxFiles);
			var photoList = $.parseJSON(this.photoListField.val());
			var newPhotoList = [];
			for (var i=0; i<photoList.length; i++) {
				if (photoList[i].id != fileId) newPhotoList.push(photoList[i]);
			}
			this.photoListField.val(newPhotoList.length == 0 ? "" : JSON.stringify(newPhotoList));
		},_this),
		$.proxy(function(reason) {
			this.handleError(reason);
		},_this)
	);
}


