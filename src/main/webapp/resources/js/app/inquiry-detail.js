$(function(){ 
	// activate mark location on map button
	$("[href^='#gps-location']").on('click', function(event) {
		/** @type APP.apputils.WidgetsUtilsClass */
		var WidgetsUtils = APP.apputils.WidgetsUtils; 
		var dialog = WidgetsUtils.displayReadOnlyMapDialog();
		var mapCanvas = $(".modal-body", dialog);
		mapCanvas.height(window.innerHeight * 0.8);
		var gpsLocation = $(this).data("location");
		var markerLocation = null;
		if (gpsLocation) {
			var latLng = gpsLocation.split(",");
			markerLocation = new google.maps.LatLng(latLng[0],latLng[1]);
		}
		/** @type APP.app.GoogleStreetViewLocator */
		var mapLocator = new APP.app.GoogleStreetViewLocator(mapCanvas,markerLocation,false);
		mapLocator.openMap(mapCanvas);
	});
});