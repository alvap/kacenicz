/*
 * @author Pavla Novakova (pavla.novakova@gmail.com)
 */
/**
 * @file Location editor component (uses Google Maps API).
 * <dl>
 * <dt>Pattern:</dt>
 * <dd>class</dd>
 * <dt>Dependencies:</dt>
 * </dl>
 */
/**
 * @classdesc Enables to mark / display a position (of a tree) on a map using Google Map API.
 * 
 * @param mapCanvas {jQuery}
 * @param markerLocation {google.maps.LatLng current persistent location}
 * @param edit {boolean} true if edit mode, false if read only (display)
 */
APP.app.GoogleStreetViewLocator = function(mapCanvas, markerLocation, edit) {
	this.markerLocation = markerLocation;
	this.mapCanvas = mapCanvas;
	this.edit = edit;
	/** @type google.maps.Marker */
	this.marker = null;
}

/** @type boolean 
 * 'true' if google maps api is already loaded ('static variable'). 
*/
APP.app.GoogleStreetViewLocator.mapsLoaded = false;
/**
 * @type function
 * Function called when google maps API is loaded.
 */
APP.app.GoogleStreetViewLocator.mapsLoadedCallback = function() { APP.app.GoogleStreetViewLocator.mapsLoaded = true; }

/**
 * Tries to open map (checks precondition that Google Maps API is already loaded) and delegates 
 * to #doOpenMap.
 */
APP.app.GoogleStreetViewLocator.prototype.openMap = function() {
	if (!APP.app.GoogleStreetViewLocator.mapsLoaded) {
		alert("Mapa ještě není nahrána, zkuste to, prosím, za chvíli znovu.");
	} else {
		this.doOpenMap();
	}
}
/**
 * Based on settings opens editable or read only map.
 */
APP.app.GoogleStreetViewLocator.prototype.doOpenMap = function() {
	if (this.edit) {
		this.editLocation();
	} else {
		this.displayLocation();
	}
}

/**
 * Displays location using street view (if street view is available, otherwise map view is used).
 */
APP.app.GoogleStreetViewLocator.prototype.displayLocation = function() {

	var map = new google.maps.Map(this.mapCanvas[0], {
        center: this.markerLocation,
        zoom: 12, // city
        mapTypeId: google.maps.MapTypeId.HYBRID,
        mapTypeControl: false
   });

	var marker = new google.maps.Marker({ position: this.markerLocation, map: map });	
//   var panorama = new google.maps.StreetViewPanorama(this.mapCanvas[0], {position: this.markerLocation });

//   var heading = google.maps.geometry.spherical.computeHeading(panorama.getPosition(),this.markerLocation);
//   panorama.setPov({ heading: heading,  pitch: 0, zoom: 1 });
//   
//   var streetViewService = new google.maps.StreetViewService();
//   var STREETVIEW_MAX_DISTANCE = 100;
//   var _this = this;
//   streetViewService.getPanoramaByLocation(this.markerLocation, STREETVIEW_MAX_DISTANCE, function (streetViewPanoramaData, status) {
//	    if (status === google.maps.StreetViewStatus.OK) {
//	    	panorama.setVisible(true);
//	    } 
//	});
}

/**
 * Displays map for entering location as marker (placed by click on a map)
 */
APP.app.GoogleStreetViewLocator.prototype.editLocation = function() {
	
	var mapCenter = this.markerLocation;
	if (!mapCenter) {
		mapCenter = new google.maps.LatLng(49.205712,16.608826); // Luzanecky park, Brno
	}
	var map = new google.maps.Map(this.mapCanvas[0], {
         center: mapCenter,
         zoom: 12, // city
         mapTypeId: google.maps.MapTypeId.HYBRID,
         mapTypeControl: false,
		 streetViewControl: false
    });
	
	if (this.markerLocation) {
		this.placeMarker(map, this.markerLocation, "add");		
	}

	map.addListener('click', $.proxy(function(e) {
		 var latLng = e.latLng;
		 if (this.markerLocation == null) {
			 this.placeMarker(map, latLng, 'create');
		 } else {
			 this.placeMarker(map, latLng, 'move');
		 }
		 map.panTo(latLng);
	 }, this));
	
}

// operation: create, add, move
APP.app.GoogleStreetViewLocator.prototype.placeMarker = function(map, markerLocation, operation) {
	
	if (operation == 'create' || operation == 'add') {
	     var marker = new google.maps.Marker({ position: markerLocation,  map: map });
	     marker.addListener('click', $.proxy(function(e) {
				marker.setMap(null);
				this.marker = null;
				this.markerLocation = null;
		}, this));
	     this.marker = marker;
	} else { // move
		this.marker.setPosition(markerLocation);
	}
	this.markerLocation = markerLocation;
}

/**
 * @returns current location of marker in a map
 */
APP.app.GoogleStreetViewLocator.prototype.getMarkerLocation = function() {
	if (this.markerLocation) return [this.markerLocation.lat(), this.markerLocation.lng()];
	return null;
}

