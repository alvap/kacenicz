/*
 * @author Pavla Novakova (pavla.novakova@gmail.com)
 */
/**
 * @file Javascript constants passed from JSP (and shared across  all javascript files).
 * <dl>
 * 	<dt>Pattern:</dt><dd>singleton</dd>
 * </dl>
 */
/**
 * @classdesc A wrapper for javascript constants passed from JSP (and shared across all javascript files).
 * @class
 * @protected
 */
APP.apputils.ConstantsClass = function() {
	
	/**
	 * The value of the web application context path.
	 * @type string 
	 */
	this.CONTEXT_PATH = null;
	
};
APP.apputils.Constants = new APP.apputils.ConstantsClass();