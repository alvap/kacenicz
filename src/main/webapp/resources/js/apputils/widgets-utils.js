
/*
 * @author Pavla Novakova (pavla.novakova@gmail.com)
 */
/**
 * @file Utility methods for invoking application widgets like alerts or confirmation dialogs.
 * <dl>
 * 	<dt>Pattern:</dt><dd>singleton</dd>
 *  <dt>Dependencies:</dt>
 *  <dd>APP.gui.Widget*</dd>
 * </dl>
 */
/**
 * @classdesc Provides simple methods for displaying application widgets like alerts or confirmation dialogs. 
 * Prefer using these utility methods over direct instantiation of widgets since this class is also responsible 
 * for displaying single alert or confirmation dialog at time and reuses existing instances of widgets. 
 * @class
 * @protected
 */
APP.apputils.WidgetsUtilsClass = function() {
	
	/** @type APP.gui.WidgetModalMapDialogClass */ 
	var modalMapDialog = null;
	
	/**
	 * Displays alert.
	 * @param {string} message localized message to display
	 * @param {Object} options for options see {@link APP.gui.WidgetAlertClass#display} method 
	 */
	this.displayReadOnlyMapDialog = function() {
		if (!modalMapDialog) { modalMapDialog = new APP.gui.WidgetModalMapDialogClass();};
		return modalMapDialog.displayReadOnlyMapDialog();
	};
	
	/**
	 * Displays confirmation dialog.
	 * @param {string} message localized message to display
	 * @param {Object} options for options see {@link APP.gui.WidgetConfirmDialogClass#display} method 
	 */
	this.displayEditMapDialog = function(headerTitleElement, okCallback) {
		if (!modalMapDialog) { modalMapDialog = new APP.gui.WidgetModalMapDialogClass();};
		return modalMapDialog.displayEditMapDialog(headerTitleElement, okCallback)
	};
	
};

APP.apputils.WidgetsUtils = new APP.apputils.WidgetsUtilsClass();