<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp"%>
<tiles:insertDefinition name=".layout.exception">
	<spring:message code="exception.defaultError.headline" var="h" />
	<tiles:putAttribute name="title" type="string">${h}</tiles:putAttribute>
    <tiles:putAttribute name="headlineText" type="string" cascade="true">${h}</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<div class="container container-error">
			<p class="alert alert-danger"><spring:message code="exception.contact" /></p>
			<p>${exception}</p>
			<p><a href='<c:url value="/" />' class="btn btn-primary"><spring:message code="exception.text.continueOnHomePage" /></a></p>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>