<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="news" scope="request" />
<tiles:insertDefinition name=".layout.public.pages">
	<tiles:putAttribute name="title">Novinky - Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="headlineText" cascade="true">Novinky</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<c:choose>
				<c:when test="${!empty newsList}">
					<dl class="news">								
						<c:forEach items="${newsList}" var="news">
							<dt><joda:format value="${news.date}"/></dt>
							<dd><c:out value="${news.text}" escapeXml="false" /></dd>
						</c:forEach>
					</dl>	
				</c:when>
				<c:otherwise>
					<p class="perex">Žádné novinky nejsou k dispozici.</p>
				</c:otherwise>
			</c:choose>	
			<div class="row photos">
		      <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-novinky.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-novinky.jpg' />" alt="Kácení Šarapatka - novinky" />
			    </a>
			  </div>	
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-novinky-1.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-novinky-1.jpg' />" alt="Kácení Šarapatka - novinky - 1" />
			    </a>
			  </div>
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-novinky-2.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-novinky-2.jpg' />" alt="Kácení Šarapatka - novinky - 2" />
			    </a>
			  </div>
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-novinky-3.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-novinky-3.jpg' />" alt="Kácení Šarapatka - novinky - 3" />
			    </a>
			  </div>
			</div>       
	    </div>
		</div>
    </tiles:putAttribute>
</tiles:insertDefinition>