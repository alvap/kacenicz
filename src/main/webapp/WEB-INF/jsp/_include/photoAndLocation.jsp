<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:if test="${!empty work.gpsLocation}">
	<p><b>Poloha:</b> <a href="#gps-location" data-toggle="modal" data-location="${work.gpsLocation}">Zobrazit mapu</a></p>
</c:if>
<c:if test="${!empty work.photoList}">
	<div class="popup-gallery">
		<c:forEach items="${work.photoList}" var="photo">
			<a href="${photo.webContentLink}"><img src=" https://drive.google.com/thumbnail?sz=h220&id=${photo.id}" /></a>	
		</c:forEach>
	</div>
</c:if>
