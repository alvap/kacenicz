<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>

<table class="table table-bordered">
<thead>
	<tr>
		<th>Název</th>
		<th>Mn.</th>
		<th>MJ</th>
		<th>Typ<br/>zásahu</th>
		<th>Cena<br/>štěpkování</th>
		<th>Cena<br/>likvidace<br/>dřeva</th>
		<th>Cena<br/>ořezu</th>
		<th>Cena<br/>kácení</th>
		<th>Cena<br/>frézování<br/>pařezu</th>
		<th>Cena<br/>vazby</th>
		<th>Cena<br/>ostatní</th>
		<th>Cena<br/>celkem<br/>bez DPH</th>
		<c:if test="${param.showVatRate}">
			<th>Sazba<br/>DPH</th>
		</c:if>
		<c:if test="${param.edit}">
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</c:if>
	</tr>
</thead>	
<tbody>								
	<c:forEach items="${priceOffer.priceOfferItemList}" var="priceOfferItem" varStatus="i">
		<tr>
			<td><c:out value="${priceOfferItem.name}" /></td>
			<td><f:formatNumber maxFractionDigits="2" value="${priceOfferItem.amount}"/></td>
			<td><c:out value="${priceOfferItem.amountUnit}" /></td>
			<td><c:out value="${priceOfferItem.type}" /></td>
			<td>
				<c:if test="${priceOfferItem.priceChipping.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOfferItem.priceChipping}"/>&nbsp;Kč
				</c:if>
			</td>
			<td>
				<c:if test="${priceOfferItem.priceDisposal.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOfferItem.priceDisposal}"/>&nbsp;Kč
				</c:if>	
			</td>
			<td>
				<c:if test="${priceOfferItem.priceTrimming.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOfferItem.priceTrimming}"/>&nbsp;Kč
				</c:if>	
			</td>
			<td>
				<c:if test="${priceOfferItem.priceFelling.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOfferItem.priceFelling}"/>&nbsp;Kč
				</c:if>	
			</td>
			<td>
				<c:if test="${priceOfferItem.priceStumpsMilling.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOfferItem.priceStumpsMilling}"/>&nbsp;Kč
				</c:if>	
			</td>
			<td>
				<c:if test="${priceOfferItem.priceBinding.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOfferItem.priceBinding}"/>&nbsp;Kč
				</c:if>	
			</td>
			<td>
				<c:if test="${priceOfferItem.priceOther.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOfferItem.priceOther}"/>&nbsp;Kč
				</c:if>	
			</td>
			<td>
				<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOfferItem.priceWithoutVat}"/>&nbsp;Kč
			</td>
			<c:if test="${param.showVatRate}">
				<td>
					<c:if test="${priceOfferItem.vatRate > 0}">
						<c:out value="${priceOfferItem.vatRate}" />&nbsp;%
					</c:if>
				</td>
			</c:if>
			<c:if test="${param.edit}">
				<td>
					<a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiryId}/uprava-polozky-cenove-nabidky/${priceOfferItem.id}' />" title="Upravit položku cenové nabídky" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"><span class="hidden">Upravit</span></i></a>
				</td>
				<td>
					<a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiryId}/smazat-polozku-cenove-nabidky/${priceOfferItem.id}' />" title="Smazat položku cenové nabídky" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-remove"><span class="hidden">Smazat</span></i></a>
				</td>
			</c:if>	
		</tr>
	</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">Celkem</td>
			<td>
				<c:if test="${priceOffer.totalPriceChipping.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceChipping}"/>&nbsp;Kč
				</c:if>
			</td>
			<td>
				<c:if test="${priceOffer.totalPriceDisposal.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceDisposal}"/>&nbsp;Kč
				</c:if>
			</td>
			<td>
				<c:if test="${priceOffer.totalPriceTrimming.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceTrimming}"/>&nbsp;Kč
				</c:if>
			</td>
			<td>
				<c:if test="${priceOffer.totalPriceFelling.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceFelling}"/>&nbsp;Kč
				</c:if>
			</td>
			<td>
				<c:if test="${priceOffer.totalPriceStumpsMilling.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceStumpsMilling}"/>&nbsp;Kč
				</c:if>
			</td>
			<td>
				<c:if test="${priceOffer.totalPriceBinding.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceBinding}"/>&nbsp;Kč
				</c:if>
			</td>
			<td>
				<c:if test="${priceOffer.totalPriceOther.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceOther}"/>&nbsp;Kč
				</c:if>
			</td>
			<td>
				<c:if test="${priceOffer.totalPriceWithoutVat.unscaledValue() != 0}">
					<f:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceWithoutVat}"/>&nbsp;Kč
				</c:if>
			</td>
			<c:if test="${param.showVatRate}">
				<td>&nbsp;</td>
			</c:if>
			<c:if test="${param.edit}">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</c:if>
		</tr>
	</tfoot>
</table>	