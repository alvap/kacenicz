<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="serviceCols" value="col-sm-6 col-md-4" />
<div class="row theme-inverse" id="services-wrapper">
	<div class="${serviceCols}">
		<${param.h} id="odstranit">
			<a href="<c:url value='/sluzby/odstranit-strom' />" title="Kácení Šarapatka - odstranění stromu"><span></span></a>
			<a href="<c:url value='/sluzby/odstranit-strom' />" title="Kácení Šarapatka - odstranění stromu" class="link-text">Odstranit strom</a>
		</${param.h}>
	</div>
	<div class="${serviceCols}">
		<${param.h} id="vysadit">
			<a href="<c:url value='/sluzby/vysadit-strom' />" title="Kácení Šarapatka - vysazení stromu"><span></span></a>
			<a href="<c:url value='/sluzby/vysadit-strom' />" title="Kácení Šarapatka - vysazení stromu" class="link-text">Vysadit strom</a>
		</${param.h}>
	</div>
	<div class="${serviceCols}">
		<${param.h} id="orezat">
			<a href="<c:url value='/sluzby/orezat-strom' />" title="Kácení Šarapatka - ořez stromu"><span></span></a>
			<a href="<c:url value='/sluzby/orezat-strom' />" title="Kácení Šarapatka - ořez stromu" class="link-text">Ořezat strom</a>
		</${param.h}>
	</div>
	<div class="${serviceCols}">
		<${param.h} id="zabezpecit">
			<a href="<c:url value='/sluzby/zabezpecit-strom' />" title="Kácení Šarapatka - zabezpečení stromu"><span></span></a>
			<a href="<c:url value='/sluzby/zabezpecit-strom' />" title="Kácení Šarapatka - zabezpečení stromu" class="link-text">Zabezpečit strom</a>
		</${param.h}>
	</div>
	<div class="${serviceCols}">
		<${param.h} id="zhodnotit">
			<a href="<c:url value='/sluzby/zhodnotit-strom' />" title="Kácení Šarapatka - zhodnocení stromu"><span></span></a>
			<a href="<c:url value='/sluzby/zhodnotit-strom' />" title="Kácení Šarapatka - zhodnocení stromu" class="link-text">Zhodnotit strom</a>
		</${param.h}>
	</div>
	<div class="${serviceCols}">
		<${param.h} id="skoleni">
			<a href="<c:url value='/sluzby/skoleni-a-vzdelavani' />" title="Kácení Šarapatka - školení a vzdělávání"><span></span></a>
			<a href="<c:url value='/sluzby/skoleni-a-vzdelavani' />" title="Kácení Šarapatka - školení a vzdělávání" class="link-text">Školení a vzdělávání</a>
		</${param.h}>
	</div>
	<div class="${serviceCols}">
		<${param.h} id="frezovani">
			<a href="<c:url value='/sluzby/frezovani-parezu' />" title="Kácení Šarapatka - frézování pařezů"><span></span></a>
			<a href="<c:url value='/sluzby/frezovani-parezu' />" title="Kácení Šarapatka - frézování pařezů" class="link-text">Frézování pařezů</a>
		</${param.h}>
	</div>
</div>
	