<%-- jstl taglibs --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- spring taglibs --%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="spring-ext" uri="http://cz.devmint.spring-ext/tags" %>
<%@taglib prefix="spring-sec-ext" uri="http://cz.devmint.spring-ext/spring-security" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- spring security --%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%-- tiles taglibs --%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<%-- joda taglib --%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%-- custom util taglibs --%>
<%@taglib prefix="ui" uri="http://cz.devmint.spring-ext/ui-tags" %>
<%@taglib prefix="asset" uri="http://cz.devmint.spring-ext/asset-tags" %>
<%@taglib prefix="pagination" uri="http://cz.devmint.spring-ext/pagination-tags" %>

<%@taglib prefix="app" tagdir="/WEB-INF/tags" %>

