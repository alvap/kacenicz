<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<fieldset id="${param.workType}" class="theme-indent collapse">
   	<div class="form-group">
   		<spring-ext:label path="workList[${param.index}].numberOfItems" cssClass="control-label col-sm-2"/>
   		<div class="col-sm-2">
   			<spring-ext:input path="workList[${param.index}].numberOfItems" cssClass="form-control"/>
   		</div>
   		<c:if test="${param.workType == 'MULTIPLE_FELLING' || param.workType == 'MULTIPLE_RISK_FELLING'}">
	   		<spring-ext:label path="workList[${param.index}].additionalNumberOfItems" cssClass="control-label col-sm-2"/>
	   		<div class="col-sm-2">
	   			<spring-ext:input path="workList[${param.index}].additionalNumberOfItems" cssClass="form-control" />
	   		</div>
   		</c:if>
   		<c:if test="${param.workType == 'MILLING_OF_STUMPS'}">
   			<spring-ext:label path="workList[${param.index}].diameter" cssClass="control-label col-sm-2"/>
	   		<div class="col-sm-2">
	   			<spring-ext:input path="workList[${param.index}].diameter" cssClass="form-control" forceHelp="true" />
	   		</div>
   		</c:if>
	</div>
	<c:if test="${param.workType != 'PLANTING'}">
		<div class="form-group">
			<div class="col-sm-12">
				<p class="form-actions">
					<a href="#upload-photo" class="btn btn-default btn-sm" data-imageFolderId-path="workList[${param.index}].imageFolderId" data-photoList-path="workList[${param.index}].photoList" data-target-type="${param.workType}" >Nahrát fotografii (max 5)</a>
					<form:hidden path="workList[${param.index}].imageFolderId" />
					<form:hidden path="workList[${param.index}].photoList" />
					<a href="#gps-location" data-toggle="modal" data-target-path="workList[${param.index}].gpsLocation" class="btn btn-default btn-sm">Označit na Google Mapě </a>
					<form:hidden path="workList[${param.index}].gpsLocation" />
				</p>
			</div>
			<div class="col-sm-12">
				<div id="uploaded-photos-${param.workType}" class="img-container">
					<c:if test="${!empty inquiry.workList[param.index].photoList}">
						<c:forEach items="${inquiry.workList[param.index].photoList}" var="photo">
							<p>
								<span class="img" style="background-image: url('${photo.thumbnailLink}')"></span>
								<a href="#remove-img" data-file-id="${photo.id}" title="Smazat fotografii">
									<i class='glyphicon glyphicon-trash'><span>Smazat fotografii</span></i> ${photo.originalFilename}
								</a>
							</p>
						</c:forEach>
					</c:if>
				</div>
			</div>
		</div>
	</c:if>
</fieldset>					