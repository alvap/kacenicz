<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<fieldset id="${param.workType}" class="theme-indent collapse" data-path="workList[${param.index}]">
   	<div class="form-group">
   		<div class="col-lg-3 col-md-4 col-sm-5"><p><img src="<c:url value='/resources/imgs/strom.gif' />" alt="Rozměry stromu" class="img-responsive" /></p></div>
   		<div class="col-lg-4 col-md-5 col-sm-6">
    		<div class="row">
		    	<spring-ext:label path="workList[${param.index}].height" cssClass="control-label col-md-4 col-sm-5 col-xs-12"/>
		    	<div class="col-sm-6 col-md-5 col-xs-6">
		    		<p><spring-ext:inputGroup path="workList[${param.index}].height" cssClass="form-control" suffix="m" /></p>
				</div>
			</div>
			<div class="row">
				<spring-ext:label path="workList[${param.index}].girth" cssClass="control-label col-md-4 col-sm-5 col-xs-12"/>
				<div class="col-sm-6 col-md-5 col-xs-6">
					<p><spring-ext:inputGroup path="workList[${param.index}].girth" cssClass="form-control" suffix="cm" /></p>	
				</div>
			</div>
			<div class="row">
				<spring-ext:label path="workList[${param.index}].crownWidth" cssClass="control-label col-md-4 col-sm-5 col-xs-12" />
				<div class="col-sm-6 col-md-5 col-xs-6">
					<p><spring-ext:inputGroup path="workList[${param.index}].crownWidth" cssClass="form-control" suffix="m" /></p>
				</div>
			</div>
			<div class="row">
				<spring-ext:label path="workList[${param.index}].treeType" cssClass="control-label col-md-4 col-sm-5 col-xs-12" />
				<div class="col-sm-6 col-md-5 col-xs-6">
					<p><spring-ext:input path="workList[${param.index}].treeType" cssClass="form-control"/></p>
				</div>
			</div>
		</div>
		<div class="clearfix visible-sm-block visible-md-block"></div>
		<div class="col-lg-5 col-md-6 col-sm-7 reset-col">
			<p><spring-ext:label path="workList[${param.index}].itemsDescription" /></p>	
		    <spring-ext:textarea path="workList[${param.index}].itemsDescription" cssClass="form-control" rows="8" />
		</div>	
	</div>
	<div class="form-group">
		<div class="col-sm-12">
			<p class="form-actions">
				<a href="#upload-photo" class="btn btn-default btn-sm" data-imageFolderId-path="workList[${param.index}].imageFolderId" data-photoList-path="workList[${param.index}].photoList" data-target-type="${param.workType}" >Nahrát fotografii (max 5)</a>
				<form:hidden path="workList[${param.index}].imageFolderId" />
				<form:hidden path="workList[${param.index}].photoList" />
				<a href="#gps-location" data-toggle="modal" data-target-path="workList[${param.index}].gpsLocation" class="btn btn-default btn-sm">Označit na Google Mapě </a>
				<form:hidden path="workList[${param.index}].gpsLocation" />
			</p>
		</div>
		<div class="col-sm-12">
			<div id="uploaded-photos-${param.workType}" class="img-container">
				<c:if test="${!empty inquiry.workList[param.index].photoList}">
					<c:forEach items="${inquiry.workList[param.index].photoList}" var="photo">
						<p>
							<span class="img" style="background-image: url('${photo.thumbnailLink}')"></span>
							<a href="#remove-img" data-file-id="${photo.id}" title="Smazat fotografii">
								<i class='glyphicon glyphicon-trash'><span>Smazat fotografii</span></i> ${photo.originalFilename}
							</a>
						</p>
					</c:forEach>
				</c:if>
			</div>
		</div>
	</div>
</fieldset>					