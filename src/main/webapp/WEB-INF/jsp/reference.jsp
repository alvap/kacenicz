<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="portfolio" scope="request" />
<tiles:insertDefinition name=".layout.public.pages">
	<tiles:putAttribute name="title">Reference - Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="headlineText" cascade="true">Reference</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<app:inquiry-btn />
					<p class="perex">Seznam významných zákázek z oboru Arboristika lze rozdělit do několika sekcí:</p>
					<h2 class="theme">1. Komplexní revitalizace městské zeleně velkých měst</h2>
				</div>	
			</div>
			<div class="row">
				<div class="col-sm-6">
				    <dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Systémová úprava vegetace v Brně-Bystrci</dd>
				    	<dt>Investor:</dt><dd>Statutární město Brno,Dominikánské nám. 1,60167</dd>
				    	<dt>Kontakt:</dt><dd>Ing. Eva Kubátová, tel.&nbsp;605&nbsp;901&nbsp;650, vedoucí odboru ŽP, Brno Bystrc</dd>
				    	<dt>Doba plnění:</dt><dd>listopad 2009 – duben 2012</dd>
				    	<dt>Objem zakázky:</dt><dd>4 mil Kč</dd>
				    	<dt>Popis:</dt><dd>Komplexní revitalizace stromů, kácení , zdravotní a bezpečnostní řezy stromů, instalace bezpečnostních vazeb, výsadby stromů</dd>
				    	<!-- chybi foto -->
				    </dl>
				    <hr/>
			    </div>
			    <div class="col-sm-6">
				    <dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Regenerace zeleně města Prostějov</dd>
				    	<dt>Investor:</dt><dd>Statutární město Prostějov , odbor majetku a investic</dd>
				    	<dt>Kontakt:</dt><dd>Ing Simona Říhová 739 245 398 vedoucí odboru majetku a investic</dd>
				    	<dt>Objem zakázky:</dt><dd>250 tis Kč</dd>
				    	<dt>Doba plnění:</dt><dd>listopad 2012 – současnost</dd>
				    	<dt>Popis:</dt><dd>Dlouhodobá realizace arboristických zásahů včetně evidence stromů v systému <a href="https://www.stromypodkontrolou.cz/map/?draw_selection_circle=1#%7B%22zoom%22%3A19%2C%22lat%22%3A49.473761313187936%2C%22lng%22%3A17.11205805053601%7D">www.stromypodkontrolou.cz</a></dd>
				    </dl>
				    <hr class="visible-xs-block" />
				 </div>   
			  </div>
			  <div class="row">
			  	<div class="col-sm-6">
			    	<dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Revitalizace zeleně města Jeseník</dd>
				    	<dt>Investor:</dt><dd>Technické služby Jeseník</dd>
				    	<dt>Kontakt:</dt><dd><a href="mailto:zelen@tsje.cz">zelen@tsje.cz</a>, tel.&nbsp;737&nbsp;285&nbsp;531</dd>
				    	<dt>Objem zakázky:</dt><dd>4 mil Kč</dd>
				    	<dt>Doba plnění:</dt><dd>rok 2012 – 2013</dd>
				    	<dt>Popis:</dt><dd>Komplexní revitalizace stromů, kácení , zdravotní a bezpečnostní řezy stromů, instalace bezpečnostních vazeb, výsadby stromů. 
				    	<a href="https://www.stromypodkontrolou.cz/map/#%7B%22zoom%22%3A17%2C%22lat%22%3A50.22891476253815%2C%22lng%22%3A17.210621295861753%7D">www.stromypodkontrolou.cz</a></dd>
			    	</dl>
			    </div>	
			</div>
			<div class="row">	
				<div class="col-sm-12">    
			    	<h2 class="theme">2. Komplexní úprava městské zeleně menších měst a obcí</h2>
			    </div>
		    </div>
		    <div class="row">
		    	<div class="col-sm-6">
				    <dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Realizace pěstebních opatření města Modřice</dd>
				    	<dt>Investor:</dt><dd>město Modřice</dd>
				    	<dt>Kontakt:</dt><dd>Ing. Hana Chybíková - tel.&nbsp;537&nbsp;001&nbsp;013, 724&nbsp;767&nbsp;000, <a href="mailto:hana.chybikova@mesto-modrice.cz">hana.chybikova@mesto-modrice.cz</a></dd>
				    	<dt>Objem zakázky:</dt><dd>dlouhodobá realizace arboristických zásahů včetně evidence stromů v systému <a href=" http://www.stromypodkontrolou.cz/map/#%7B%22zoom%22%3A17%2C%22lat%22%3A49.1275151067636%2C%22lng%22%3A16.6146621098677%7D">www.stromypodkontrolou.cz</a></dd>
				    	<dt>Doba plnění:</dt><dd>rok 2012 – současnost</dd>
				    	<dt>Popis:</dt><dd>Komplexní úprava městské zeleně menšího města Modřice</dd>
				    </dl>
				    <hr class="visible-xs-block" />
				  </div>
				  <div class="col-sm-6">  
				    <dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Realizace pěstebních opatření obec Zbraslav u Brna</dd>
				    	<dt>Investor:</dt><dd>Zbraslav u Brna</dd>
				    	<dt>Kontakt:</dt><dd>starostka Ing. Jana Valová, <a href="mailto:starosta@zbraslavubrna.cz">starosta@zbraslavubrna.cz</a></dd>
				    	<dt>Doba plnění:</dt><dd>rok 2000 – současnost</dd>
				    	<dt>Popis</dt><dd>Viz <a href="http://www.stromypodkontrolou.cz/map/#%7B%22zoom%22%3A18%2C%22lat%22%3A49.2215531%2C%22lng%22%3A16.294151499999998%7D">www.stromypodkontrolou.cz</a></dd>
				    </dl>
				  </div>  
		    </div>
		    <div class="row">	
				<div class="col-sm-12">    
		    		<h2 class="theme">3. Zajištění provozní bezpečnostní dřevin památkově chráněných objektů, zámeckých zahrad, hradů a reprezentativních ploch</h2>
		    	</div>
		    </div>
		    <div class="row">
		    	<div class="col-sm-6">
				    <dl class="dl-horizontal">	
				       	<dt>Název:</dt><dd>Ošetřování památných dřevin v zámeckém parku Buchlovice</dd>
				    	<dt>Investor:</dt><dd>Zámecký park Buchlovice</dd>
				    	<dt>Kontakt:</dt><dd>Vedoucí odboru správy Státního zámku Buchlovice: Ivana Šupková, <a href="mailto:supkova.ivana@npu.cz">supkova.ivana@npu.cz</a></dd>
				    	<dt>Objem zakázky:</dt><dd>Dlouhodobá realizace arboristických zásahů</dd>
				    	<dt>Doba plnění:</dt><dd>rok 2000 – současnost</dd>
				    	<dt>Fotodokumentace:</dt><dd><a href="https://goo.gl/photos/maBdvgwrLvQFJs2B6">Google Photos</a></dd>
				    </dl>
				    <hr/>
			    </div>
			    <div class="col-sm-6">
				    <dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Rizikové kácení stromů a zajištění provozní bezpečnosti významných dřevin – Hrad Pernštejn</dd>
				    	<dt>Investor:</dt><dd>Správa hradu Pernštejn</dd>
				    	<dt>Kontakt:</dt><dd>Eva Škrabalová, tel.&nbsp;724&nbsp;095&nbsp;282, <a href="mailto:skrabalova.eva@npu.cz">skrabalova.eva@npu.cz</a></dd>
				    	<dt>Doba plnění:</dt><dd>rok 2000 – současnost</dd>
				    </dl>
				    <hr class="visible-xs-block" />
				</div>   
			</div>
			<div class="row">	    
			    <div class="col-sm-6">
				    <dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Vysoké učení technické v Brně</dd>
				    	<dt>Kontakt:</dt><dd>Ing. Andrea Fridrichová</dd>
				    	<dt>Objem zakázky:</dt><dd>dlouhodobá realizace arboristických zásahů</dd>
				    	<dt>Doba plnění:</dt><dd>rok 2000 – současnost</dd>
				    	<dt>Popis:</dt><dd>Komplexní údržba dřevin reprezentační zahrady rektorátu, dlouhodobé zajištění provozní bezpečnosti mohutného stromořadí platanů, 
				    	<a href="https://www.stromypodkontrolou.cz/map/?draw_selection_circle=1#%7B%22lat%22%3A49.20216336090295%2C%22lng%22%3A16.60471110194919%2C%22zoom%22%3A19%7D">www.stromypodkontrolou.cz</a></dd>
				    	<dt>Fotodokumentace:</dt><dd><a href="https://goo.gl/photos/e3jXexZKhshkpaqk7">Google Photos</a></dd>
				    </dl>
				</div> 
			    <div class="col-sm-6">
				    <dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Kácení stromů a zajištění provozní bezpečnosti významných dřevin – Domov seniorů Rožnov pod Radhoštěm</dd>
				    	<dt>Investor:</dt><dd>Domov seniorů Rožnov pod Radhoštěm</dd>
				    	<dt>Kontakt:</dt><dd>Sociální služby Vsetín, p. o., Záviše Kalandry 1353, 75501 Vsetín</dd>
				    	<dt>Fotodokumentace:</dt><dd><a href="https://goo.gl/photos/nyPuJzK2ccWnAvUj8">Google Photos</a></dd>
				    </dl>
				</div>    
			</div>
			<div class="row">	
				<div class="col-sm-12">        
		    		<h2 class="theme">4. Tvorba lsoparků výběrem a upřednostněním perspektivních druhů na lesních pozemcích soukromých vlastníků</h2>
		    	</div>
		    </div>	
		    <div class="row">
		    	<div class="col-sm-6">
				    <dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Regenerace lesoparku soukromého investora Zlín</dd>
				    	<dt>Kontakt:</dt><dd>Mladcová</dd>
				    	<dt>Popis:</dt><dd>návrhy pěstebních opatření, zajištění provozní bezpečnosti dřevin a upřednostnění perspektivních stromů , z neudržovaného lesního porostu tvorba plochy parkového charakteru.</dd>
				    	<dt>Fotodokumentace:</dt><dd><a href="https://goo.gl/photos/8xHq1ffQaYsGfrFp8">Google Photos</a></dd>
				    </dl>
			    </div>
		    </div>
		     <div class="row">	
				<div class="col-sm-6">    
		    		<h2 class="theme">5. Prezentace v médiích</h2>
		    		<div class="media">
					  <div class="media-left">
					    <a href="http://www.ceskatelevize.cz/ivysilani/10122427178-udalosti-v-regionech-brno/313281381990520-udalosti-v-regionech/obsah/262644-pamatnou-lipu-osetrili-arboriste/">
					      <img class="media-object" src="<c:url value='/resources/imgs/logo-ceskatelevize.png' />" alt="Logo - Česká televize">
					    </a>
					  </div>
					  <div class="media-body">
					    <a href="http://www.ceskatelevize.cz/ivysilani/10122427178-udalosti-v-regionech-brno/313281381990520-udalosti-v-regionech/obsah/262644-pamatnou-lipu-osetrili-arboriste/">
					    	Vysílání České televize - ošetření památné lípy	
					    </a>
					  </div>
					</div>
		    	</div>
		    	<div class="col-sm-6">    
		    		<h2 class="theme">6. Zahraniční mise</h2>
		    		<dl class="dl-horizontal">
				    	<dt>Název:</dt><dd>Ošetřování památných kaštanovníků Pianello - Korsika</dd>
				    	<dt>Fotodokumentace:</dt><dd><a href="https://goo.gl/photos/NHVqjqgfa9xcaqAV6">Google Photos</a></dd>
				    </dl>
		    	</div>
		    </div>
		    <div class="row">	
				<div class="col-sm-12">    
		    		<h2 class="theme">7. Reference na serveru referencehodnoceni.cz</h2>
		    	</div>
		    </div>
		    <div class="row">
		    	<div class="col-sm-6">
		    		<script type="text/javascript" src="http://zdenek-sarapatka.referencehodnoceni.cz/refebox/e4662bce101233cef8c617a329fa5094.js"></script>
		    	</div>
		    </div>		
		    <div class="row photos">
		      <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-reference.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-reference.jpg' />" alt="Kácení Šarapatka - reference" />
			    </a>
			  </div>	
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-reference-1.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-reference-1.jpg' />" alt="Kácení Šarapatka - reference - 1" />
			    </a>
			  </div>
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-reference-2.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-reference-2.jpg' />" alt="Kácení Šarapatka - reference - 2" />
			    </a>
			  </div>
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-reference-3.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-reference-3.jpg' />" alt="Kácení Šarapatka - reference - 3" />
			    </a>
			  </div>
			</div>       
	    </div>
    </tiles:putAttribute>
</tiles:insertDefinition>