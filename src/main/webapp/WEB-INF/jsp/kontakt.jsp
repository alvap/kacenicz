<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="contact" scope="request" />
<tiles:insertDefinition name=".layout.public.pages">
	<tiles:putAttribute name="title">Kontakt - Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="headlineText" cascade="true">Kontakt</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<section>
						<p>Využijte našeho poptávkového formuláře:</p>
						<app:inquiry-btn />
					</section>
					<section>
						<p><strong>Kácení Šarapatka s.r.o.</strong></p>
						<dl>
							<dt class="pull-left">Sídlo:</dt><dd>&nbsp;Karásek 2249/1g, Řečkovice, 621 00 Brno</dd>
							<dt class="pull-left">Identifikační číslo:</dt><dd>&nbsp;06667490</dd>
							<dt class="pull-left">Email:</dt><dd>&nbsp;<a href="mailto:info@kaceni.cz">info@kaceni.cz</a></dd>
						</dl>
					</section>
					<section>
						<p>Ing. Tomáš Šarapatka</p>
						<dl>
							<dt class="pull-left">Tel.:</dt><dd> &nbsp;+420&nbsp;724&nbsp;088&nbsp;639</dd>
							<dt class="pull-left">Email:</dt><dd>&nbsp;<a href="mailto:tomas@kaceni.cz">tomas@kaceni.cz</a></dd>
						</dl>
					</section>
				</div>
				<div class="col-sm-6">
					<h2>Ing. Tomáš Šarapatka</h2>
					<ul>
						<li>Střední zahradnická škola Brno</li>
						<li>Mendelova zemědělská a lesnická univerzita v Brně – obor ekologie a krajinné inženýrství</li>
						<li>Evropský arboristický certifikát „EUROPEAN TREEWORKER“</li>
						<li>člen mezinárodní arboristické společnosti ISA - International Society of Arboriculture</li>
						<li>působení v americké firmě Bartlet tree experts - Atlanta – Georgia školící činnost pro firmu WMS -ITC Baltimore</li>
						<li>komisař pro certifikaci European tree worker a Český certifikovaný arborista</li>
						<li>člen arboristické akademie</li>
					</ul>
				</div>
 	    	</div>
 	    	<div class="row photos">
		      <div class="col-sm-2 col-sm-offset-2 col-xs-6">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-kontakt.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-kontakt.jpg' />" alt="Kácení Šarapatka - kontakt" />
			    </a>
			  </div>	
			  <div class="col-sm-2 col-xs-6">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-kontakt-1.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-kontakt-1.jpg' />" alt="Kácení Šarapatka - kontakt - 1" />
			    </a>
			  </div>
			  <div class="col-sm-2 col-xs-6">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-kontakt-2.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-kontakt-2.jpg' />" alt="Kácení Šarapatka - kontakt - 2" />
			    </a>
			  </div>
			  <div class="col-sm-2 col-xs-6">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-kontakt-3.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-kontakt-3.jpg' />" alt="Kácení Šarapatka - kontakt - 3" />
			    </a>
			  </div>
			</div>       
	    </div>
 	    </div>	
    </tiles:putAttribute>
</tiles:insertDefinition>