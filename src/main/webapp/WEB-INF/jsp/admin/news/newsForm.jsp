<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="${news.type == 'NEWS' ? 'news' : 'newsEducation'}" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - editace novinky</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<h1 class="admin-theme">
				<c:choose>
					<c:when test="${empty news.id}">Nová novinka</c:when>
					<c:otherwise>Úprava novinky</c:otherwise>
				</c:choose>
				<c:if test="${news.type == 'EDUCATION'}">(Školení a vzdělávání)</c:if>
			</h1>
			<ui:message />
			<form:form modelAttribute="news" cssClass="form-horizontal auto-width">
				<c:set var="labelCols" value="col-sm-3" />
				<c:set var="controlCols" value="col-sm-5" />
				<spring-ext:formGroup path="date">
					<spring-ext:label path="date" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="date" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="text">
					<spring-ext:label path="text" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:textarea path="Text" cssClass="form-control" rows="10" cols="50" />
					</div>
				</spring-ext:formGroup>
				<div class="form-actions">
					<input type="submit" value="Uložit" name="save" class="btn btn-primary"/>
					<input type="submit" value="Zrušit" name="cancel" class="btn btn-default" formnovalidate />
				</div>
				<form:hidden path="id"/>
				<form:hidden path="version"/>
				<form:hidden path="type" />
			</form:form>
		</div>	
    </tiles:putAttribute>
</tiles:insertDefinition>