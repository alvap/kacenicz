<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="${educationNews ? 'newsEducation': 'news'}" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - přehled novinek</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<h1 class="admin-theme">Přehled novinek <c:if test="${educationNews}">(Školení a vzdělávání)</c:if></h1>
			<ui:message />
			<c:choose>
				<c:when test="${educationNews}">
					<c:url value='/administrace/novinky/nova-novinka-vzdelavani' var="newsCreateUrl" />
					<c:url value='/administrace/novinky/upravy-novinky-vzdelavani/' var="newsUpdateUrl" />
					<c:url value='/administrace/novinky/smazani-novinky-vzdelavani/' var="newsRemoveUrl" />
				</c:when>
				<c:otherwise>
					<c:url value='/administrace/novinky/nova-novinka' var="newsCreateUrl" />
					<c:url value='/administrace/novinky/upravy-novinky/' var="newsUpdateUrl" />
					<c:url value='/administrace/novinky/smazani-novinky/' var="newsRemoveUrl" />
				</c:otherwise>
			</c:choose>
			<p><a href="${newsCreateUrl}" class="btn btn-default">Nová novinka</a></p>
			<c:choose>
				<c:when test="${!empty newsList}">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Datum</th>
								<th>Text</th>
								<th></th>
								<th></th>
							</tr>
						</thead>	
						<tbody>								
							<c:forEach items="${newsList}" var="news">
								<tr>
									<td><joda:format value="${news.date}"/></td>
									<td><c:out value="${news.text}" escapeXml="false"/></td>
									<td>
										<a href="${newsUpdateUrl}${news.id}" title="Upravit novinku" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"><span class="hidden">Upravit</span></i></a>
									</td>
									<td>
										<a href="${newsRemoveUrl}${news.id}" title="Smazat novinku" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-remove"><span class="hidden">Smazat</span></i></a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>	
				</c:when>
				<c:otherwise>
					<p class="alert alert-info">Žádné novinky nejsou k dispozici.</p>
				</c:otherwise>
			</c:choose>	
		</div>
    </tiles:putAttribute>
  </tiles:insertDefinition>