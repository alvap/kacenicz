<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="inquiryList" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - detail poptávky - cenová nabídka</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<h1 class="admin-theme">Detail poptávky - cenová nabídka</h1>
			<ul class="nav nav-tabs admin-menu">
			  <li><a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiryId}' />">Poptávka</a></li>
			  <li class="active"><a href="#">Cenová nabídka</a></li>
			</ul>
			<ui:message />
			<c:choose>
				<c:when test="${empty priceOffer}">
					<p class="alert alert-info">Cenová nabídka není vytvořená.</p>
					<div class="btns">
						<a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiryId}/nova-polozka-cenove-nabidky' />" class="btn btn-default">Vytvořit položku cenové nabídky</a>
						<a href="<c:url value='/administrace/poptavky/nahrat-cenovou-nabidku/${inquiryId}' />" class="btn btn-warning">Nahrát k poptávce cenovou nabídku</a>
					</div>
				</c:when>
				<c:otherwise>
					<p class="btns">
						<a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiryId}/nova-polozka-cenove-nabidky' />" class="btn btn-default">Vytvořit položku cenové nabídky</a>
						<a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiryId}/tisk-cenove-nabidky/${priceOffer.id}' />" class="btn btn-info" target="_blank">Vytisknout cenovou nabídku</a>
						<a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiryId}/upravit-cenovou-nabidku/${priceOffer.id}' />" class="btn btn-warning">Upravit doplňující údaje</a>
						<a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiryId}/smazat-cenovou-nabidku/${priceOffer.id}' />" class="btn btn-danger">Smazat cenovou nabídku</a>
					</p>
					<div class="row">
						<div class="col-sm-5">
							<dl>
								<dt>Adresa:</dt>
								<dd><c:out value="${!empty priceOffer.address && !empty priceOffer.address.name ? priceOffer.address.name : 'Jméno nezadáno'}" /></dd>
								<dd><c:out value="${!empty priceOffer.address && !empty priceOffer.address.street ? priceOffer.address.street : 'Ulice nazadána'}" /></dd>
								<dd><c:out value="${!empty priceOffer.address && !empty priceOffer.address.city ? priceOffer.address.city : 'Město nezadáno'}" /></dd>
								<dd><c:out value="${!empty priceOffer.address && !empty priceOffer.address.zip ? priceOffer.address.zip : 'PSČ nezadáno'}" /></dd>
							</dl>
							<dl class="dl-inline">	
								<dt>Telefon:</dt>
								<dd><c:out value="${priceOffer.phoneNumber}" /></dd>
								<dt>Email:</dt>
								<dd><c:out value="${priceOffer.email}" /></dd>
							</dl>
						</div>
						<div class="col-sm-7">
							<dl>
								<dt>Datum:</dt><dd><joda:format value="${priceOffer.date}" dateTimeZone="Europe/Prague" pattern="dd.MM.yyyy"/></dd>
								<c:if test="${!empty priceOffer.internalNote}">
									<dt>Poznámka:</dt>
									<dd><c:out value="${priceOffer.internalNote}" /></dd>
								</c:if>	
							</dl>
						</div>
					</div>	
					<jsp:include page="/WEB-INF/jsp/_include/priceOfferTable.jsp">
						<jsp:param value="true" name="edit"/>
						<jsp:param value="true" name="showVatRate"/>
					</jsp:include>
					<dl class="dl-inline">
						<dt>Cena celkem bez DPH:</dt><dd><f:formatNumber  minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceWithoutVat}"/></dd>
						<dt>Cena celkem s DPH:</dt><dd><f:formatNumber  minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceWithVat}"/></dd>
					</dl>
				</c:otherwise>
			</c:choose>
		</div>
    </tiles:putAttribute>
  </tiles:insertDefinition>