<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="inquiryList" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - přehled poptávek</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
		<h1 class="admin-theme">Přehled poptávek</h1>
		<p>
			<a href="<c:url value='/administrace/poptavky/nahrat-cenovou-nabidku' />" class="btn btn-warning">Nahrát poptávku / cenovou nabídku</a>
			<a href="<c:url value='/administrace/poptavky/zadat-poptavku' />" class="btn btn-info">Zadat poptávku</a>
		</p>
		<form:form modelAttribute="inquiryFilter" cssClass="form-inline">
			<ui:message attrName="formErrorMessage" />
			<div class="errors">
				<form:errors path="*" element="p" />
			</div>
			<div class="form-group">
				<spring-ext:select path="inquiryState" cssClass="form-control">
				    <option value="">všechny</option>
	  				<c:forEach items="${inquiryStateEnumList}" var="inquiryState">
	  					<option value="${inquiryState}" <c:if test="${inquiryFilter.inquiryState == inquiryState}">selected="selected"</c:if>>
	  						<spring:message code="inquiryState.${inquiryState}" />
	  					</option>
	  				</c:forEach>
	  			</spring-ext:select>
  			</div>
  			<div class="form-group">
  				<spring-ext:input path="dateFrom"  maxlength="16" inputType="date" cssClass="form-control" forcePlaceholder="true" generateCssRelativeSize="false" includeErrors="false" />
  			</div>
  			<div class="form-group">
  				<spring-ext:input path="dateTo"  maxlength="16" inputType="date" cssClass="form-control" forcePlaceholder="true" generateCssRelativeSize="false" includeErrors="false"/>
  			</div>
  			<div class="form-group">
  				<spring-ext:input path="lastName" cssClass="form-control" forcePlaceholder="true" includeErrors="false"/>
  			</div>
			<div class="form-actions">
	    		<input type="submit" value="Vyhledat" name="filter" class="btn btn-primary"/>
				<input type="submit" value="Vymazat filter"  name="resetFilter" class="btn btn-default" />
			</div>	
		</form:form>
		<ui:message />
		<c:if test="${empty inquiryListPage || fn:length(inquiryListPage.content) == 0}" var="emptyInquiryList" />
		<c:choose>
			<c:when test="${emptyInquiryList}">
				<p class="alert alert-warning">Žádné poptávky nejsou k dispozici.</p>
			</c:when>
			<c:otherwise>
				<c:set var="inquiryList" value="${inquiryListPage.content}" />
				<ui:message />	
				<c:set var="paginationContext" value="${sessionScope['inquiryPaginationContext']}" />
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Datum</th>
							<th>Zákazník</th>
							<th>Zpráva</th>
							<th>Služby</th>
							<th>Poznámka</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${inquiryList}" var="inquiry">
							<tr>
								<td><a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiry.id}' />"><joda:format value="${inquiry.created}" pattern="dd.MM.yyyy HH:mm" dateTimeZone="Europe/Prague"/></a></td>
								<td>${inquiry.customer.name}</td>
								<td>${inquiry.message}</td>
								<td>${inquiry.adminSubject}</td>
								<td><c:out value="${inquiry.adminNote}" />
									<a href="<c:url value='/administrace/poptavky/upravit-poznamku-k-poptavce/${inquiry.id}'/>" title="Upravit poznámku"><i class="glyphicon glyphicon-pencil"><span class="sr-only">Upravit</span></i></a>
								</td>
								<td>
									<form action="<c:url value='/administrace/poptavky/zmenit-stav-poptavky' />" method="post">
										<input type="hidden" value="${inquiry.id}" name="inquiryId" />
										<select name="inquiryState" class="form-control input-sm">
											<c:forEach items="${inquiryStateEnumList}" var="inquiryState">
							  					<option value="${inquiryState}" <c:if test="${inquiry.state == inquiryState}">selected="selected"</c:if>>
							  						<spring:message code="inquiryState.${inquiryState}" />
							  					</option>
							  				</c:forEach>
							  			</select>
							  			<input type="submit" value="Změnit" class="btn btn-sm btn-default" />
									</form>
								</td>
								<td>
									<a href="<c:url value='/administrace/poptavky/smazat-poptavku/${inquiry.id}'/>" title="Smazat poptávku" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-remove"></i>&nbsp;Smazat</a>
								</td>		
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>
		<pagination:pages page="${inquiryListPage}" cssClass="pagination"  />
		</div>
    </tiles:putAttribute>
</tiles:insertDefinition>