<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="inquiryList" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="pageCss" cascade="true">
		<asset:css url="/resources/libs/magnific-popup-1.1/magnific-popup.css" />
		<asset:css url="/resources/min-assets/magnific-popup.css" isForProductionEnvironment="true"/> 
	</tiles:putAttribute>
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - detail poptávky</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
		<h1 class="admin-theme">Detail poptávky</h1>
		<ul class="nav nav-tabs admin-menu">
		  <li class="active"><a href="#">Poptávka</a></li>
		  <li><a href="<c:url value='/administrace/poptavky/detail-poptavky/${inquiry.id}/cenova-nabidka' />">Cenová nabídka</a></li>
		</ul>
		<p><b>Datum: <joda:format value="${inquiry.created}" dateTimeZone="Europe/Prague" pattern="dd.MM.yyyy HH:mm"/></b></p>
		<h2 class="admin-theme">Kontaktní údaje</h2>
		<dl class="dl-inline">
			<dt>Jméno:</dt><dd>${inquiry.customer.name}</dd>
			<c:if test="${!empty inquiry.customer.companyName}"><dt>Firma:</dt><dd>${inquiry.customer.companyName}</dd></c:if>
			<dt>Telefon:</dt><dd>${inquiry.customer.phoneNumber}</dd>
			<dt>Email:</dt><dd><a href="mailto:${inquiry.customer.userAccount.email}">${inquiry.customer.userAccount.email}</a></dd>
		</dl>
		<h2 class="admin-theme">Předmět</h2>
		<p><c:out value="${inquiry.message}" /></p>
		<c:if test="${!empty inquiry.address}">
			<h2 class="admin-theme">Adresa</h2>
			<dl class="dl-inline">	
				<dt>Ulice:</dt><dd><c:out value="${inquiry.address.street}"/></dd>
				<dt>Město:</dt><dd><c:out value="${inquiry.address.city}"/></dd>
				<dt>PSČ:</dt><dd><c:out value="${inquiry.address.zip}"/></dd>
			</dl>
		</c:if>
		<div>
		<h2 class="admin-theme">Poptávané služby</h2>
		<c:forEach items="${inquiry.workList}" var="work">
			<div class="theme-indent">
			<c:set var="work" scope="request" value="${work}" />
			<c:choose>
				<c:when test="${work.workType.id == '0'}">
					<h3 class="admin-theme"><spring:message code="workType.SINGLE_FELLING" /></h3>
					<dl class="dl-inline">
						<dt>Výška (m):</dt><dd><c:out value="${work.height}"/></dd>
						<dt>Obvod kmene (cm):</dt><dd><c:out value="${work.girth}"/></dd>
						<dt>Šířka koruny (m):</dt><dd><c:out value="${work.crownWidth}"/></dd>
						<dt>Druh stromu:</dt><dd><c:out value="${work.treeType}" /></dd>
					</dl>
					<p><b>Popis překážek:</b></p>
					<p><c:out value="${work.itemsDescription}"/></p>
					<jsp:include page="/WEB-INF/jsp/_include/photoAndLocation.jsp" />
					<hr/>
				</c:when>	
				<c:when test="${work.workType.id == '1'}">
					<h3 class="admin-theme"><spring:message code="workType.SINGLE_RISK_FELLING" /></h3>
					<dl class="dl-inline">
						<dt>Výška (m):</dt><dd><c:out value="${work.height}"/></dd>
						<dt>Obvod kmene (cm):</dt><dd><c:out value="${work.girth}"/></dd>
						<dt>Šířka koruny (m):</dt><dd><c:out value="${work.crownWidth}"/></dd>
						<dt>Druh stromu:</dt><dd><c:out value="${work.treeType}" /></dd>
					</dl>
					<p><b>Popis překážek:</b></p>
					<p><c:out value="${work.itemsDescription}"/></p>
					<jsp:include page="/WEB-INF/jsp/_include/photoAndLocation.jsp" />
					<hr/>
				</c:when>
				<c:when test="${work.workType.id == '2'}">
					<h3 class="admin-theme"><spring:message code="workType.MULTIPLE_FELLING" /></h3>
					<dl class="dl-inline">
						<dt>Počet listnatých:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
						<dt>Počet jehličnatých:</dt><dd><c:out value="${work.additionalNumberOfItems}" /></dd>
					</dl>
					<jsp:include page="/WEB-INF/jsp/_include/photoAndLocation.jsp" />
					<hr/>
				</c:when>
				<c:when test="${work.workType.id == '3'}">
					<h3 class="admin-theme"><spring:message code="workType.MULTIPLE_RISK_FELLING" /></h3>
					<dl class="dl-inline">
						<dt>Počet listnatých:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
						<dt>Počet jehličnatých:</dt><dd><c:out value="${work.additionalNumberOfItems}" /></dd>
					</dl>
					<jsp:include page="/WEB-INF/jsp/_include/photoAndLocation.jsp" />
					<hr/>
				</c:when>
				<c:when test="${work.workType.id == '4'}">
					<h3 class="admin-theme"><spring:message code="workType.MILLING_OF_STUMPS" /></h3>
					<dl class="dl-inline">
						<dt>Počet ks:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
						<dt>Průměr:</dt><dd><c:out value="${work.diameter}" /></dd>
					</dl>
					<jsp:include page="/WEB-INF/jsp/_include/photoAndLocation.jsp" />
					<hr/>
				</c:when>
				<c:when test="${work.workType.id == '5'}">
					<h3 class="admin-theme"><spring:message code="workType.GARDEN_MAINTANANCE" /></h3>
					<hr/>
				</c:when> 
				<c:when test="${work.workType.id == '6'}">
					<h3 class="admin-theme"><spring:message code="workType.TREE_TRIMMING" /></h3>
					<dl class="dl-inline">
						<dt>Počet ks:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
					</dl>	
					<jsp:include page="/WEB-INF/jsp/_include/photoAndLocation.jsp" />
					<hr/>
				</c:when>
				<c:when test="${work.workType.id == '7'}">
					<h3 class="admin-theme"><spring:message code="workType.PLANTING" /></h3>
					<dl class="dl-inline">
						<dt>Počet ks:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
					</dl>
					<hr/>
				</c:when> 
				<c:when test="${work.workType.id == '8'}">
					<h3 class="admin-theme"><spring:message code="workType.DISPOSAL" /></h3>
					<c:forEach items="${work.filteredDisposalList}" var="disposal">
						<ul>
							<li><spring:message code="disposalAdminType.${disposal.disposal}" /></li>
						</ul>						    
					</c:forEach>
					<hr/>
				</c:when>
			</c:choose>
			</div>
		</c:forEach>
		</div>
		<c:if test="${!empty inquiry.term}"><%-- if term is not set it is inquiry import --%>
			<h2 class="admin-theme">Termín, platba</h2>
			<dl class="dl-inline">
				<dt>Termín:</dt><dd><spring:message code="termType.${inquiry.term}" /></dd>
				<dt>Požaduje fakturu:</dt><dd>${inquiry.requiresBill ? 'ano' : 'ne'}</dd>
			</dl>
		</c:if>	
		<c:if test="${inquiry.requiresBill}">
		<h2 class="admin-theme">Fakturační údaje</h2>
			<dl class="dl-inline">
				<dt>Jméno:</dt><dd>${inquiry.customer.billingData.address.name}</dd>
				<dt>Ulice:</dt><dd>${inquiry.customer.billingData.address.street}</dd>
				<dt>Město:</dt><dd>${inquiry.customer.billingData.address.city}</dd>
				<dt>PSČ:</dt><dd>${inquiry.customer.billingData.address.zip}</dd>
				<dt>IČO:</dt><dd>${inquiry.customer.billingData.regNo}</dd>
				<dt>DIČ:</dt><dd>${inquiry.customer.billingData.vatNo}</dd>
				<dt>Plátce DPH:</dt><dd>${inquiry.vatPayer ? 'ano' : 'ne'}</dd>
			</dl>		
		</c:if>
		</div>
    </tiles:putAttribute>
    <tiles:putAttribute name="javascript">
    	<%-- ORDERED !! list of javascript files, a file may depend on previous file(s) --%>
		<asset:script url="/resources/libs/bootstrap-3.3.6/js/modal.js" />
		<asset:script url="/resources/min-assets/jquery.magnific-popup.min.js" isForProductionEnvironment="true" />
		<asset:script url="/resources/js/app-admin.js" isForProductionEnvironment="true" />
        <asset:script url="/resources/js/namespace.js" />
        <asset:script url="/resources/js/apputils/constants.js" />
        <asset:script url="/resources/js/utils/dto.js" />
		<asset:script url="/resources/js/gui/widgets.js" />
        <asset:script url="/resources/js/apputils/widgets-utils.js" />
        <asset:script url="/resources/js/app/google-street-view-locator.js" />
        <asset:script url="/resources/js/app/inquiry-detail.js" />
        <asset:script url="/resources/libs/magnific-popup-1.1/jquery.magnific-popup.js" />
         <script type="text/javascript"> 
         	$('.popup-gallery').magnificPopup({
         		  type: 'image',
         		  delegate: 'a',
         		  gallery:{enabled:true}
         		  // other options
         		});
         </script> 
         <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7UYx2oC_P2zL4h8yU1_9Q9ngm0DfTKPg&&callback=APP.app.GoogleStreetViewLocator.mapsLoadedCallback&&libraries=geometry" async defer></script>
    </tiles:putAttribute>
  </tiles:insertDefinition>