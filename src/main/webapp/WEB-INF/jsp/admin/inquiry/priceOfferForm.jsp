<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="inquiryList" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - doplňující údaje k cenové nabídce</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<h1 class="admin-theme">Doplňující údaje k cenové nabídce</h1>
			<ui:message />
			<form:form modelAttribute="priceOffer" cssClass="form-horizontal">
				<c:set var="labelCols" value="col-sm-3" />
				<c:set var="controlCols" value="col-sm-5" />
				<spring-ext:formGroup path="address.name">
					<spring-ext:label path="address.name" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="address.name" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="address.street">
					<spring-ext:label path="address.street" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="address.street" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="address.city">
					<spring-ext:label path="address.city" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="address.city" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="address.zip">
					<spring-ext:label path="address.zip" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="address.zip" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="email">
					<spring-ext:label path="email" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="email" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="phoneNumber">
					<spring-ext:label path="phoneNumber" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="phoneNumber" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="internalNote">
					<spring-ext:label path="internalNote" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:textarea path="internalNote" cssClass="form-control" rows="10" cols="40"/>
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="date">
					<spring-ext:label path="date" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input inputType="date" path="date" cssClass="form-control" size="10" />
					</div>
				</spring-ext:formGroup>
				<div class="form-actions">
					<input type="submit" value="Uložit" name="save" class="btn btn-primary"/>
					<input type="submit" value="Zrušit" name="cancel" class="btn btn-default" formnovalidate />
				</div>
				<form:hidden path="id"/>
				<form:hidden path="version"/>
			</form:form>
		</div>	
    </tiles:putAttribute>
</tiles:insertDefinition>