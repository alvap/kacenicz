<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="inquiryList" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - úprava poznámky k poptávce</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<h1 class="admin-theme">Úprava poznámky k poptávce</h1>
			<ui:message />
			<form:form modelAttribute="inquiry" cssClass="form-horizontal">
				<form:hidden path="id"/>
				<div class="form-group">
					<div class="col-sm-5">
						<spring-ext:textarea path="adminNote" cssClass="form-control" rows="10"/>
					</div>
				</div>
				<div class="form-actions">
		    		<input type="submit" value="Uložit" name="save" class="btn btn-primary"/>
					<input type="submit" value="Zrušit" name="cancel" class="btn btn-default" />
				</div>
			</form:form>
		</div>	
    </tiles:putAttribute>
</tiles:insertDefinition>