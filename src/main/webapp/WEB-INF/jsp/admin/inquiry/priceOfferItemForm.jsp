<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="inquiryList" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - cenová nabídka k poptávce</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<h1 class="admin-theme">Cenová nabídka k poptávce</h1>
			<ui:message />
			<c:if test="${!empty priceOffer}">
				<jsp:include page="/WEB-INF/jsp/_include/priceOfferTable.jsp">
					<jsp:param value="true" name="showVatRate"/>
				</jsp:include>
			</c:if>
			<div class="row">
			<div class="col-sm-6">
			<form:form modelAttribute="priceOfferItem" cssClass="form-horizontal">
				<c:set var="labelCols" value="col-sm-3" />
				<c:set var="controlCols" value="col-sm-5" />
				<spring-ext:formGroup path="name">
					<spring-ext:label path="name" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="name" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="amount">
					<spring-ext:label path="amount" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="amount" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="amountUnit">
					<spring-ext:label path="amountUnit" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="amountUnit" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="type">
					<spring-ext:label path="type" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="type" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="priceChipping">
					<spring-ext:label path="priceChipping" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="priceChipping" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="priceDisposal">
					<spring-ext:label path="priceDisposal" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="priceDisposal" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="priceTrimming">
					<spring-ext:label path="priceTrimming" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="priceTrimming" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="priceFelling">
					<spring-ext:label path="priceFelling" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="priceFelling" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="priceStumpsMilling">
					<spring-ext:label path="priceStumpsMilling" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="priceStumpsMilling" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="priceBinding">
					<spring-ext:label path="priceBinding" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="priceBinding" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="priceOther">
					<spring-ext:label path="priceOther" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="priceOther" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<spring-ext:formGroup path="vatRate">
					<spring-ext:label path="vatRate" cssClass="control-label ${labelCols}" />
					<div class="${controlCols}">
						<spring-ext:input path="vatRate" cssClass="form-control" />
					</div>
				</spring-ext:formGroup>
				<div class="form-actions">
					<input type="submit" value="Uložit a zadat další položku" name="saveAndContinue" class="btn btn-primary"/>
		    		<input type="submit" value="Uložit a zpět na detail poptávky" name="save" class="btn btn-primary"/>
					<input type="submit" value="Zrušit" name="cancel" class="btn btn-default" formnovalidate />
				</div>
				<form:hidden path="id"/>
				<form:hidden path="version"/>
				<form:hidden path="priceOffer"/>
			</form:form>
			</div>
			<div class="col-sm-6">
				<h2 class="admin-theme">Poptávané služby</h2>
				<c:forEach items="${inquiry.workList}" var="work">
					<div class="theme-indent">
					<c:set var="work" scope="request" value="${work}" />
					<c:choose>
						<c:when test="${work.workType.id == '0'}">
							<h3 class="admin-theme"><spring:message code="workType.SINGLE_FELLING" /></h3>
							<dl class="dl-inline">
								<dt>Výška (m):</dt><dd><c:out value="${work.height}"/></dd>
								<dt>Obvod kmene (cm):</dt><dd><c:out value="${work.girth}"/></dd>
								<dt>Šířka koruny (m):</dt><dd><c:out value="${work.crownWidth}"/></dd>
								<dt>Druh stromu:</dt><dd><c:out value="${work.treeType}" /></dd>
							</dl>
							<p><b>Popis překážek:</b></p>
							<p><c:out value="${work.itemsDescription}"/></p>
							<hr/>
						</c:when>	
						<c:when test="${work.workType.id == '1'}">
							<h3 class="admin-theme"><spring:message code="workType.SINGLE_RISK_FELLING" /></h3>
							<dl class="dl-inline">
								<dt>Výška (m):</dt><dd><c:out value="${work.height}"/></dd>
								<dt>Obvod kmene (cm):</dt><dd><c:out value="${work.girth}"/></dd>
								<dt>Šířka koruny (m):</dt><dd><c:out value="${work.crownWidth}"/></dd>
								<dt>Druh stromu:</dt><dd><c:out value="${work.treeType}" /></dd>
							</dl>
							<p><b>Popis překážek:</b></p>
							<p><c:out value="${work.itemsDescription}"/></p>
							<hr/>
						</c:when>
						<c:when test="${work.workType.id == '2'}">
							<h3 class="admin-theme"><spring:message code="workType.MULTIPLE_FELLING" /></h3>
							<dl class="dl-inline">
								<dt>Počet listnatých:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
								<dt>Počet jehličnatých:</dt><dd><c:out value="${work.additionalNumberOfItems}" /></dd>
							</dl>
							<hr/>
						</c:when>
						<c:when test="${work.workType.id == '3'}">
							<h3 class="admin-theme"><spring:message code="workType.MULTIPLE_RISK_FELLING" /></h3>
							<dl class="dl-inline">
								<dt>Počet listnatých:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
								<dt>Počet jehličnatých:</dt><dd><c:out value="${work.additionalNumberOfItems}" /></dd>
							</dl>
							<hr/>
						</c:when>
						<c:when test="${work.workType.id == '4'}">
							<h3 class="admin-theme"><spring:message code="workType.MILLING_OF_STUMPS" /></h3>
							<dl class="dl-inline">
								<dt>Počet ks:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
								<dt>Průměr:</dt><dd><c:out value="${work.diameter}" /></dd>
							</dl>
							<hr/>
						</c:when>
						<c:when test="${work.workType.id == '5'}">
							<h3 class="admin-theme"><spring:message code="workType.GARDEN_MAINTANANCE" /></h3>
							<hr/>
						</c:when> 
						<c:when test="${work.workType.id == '6'}">
							<h3 class="admin-theme"><spring:message code="workType.TREE_TRIMMING" /></h3>
							<dl class="dl-inline">
								<dt>Počet ks:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
							</dl>	
							<hr/>
						</c:when>
						<c:when test="${work.workType.id == '7'}">
							<h3 class="admin-theme"><spring:message code="workType.PLANTING" /></h3>
							<dl class="dl-inline">
								<dt>Počet ks:</dt><dd><c:out value="${work.numberOfItems}" /></dd>
							</dl>
							<hr/>
						</c:when> 
						<c:when test="${work.workType.id == '8'}">
							<h3 class="admin-theme"><spring:message code="workType.DISPOSAL" /></h3>
							<c:forEach items="${work.filteredDisposalList}" var="disposal">
								<ul>
									<li><spring:message code="disposalAdminType.${disposal.disposal}" /></li>
								</ul>						    
							</c:forEach>
							<hr/>
						</c:when>
					</c:choose>
					</div>
				</c:forEach>
			</div>
			</div>
		</div>	
    </tiles:putAttribute>
</tiles:insertDefinition>