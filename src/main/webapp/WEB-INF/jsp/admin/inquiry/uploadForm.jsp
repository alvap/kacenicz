<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="inquiryList" scope="request"/>
<tiles:insertDefinition name=".layout.admin">
	<tiles:putAttribute name="title">Administrace - Kácení Šarapatka - nahrání poptávky / cenové nabídky</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<h1 class="admin-theme">Nahrání poptávky / cenové nabídky</h1>
			<ui:message />
			<form:form modelAttribute="uploadForm" cssClass="form-inline" enctype="multipart/form-data">
				<c:if test="${!empty errorMessages}">
					<ul class="list-unstyled error-block">
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" /></li>
						</c:forEach>
					</ul>
				</c:if>
				<input name="file" type="file" />
				<div class="form-actions">
					<input type="submit" value="Nahrát" name="save" class="btn btn-primary"/>
					<input type="submit" value="Zpět" name="cancel" class="btn btn-default" />
				</div>
				<form:hidden path="inquiryId" />
			</form:form>
		</div>	
    </tiles:putAttribute>
</tiles:insertDefinition>