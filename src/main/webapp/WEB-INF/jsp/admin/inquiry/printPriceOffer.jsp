<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<!DOCTYPE html>
<html>
    <head>
    	<title>Cenová nabídka - TISK</title>
    	<ui:meta />
    	<%-- FIXME: add production url --%>
        <asset:css url="/resources/css/app.css" isForProductionEnvironment="true" />
        <%-- ====================================================================================
		CSS - local (development) - raw .less files are used as styles - they are compiled to css
		in runtime by included less.js script. Compiled less files are cached in a browser. To force reloading 
		less files application property 'developmentMode' must be set to true, recompilation of 
		.less files is very slow (should be enabled only if you are changing .less files). Note that watching
		for less files changes doesn't recognize 'ímported' (nested) .less files - a manual browser
		cache cleaning is necessary in this case.
		=====================================================================================--%>
	    <%-- Bootstrap library --%>
        <asset:less url="/resources/less/bootstrap-customization.less" />
	    <%-- Common application styles and Boootstrap overrides --%>
        <asset:less url="/resources/less/app.less" />
        <asset:less url="/resources/less/admin.less" />
		<asset:css url="/resources/css/admin.css" isForProductionEnvironment="true" />
        <asset:IE10ViewportBugWorkaroundCSS />
        <asset:html5SupportForIE />
        <style>
        	@media print {
	        	table {font-size: 10px;}
	        	#print-footer {position: fixed; bottom: 0; left: 0; width: 100%;}
	        	#header-part1 {position: fixed; top: 0; left:0; z-index: 100}
	        	.header-part2 {position: fixed; top: 0; z-index: 80}
	        	#header-part3 {position: fixed; top: 0; right:0; z-index: 100}
	        	header {margin-top: 170px;}
	        	.page-break {page-break-after: always; position: relative;}
	        	.new-page {position: relative; height: 230px;}
        	}
        	.container {padding-top: 20px;}
        	#config {
        		background-color: #f3f3f3;
        		padding: 10px;
        		text-align: center;
        		width: 100%;
        	}
        </style>
    </head>
  	<body class="print">
  		<header class="visible-print-block">
  			<img src="<c:url value='/resources/imgs/part1-logo.png' />" id="header-part1" />
	  		<img src="<c:url value="/resources/imgs/part2-line.png" />" class="header-part2" style="left: 227px;"/>
	  		<img src="<c:url value="/resources/imgs/part2-line.png" />" class="header-part2" style="left: 500px;" />
	  		<img src="<c:url value="/resources/imgs/part2-line.png" />" class="header-part2"  style="left: 700px;"/>
	  		<img src="<c:url value='/resources/imgs/part3-address.png' />" id="header-part3" />	  		
		</header>
		<div class="hidden-print text-center" id="config"><form><label>Zalomení za řádkem č.:</label>&nbsp;<input type="text"/>&nbsp;<input type="submit" value="Použít" /></form></div>
		<div class="container">
			<div class="row">
				<div class="col-xs-6">
					<div class="legend-canvas">
						<h4>Dodavatel</h4>
						<dl class="dl-horizontal-print">
							 <dd>ŠARAPATKA- sdružení FO<br/>Břenkova 18<br/>613 00 Brno
							 <dt>IČ:</dt><dd>64306267</dd> 
							 <dt>DIČ:</dt><dd>CZ520321126</dd> 
							 <dt>Registrace:</dt><dd>ŽÚ ÚMČ Brno střed ev.č: 370202-51992-00</dd> 
							 <dt>Tel.:</dt><dd>+420 724 088 639</dd> 
							 <dt>E-mail:</dt><dd>tomas@kaceni.cz</dd> 
							 <dt>Web:</dt><dd>www.kaceni.cz</dd> 
							 <dt>Bankovní účet:</dt><dd>163727618/0600</dd> 
						</dl>
					</div>
				</div>
				<div class="col-xs-6">
					<div>
						<h4>Odběratel</h4>
						<dl class="dl-horizontal-print">
							<dd><c:if test="${!empty priceOffer.address}">
								<c:if test="${!empty priceOffer.address.name}"><c:out value="${priceOffer.address.name}" /><br/></c:if>
								<c:if test="${!empty priceOffer.address.street}"><c:out value="${priceOffer.address.street}" /><br/></c:if>
								<c:if test="${!empty priceOffer.address.city}"><c:out value="${priceOffer.address.city}" /><br/></c:if>
								<c:if test="${!empty priceOffer.address.zip}"><c:out value="${priceOffer.address.zip}" /><br/></c:if>
							</c:if></dd>	
							<dt>Telefon:</dt><dd><c:out value="${priceOffer.phoneNumber}" /></dd>
							<dt>Email:</dt><dd><c:out value="${priceOffer.email}" /></dd>
						</dl>
					</div>
				</div>
			</div>
			<p><b>Datum vystavení: </b><joda:format value="${priceOffer.date}" dateTimeZone="Europe/Prague" pattern="dd.MM.yyyy"/></p>
			<jsp:include page="/WEB-INF/jsp/_include/priceOfferTable.jsp"/>
			<div class="pull-right">
				<dl class="dl-horizontal-print">
					<dt>Celkem bez DPH:</dt><dd><f:formatNumber  minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceWithoutVat}"/>&nbsp;Kč</dd>
					<dt>Celkem DPH:</dt><dd><f:formatNumber  minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalVat}"/>&nbsp;Kč</dd>
				</dl>
				<hr/>
				<dl class="dl-horizontal-print">	
					<dt>Cena celkem s DPH:</dt><dd><strong><f:formatNumber  minFractionDigits="2" maxFractionDigits="2" value="${priceOffer.totalPriceWithVat}"/>&nbsp;Kč</strong></dd>
				</dl>
			</div>
			<div class="clearfix"></div>
			<div class="row" id="signature">
				<div class="col-xs-3 col-xs-offset-1">
					<img src="<c:url value='/resources/imgs/signature.png'/>" />
				</div>
				<div class="col-xs-3 col-xs-offset-1">
					<p>podpis odběratele</p>
				</div>
			</div>
		</div>
		<div id="print-footer" class="text-right visible-print-block">
			<img src="<c:url value='/resources/imgs/print-logo-arboristicka-akademie.png' />" />
	  		<img src="<c:url value="/resources/imgs/print-logo-stromy-pod-kontrolou.png" />" />
		</div>
	    <asset:IE10ViewportBugWorkaroundJS />
	    <asset:lessCompiler url="/resources/tools/less-2.6.1.min.js" />
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>
        	$(function() {
        		
        		$("form").on("submit", function(event) {
        			event.preventDefault();
        			var row = $("form input[type=text]").val();
        			if (row) row = row.trim();
        			else {
        				alert("Neplatné číslo. Musí být mezi 1 a 99");
        				return;
        			}
        			var valid = false;
        			for (var i=1; i<100; i++) {
        				if (row == i) {
        					valid = true;
        					break;
        				}
        			}
        			if (!valid) {
        				alert("Neplatné číslo. Musí být mezi 1 a 99");
        				return;
        			}
        			
        			var lastRow = $("table tbody tr:nth-child(" + row + ")");
        			if (lastRow.length == 0) {
        				alert("Řádek č.: " + row + " neexistuje.");
        				return;
        			}
        			
        			var rows = $("table tbody tr");
        			var header = $("table thead");
        			var footer = $("table tfoot");
        			
        			var newTable = $("<table>", {'class': 'table table-bordered'});
        			var newTableBody = $("<tbody>"); 
        			
        			newTable.insertAfter($("table"));
        			newTable.append(header.clone());
        			newTable.append(newTableBody);
        			
        			var rowElem;
        			rows.each(function(index,item) {
        				if (index > row) {
        					newTableBody.append($(item).clone());
        					$(item).remove();
        				}
        			});      
        			
        			newTable.append(footer.clone());
        			footer.remove();
        			
        			var pageBreakDiv = $("<div>", {"class": "page-break"});
        			var newPageDiv = $("<div>", {"class": "new-page"});
        			
        			newPageDiv.insertBefore(newTable);
        			pageBreakDiv.insertBefore(newPageDiv);
        			
        			
        		})
        	});
        </script>
   </body>
</html>
