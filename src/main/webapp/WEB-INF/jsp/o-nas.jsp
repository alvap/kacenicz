<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="aboutUs" scope="request" />
<tiles:insertDefinition name=".layout.public.pages">
	<tiles:putAttribute name="title">O nás - Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="headlineText" cascade="true">O nás</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<section class="perex theme row">
				<div class="col-sm-12">
					<p>Kácení Šarapatka je <strong>rodinná firma</strong>. Arboristika a péče o stromy se u nás dědí. O stromy pečujeme <strong>již více než 20 let</strong>, působíme na českém trhu od roku 1996. 
					Začali jsme podnikat v oboru realizace a údržba zeleně. Naší specializací se postupně stala <strong>ARBORISTIKA</strong> - péče o vzrostlé dřeviny.</p> 
					<p>Mluví za nás <a href="<c:url value='/reference' />">reference</a>, ale i <a href="https://www.firmy.cz/detail/319834-rizikove-kaceni-a-udrzba-stromu-sarapatka-brno-veveri.html">komentáře našich spokojených zákazníků</a>.</p>
					<app:inquiry-btn />
				</div>
			</section>
			<div class="row">
				<div class="col-sm-12">
					<h2 class="theme">Historie firmy</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">	
					<p>V roce 2003 jsme získali <strong>Evropský arboristický certifikát European treeworker</strong>, který nás <strong>opravňuje k ošetřování stromů 
					vzácných a památkově chráněných</strong>. Od roku 2006, kdy se jeden z majitelů -  Tomáš Šarapatka - vrací z půlročního působení v americké firmě 
					Bartlett tree expert, se stáváme <strong>členy mezinárodní arboristické společnosti ISA - International Society of Arboriculture</strong>. 
					Každoročně se zúčastňujeme <strong>mistrovsví republiky ve stromolezení a národních arboristických konferencí</strong>. 
					Jsme zapojení do nejrůznějších projektů propagujících kvalitní arboristiku např. <a href="http://zdravestromy.cz/">Zdravé stromy pro zítřek</a>.</p>
					<p>Po roce 2010 vznikla postupná <strong>spolupráce s Mendelovou univerzitou</strong> v Brně, konkrétně s ústavem Nauky o dřevě, kde se Tomáš stává akademickým pracovníkem a podílí se na výuce předmětů praktické podnikání 
					v arboristice a stromolezení.</p>
				</div>
				<div class="col-sm-6">
					<p>V průběhu let 2013 a 2014 <strong>organizuje historicky první ročník akademických závodů ve stromolezení</strong>. Rozjíždí se také <strong>nová certifikace český certifikovaný arborista</strong>
					<a href="http://arborista.mendelu.cz/">arborista.mendelu.cz</a>, kde se Tomáš stává komisařem. Rok 2016 reaguje na personální změny na Mendelově univerzitě a vzniká občanské sdružení arboristická akademie, jehož náplní je 
					<strong>propagační a školící činnost v arboristice</strong>. Stále je tu snaha nejnovější poznatky z oboru aplikovat i v naší práci a <strong>snažit se bojovat v tržním prostředí s amatérským přístupem neproškolených firem</strong>. 
					Víme, že neustálé vzdělávání je důležitým předpokladem naší profese. <strong>Sledujeme nové trendy</strong>, díky kterým se my i firma posouváme vpřed. Rádi zkvalitňujeme naše služby a <strong>udržujeme naši firmu na profesionální úrovni</strong>.</p>
					<p>Náš tým tvoří: 6 stálých stromolezců, 3 groundmani a 1 pracovník v sekci palivového dřeva.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h2 class="theme">Naše strojové vybavení:</h2>
				    <ul>
					    <li>2x sklápěč (1xDucato, 1x gazelle)</li>
					    <li>1x 4x4 Mitsubishi L200 se sklopným přívěsem</li>
					    <li>2x stěpkovač  (1x Jensen A 540, 1x Vermeer BC 160XL)</li>
					    <li>2x pařezová fréza ( 1x vermeer sc 372, 1x vermeer sc 130)</li>
					    <li>1x přívěsný štípač AMR se štípací silou 12t</li>
					    <li>1x stacionární štípací automat profiloger</li>
				    </ul>
			    </div>
		    </div> 
		    <div class="row photos">
		      <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-o-nas.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-o-nas.jpg' />" alt="Kácení Šarapatka - o nás" />
			    </a>
			  </div>	
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-o-nas-1.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-o-nas-1.jpg' />" alt="Kácení Šarapatka - o nás - 1" />
			    </a>
			  </div>
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-o-nas-2.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-o-nas-2.jpg' />" alt="Kácení Šarapatka - o nás - 2" />
			    </a>
			  </div>
			  <div class="col-xs-6 col-sm-3">
			    <a href="<c:url value='/resources/imgs/kaceni-sarapatka-o-nas-3.jpg' />" class="thumbnail">
			      <img src="<c:url value='/resources/imgs/_kaceni-sarapatka-o-nas-3.jpg' />" alt="Kácení Šarapatka - o nás - 3" />
			    </a>
			  </div>
			</div>       
	    </div>
    </tiles:putAttribute>
</tiles:insertDefinition>