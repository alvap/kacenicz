<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="aboutUs" scope="request" />
<tiles:insertDefinition name=".layout.public.pages">
	<tiles:putAttribute name="title">GDPR - Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="headlineText" cascade="true">GDPR</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<h1>Souhlas se zpracováním osobních údajů</h1>
			<ol class="text-justify">
				<li>Uděluji tímto souhlas firmě Kácení Šarapatka s.r.o. se sídlem Karásek 2249/1g, Řečkovice, 621 00 Brno, IČ:  06667490, zapsané v Obchodním rejstříku vedeném u Krajského soudu v Brně pod sp. zn. C 103546, zastoupené Ing. Tomášem Šarapatkou (dále jen „správce“), aby ve smyslu nařízení č. 679/2016 o ochraně osobních údajů fyzických osob (dále jen „GDPR“) zpracovával tyto osobní údaje:
					<ul> 
						<li>Vaše jméno a příjmení (popř. obchodní firma)</li>
						<li>adresa (nebo sídlo společnosti)</li>
						<li>e-mailová adresa</li>
						<li>telefonní číslo</li>
					</ul>
				</li>
				<li>Tyto osobní údaje je nutné zpracovat pro účel vyplnění poptávkového formuláře na webových stránkách www.kaceni.cz. Údaje budou správcem zpracovány po dobu nezbytně nutnou k naplnění účelu, což je vyřízení Vašeho požadavku, v případě poskytnutí služby budou údaje ponechány ještě během realizace práce, maximálně do doby pro vyřízení reklamace (následně po provedení práce). Poté budou automaticky smazány.</li> 
				<li>S výše uvedeným zpracováním uděluji výslovný souhlas. Souhlas lze vzít kdykoliv zpět, a to zasláním e-mailu na adresu tomas@kaceni.cz.</li>
				<li>Zpracování osobních údajů je prováděno správcem, který nejmenoval pověřence pro ochranu osobních údajů, nepověřil zpracováním osobních údajů žádného zpracovatele ani neurčil zástupce pro plnění povinností ve smyslu GDPR.</li> 
				<li>Dle GDPR máte právo:
					<ul>
						<li>Vzít souhlas kdykoliv zpět</li>
						<li>Požadovat informaci, jaké vaše osobní údaje zpracováváme</li>
						<li>Požádat vysvětlení ohledně zpracování osobních údajů</li>
						<li>Vyžádat si přístup k těmto údajům a tyto nechat aktualizovat nebo opravit</li>
						<li>Požadovat opravu, výmaz osobních údajů, omezení zpracování a vznést námitku proti zpracování</li>
						<li>Všechna Vaše práva můžete uplatnit tak, že nás kontaktujete na e-mailové adrese tomas@kaceni.cz.</li>
						<li>Stížnost můžete podat u dozorového úřadu, kterým je Úřad pro ochranu osobních údajů.</li>
					</ul>
				</li>	
			</ol>
			<p>Tyto podmínky nabývají účinnosti dnem 25.5.2018.</p>
		</div>	
    </tiles:putAttribute>
</tiles:insertDefinition>