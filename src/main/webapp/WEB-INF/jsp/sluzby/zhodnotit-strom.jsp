<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="services" scope="request" />
<c:set var="selectedSubItem" value="valorize" scope="request" />
<tiles:insertDefinition name=".layout.public.subpages">
	<tiles:putAttribute name="title">Zhodnotit strom | Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="serviceContent" cascade="true">
		<app:serviceHeadline text="Zhodnotit strom" />
		<div class="row">
			<div class="col-sm-12">
				<section class="theme">
					<p class="perex">
					Inženýrská činnost v oblasti arboristky. Poskytuje informace o bezpečnosti a perspektivě vašich stromů. Mimo vizuálního hodnocení stromů zahrnuje i přístrojovou diagnostiku, která umožňuje detailnější průzkum problematických oblastí.
					</p>
				</section>
				<section class="theme">
					<p class="font-size-md">Kácení Šarapatka nabízí s využitím moderních přístrojů:</p>
					<h2 class="theme">Inventarizaci dřevin</h2>
					<p class="theme-indent">Součástí inventarizace je lokalizace (zaměření GPS), zjištění základních informací (druh stromu, rozměry), zjištění současného stavu( vitalita, stabilita, zdravotní stav) a návrh pěstebních opatření. Vše zaneseno na portál <a href="http://www.stromypodkontrolou.cz" title="Stromy pod kontrolou">www.stromypodkontrolou.cz</a></p>
					<h2 class="theme">Tahovou zkoušku</h2>
					<p class="theme-indent">Tahová zkouška zjišťujě odolnost celého stromu proti vývratu, zlomu a krutu.</p>
					<h2 class="theme">Akustický tomograf</h2>
					<p class="theme-indent">Akustický tomograf zajištuje detekci defektů (dutiny, praskliny) a jejich rozsah.</p>
					<h2 class="theme">Radar kořenů</h2>
					<p class="theme-indent">Zjišťuje rozsah kořenového systému. Je dobrým pomocníkem při plánování terénních úprav v okolí stromu. </p>
				</section>	
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>