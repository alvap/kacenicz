<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="services" scope="request" />
<c:set var="selectedSubItem" value="trim" scope="request" />
<tiles:insertDefinition name=".layout.public.subpages">
	<tiles:putAttribute name="title">Ořezat strom | Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="serviceContent" cascade="true">
		<app:serviceHeadline text="Ořezat strom" />
		<div class="row">
			<div class="col-sm-12">
				<section class="theme">
					<!-- FIXME: not existing link standard.nature.cz -->
					<p class="perex">Zvolíme takový typ řezu, který zajistí vašim stromům perspektivu a zlepší provozní bezpečnost v jejich okolí. Využíváme všech typu řezů, které jsou navrženy dle platných arboristických standardů AOPK.</p> 
				</section>
				<section class="theme">
				<p class="font-size-md">Kácení Šarapatka nabízí:</p>
				<h2 class="theme">Bezpečnostní řez</h2>
			    <p class="theme-indent">Tento řez řeší aktuální provozní bezpečnost stromu. Jeho pomocí se odstraňují suché, poškozené a ulomené větve. 
					Doporučujeme jej, není-li efektivní investovat do nákladnějšího zdravotního řezu. 
					Bezpečnostní řez provádíme dle aktuální potřeby - ideálně od dubna do září. 
					<!-- FIXME chybi foto pred a po --> 
				<h2 class="theme">Zdravotní řez</h2>
				<p class="theme-indent">Tento řez zabezpečí provozní bezpečnost stromu i v budoucnu. Jeho pomocí řešíme i větve, které svým růstem v budoucnu mohou narušit 
					provozní bezpečnost, tedy tlakové a kodominantní větvení v koruně. Zdravotní řez provádíme ve vegetačním období -  od dubna do září .
				<!-- FIXME chybi foto pred a po --> 
				<h2 class="theme">Redukční řez</h2>
				<p class="theme-indent">Tento řez využíváme pro celkovou či jednostrannou redukci koruny. Většinou jej provádíme u stromů rostoucích v blízkosti domů, komunikací či elektrického vedení nebo jiných překážek.  Větve neodstraňujeme celé, jen je odlehčíme, čímž zachováme tvar stromu. Zároveň posuneme těžiště  koruny a tím zlepšíme celkovou provozní bezpečnost stromu. 
					Redukční řez provádíme dle aktuální potřeby - ideálně od dubna do září. 
				<h2 class="theme">Obvodová redukce</h2> 
				<p class="theme-indent">Pomocí této metody ořezu stromů, zkrátíme koncové části větví (max.20% hmoty) po celém obvodu. 
					Výrazně tak posuneme jejich těžiště směrem k hlavnímu kmeni. Zachováme tak  přirozený tvar, ale výrazně snížíme riziko vylomení větví. 
					Obvodovou redukci je vhodné provádět zejména v předjaří.
				 <!-- Foto před a po: lepsi kvalita nekde?
				 http://www.kaceni.cz/files/lpa_tpnovice_ped_zsahem.JPG
				 http://www.kaceni.cz/files/lpa_tpnovice_po....JPG -->
				</section>	
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>