<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="services" scope="request" />
<c:set var="selectedSubItem" value="education" scope="request" />
<tiles:insertDefinition name=".layout.public.subpages">
	<tiles:putAttribute name="title">Školení a vzdělávání | Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="serviceContent" cascade="true">
		<app:serviceHeadline text="Školení a vzdělávání" />
		<div class="row">
			<div class="col-sm-12">
					<p class="perex">
					Získejte teoretické i praktické zkušenosti v oboru a staňte se odborníkem, který splňuje základní předpoklady pro vykonávání kvalitní a kvalifikované práce 
					v arboristických disciplínách. Systém certifikací současně podporuje vzdělávání a kvalifikační růst pracovníků v jednotlivých úrovních péče o stromy. 
					Podrobné informace o programu najdete na webu <a href="http://www.arboristickaakademie.cz">arboristickaakademie.cz</a>
					</p>
			</div>	
		</div>
		<c:if test="${!empty newsList}">
			<dl class="news-education news">								
				<c:forEach items="${newsList}" var="news">
					<dt><joda:format value="${news.date}"/></dt>
					<dd><c:out value="${news.text}" escapeXml="false" /></dd>
				</c:forEach>
			</dl>	
		</c:if>
	</tiles:putAttribute>
</tiles:insertDefinition>