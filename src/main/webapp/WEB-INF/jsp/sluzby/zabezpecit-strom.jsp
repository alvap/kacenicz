<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="services" scope="request" />
<c:set var="selectedSubItem" value="secure" scope="request" />
<tiles:insertDefinition name=".layout.public.subpages">
	<tiles:putAttribute name="title">Zabezpečit strom | Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="serviceContent" cascade="true">
		<app:serviceHeadline text="Zabezpečit strom" />
		<div class="row">
			<div class="col-sm-12">
				<section class="theme">
					<p class="perex">Potřebujete zvýšit provozní bezpečnost staticky narušených stromů? Nedílnou součástí této práce je instalace bezpečnostních vazeb, 
					jejich evidence a pravidelná kontrola.</p>
				</section>
				<section class="theme">
					<p class="font-size-md">Kácení Šarapatka nabízí:</p>
					<h2 class="theme">Zabezpečení kmene proti rozlomení</h2>
					<div class="theme-indent">
						<p>Pokud zjistíme poškození, narušující provozní bezpečnost, strom zajistíme pomocí  bezpečnostních vazeb:</p>
						<ul class="list-unstyled theme-indent">
							<li><h3>Dynamickou  vazbou</h3>
								<p>Systém využívající pletené polypropylenové lano. Pomocí lana ponecháme koruně stromu volnost pohybu, přičemž zabráníme mezním hodnotám výkyvu větví. 
								Lano zároveň slouží jako záchytný element při případném rozlomení.</p></li>
							<li><h3>Statickou vazbou</h3><p>Zajišťujeme stromy, které jsou  výrazně staticky oslabené prasklinami, dutinami nebo výrazně poškozeným dřevem zejména vyskytujících se v místech větvení.
							 Pro tuto vazbu používáme ocelových lan s nulovou pružností, které stromu poskytnou doživotní oporu.</p></li>
							 <!-- FIXME http://www.kaceni.cz/files/vazba.JPG -->
						</ul>
					</div>	
				</section>	
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>