<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="services" scope="request" />
<c:set var="selectedSubItem" value="plant" scope="request" />
<tiles:insertDefinition name=".layout.public.subpages">
	<tiles:putAttribute name="title">Vysadit strom | Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="serviceContent" cascade="true">
		<app:serviceHeadline text="Vysadit strom" />
		<div class="row">
			<div class="col-sm-12">
				<section class="theme">
					<p class="perex">Kácení starých stromů a výsadba nových, mohou souviset. Postaráme se o rychlé, bezpečné a dokonalé odstranění přestárlého stromu a výsadbu nového stromu. 
					Jedna etapa tak končí a druhá začíná. K obnově zeleně přistupujeme zodpovědně a sázíme jen tam, kde to má smysl.</p>
				</section>
				<section class="theme">
				<p class="font-size-md">Kácení Šarapatka nabízí:</p>
					<h2 class="theme">Výsadbu alejových a solitérních stromů</h2>
					<p class="theme-indent">K výsadbě nových stromů používáme kvalitní české výpěstky balové nebo prostokořenné.</p>
					<h2 class="theme">Osazování hrází a svahů</h2>
					<p class="theme-indent">V rámci protierozních opatření sázíme keře či stromy, které svým kořenovým systémem zpevňují svahy a hráze.</p>
					<h2 class="theme">Ozeleňování a rekultivace parkových ploch</h2>
					<p class="theme-indent">Na základě studií a osazovacích plánů realizujeme výsadby dřevin, keřů, bylin a travnatých ploch.</p>
					<h2 class="theme">Zakládaní břehových porostů, biokoridorů a USES</h2>
					<p class="theme-indent">Vytváříme zeleň v místech, kde je třeba zabezpečit biodiverzitu a spojovat jednotlivé biotopy v krajině.</p>
				</section>	
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>