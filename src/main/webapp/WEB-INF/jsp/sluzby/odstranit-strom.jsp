<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="services" scope="request" />
<c:set var="selectedSubItem" value="remove" scope="request" />
<tiles:insertDefinition name=".layout.public.subpages">
	<tiles:putAttribute name="title">Odstranit strom | Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="serviceContent" cascade="true">
		<app:serviceHeadline text="Odstranit strom" />
		<div class="row">
			<div class="col-sm-12">
				<section class="theme">
					<p class="perex">Rozhodli jste odstranit strom? Než zavoláte Kácení Šarapatka, je dobré zjistit, 
					<a href="http://www.mzp.cz/cz/news_130703_kaceci_vyhlaska">zda potřebujete povolení.</a></p>
				</section>
				<section class="theme">
				<p class="font-size-md">Kácení Šarapatka nabízí:</p>
					<h2 class="theme">Kácení</h2>
					<ul class="list-unstyled theme-indent">
						<li><h3>Rizikové kácení<small>, tj. kácení ve ztížených podmínkách</small></h3></li>
						<li><h3>Kácení nebezpečně situovaných stromů<small>, tj. ( havarijní stav, svahy, skály, hřbitovy, zástavba, elektrické vedení a další.)</small></h3>
							<p>Jedná se o postupné kácení stromu od vrcholu a spouštění částí stromu tak, aby nedošlo k poškození okolního majetku.</p></li>
						<li><h3>Kácení a ořez pomocí plošiny či jeřábu</h3><p>Nedovolují-li podmínky použití lanových technik, nastupuje těžká technika.</p></li>
						<li><h3>Klasické kácení</h3><p>Při údržbě parků, lesoparků a místech, kde je dostatek prostoru pro bezpečné pokácení.</p></li>
					</ul>
					<h2 class="theme">Odstranění pařezů</h2>
					<p class="theme-indent">Pařezy, které zůstávají po odstranění stromu, odfrézujeme do potřebné úrovně.
					Disponujeme velkokapacitní frézou na odstranění extrémních pařezů. V případě omezeného přístupu používáme ruční frézu o šířce 80cm - <a href="<c:url value='/sluzby/frezovani-parezu' />" title="Kácení Šarapatka - frézování pařezů">další informace o frézování pařezů</a></p>
					<h2 class="theme">Úklid a likvidaci pokáceného dřeva a větví</h2>
					<ul class="list-unstyled theme-indent">
						<li><h3>Odkup dřeva</h3><p>Nemáte-li o dřevo z pokáceného stromu zájem, rádi využijeme možnost jeho odkoupení. Ušetříte tak část nákladů spojených s kácením.</p></li>
						<li><h3>Likvidace větví štěpkováním</h3><p>Větve do průměru cca 13cm likvidujeme štěpkovačem zn. Vermeer nebo Jensen</p></li>
						<li><h3>Prodej a štípání palivového dřeva</h3><p>Zajišťujeme také štípaní a prodej palivového dřeva. Více informací se dozvíte na <a href="http://www.palivove-drevo-brno.cz" title="Prodej palivového dřeva v Brně a okolí">www.palivove-drevo-brno.cz</a></p></li>
					</ul>
				</section>	
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>