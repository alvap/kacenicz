<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="services" scope="request" />
<c:set var="selectedSubItem" value="frezovani" scope="request" />
<tiles:insertDefinition name=".layout.public.subpages">
	<tiles:putAttribute name="title">Frézování pařezů | Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="serviceContent" cascade="true">
		<app:serviceHeadline text="Frézování pařezů" />
		<div class="row">
			<div class="col-sm-12">
				<section class="theme">
					<p class="perex">Pařezy, které zůstávají po odstranění stromu, odfrézujeme do potřebné úrovně.</p>
				</section>
				<section class="theme">
				<h2 class="theme">Co obnáší frézování:</h2>
				<ul class="list-unstyled theme-indent">
					<li>Frézování pařezů je nejrychlejší způsob likvidace pařezů, umožňuje odstranění pařezů bez nutnosti jejich vykopání.</li>
					<li>Umožňuje odstraňování pařezů až 30 cm pod úroveň terénu pomocí pařezových fréz.</li>
					<li>Pařezy, které zůstávají po odstranění stromu, odfrézujeme do potřebné úrovně.</li>
					<li>Disponujeme i velkokapacitní frézou na odstranění extrémních pařezů. V případě omezeného přístupu používáme ruční frézu o šířce 80 cm.</li>
					<li>Místo po odfrézovaném pařezu je možné ihned zavézt zeminou.</li>
					<li>Z pařezu zůstává rozdrcená dřevní hmota, kterou je možno odstranit nebo ji můžete dále využít.</li> 
				</ul>
				<h2 class="theme">Ceník frézování:</h2>
					<p>Ceny vycházejí z průměru frézovaného pařezu. Cena za dopravu je 10 Kč/km.</p>
					<table class="table" style="width:auto;">
						<thead>
							<tr>
								<th>Průměr pařezu</th>
								<th>Cena (bez DPH)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0 - 20 cm</td>
								<td>250 Kč</td>
							</tr>
							<tr>
								<td>20 - 30 cm</td>
								<td>400 Kč</td>
							</tr>
							<tr>
								<td>30 - 40 cm</td>
								<td>500 Kč</td>
							</tr>
							<tr>
								<td>40 - 50 cm</td>
								<td>650 Kč</td>
							</tr>
							<tr>
								<td>50 - 60 cm</td>
								<td>800 Kč</td>
							</tr>
							<tr>
								<td>60 - 70 cm</td>
								<td>950 Kč</td>
							</tr>
							<tr>
								<td>70 - 80 cm</td>
								<td>1200 Kč</td>
							</tr>
							<tr>
								<td>80 - 90 cm</td>
								<td>1350 Kč</td>
							</tr>
							<tr>
								<td>90 - 100 cm</td>
								<td>1500 Kč</td>
							</tr>
							<tr>
								<td>nad 100 cm</td>
								<td>dle dohody</td>
							</tr>
						</tbody>
					</table>				
					<p>Cenu dále ovlivňuje:</p>
					<ul>
						<li>Požadovaná hloubka frézování (ceník frézování pařezů 20cm pod úroveň terénu)</li>
						<li>Počet frézovaných pařezů (množstevní sleva)</li>
						<li>Průměr frézovaných pařezů (měří se u země v nejnižším místě)</li>
						<li>Druh dřeviny (tvrdost)</li>
						<li>Typ stanoviště (svažitost, půda, přístupnost)</li>
						<li>Odstranění dřevní hmoty</li>
					</ul>
				</section>	
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>