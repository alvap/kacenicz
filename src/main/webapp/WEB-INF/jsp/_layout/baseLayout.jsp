<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<!DOCTYPE html>
<html>
    <head>
    	<title><tiles:insertAttribute name="title" /></title>
    	<ui:meta />
    	<%-- FIXME: add production url --%>
        <asset:css url="/resources/css/app.css" isForProductionEnvironment="true" />
        <%-- ====================================================================================
		CSS - local (development) - raw .less files are used as styles - they are compiled to css
		in runtime by included less.js script. Compiled less files are cached in a browser. To force reloading 
		less files application property 'developmentMode' must be set to true, recompilation of 
		.less files is very slow (should be enabled only if you are changing .less files). Note that watching
		for less files changes doesn't recognize 'ímported' (nested) .less files - a manual browser
		cache cleaning is necessary in this case.
		=====================================================================================--%>
	    <%-- Bootstrap library --%>
        <asset:less url="/resources/less/bootstrap-customization.less" />
	    <%-- Common application styles and Boootstrap overrides --%>
	    <asset:css url="/resources/min-assets/magnific-popup.css" />
        <asset:css url="/resources/min-assets/magnific-popup.css" isForProductionEnvironment="true" />
        <asset:less url="/resources/less/app.less" />
        <tiles:insertAttribute name="css" ignore="true" />
        <asset:IE10ViewportBugWorkaroundCSS />
        <asset:html5SupportForIE />
    </head>
  	<body>
  		<header class="navbar navbar-inverse">
  			<%-- collapsable menu --%>
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="<c:url value='/' />"><img src="<c:url value='/resources/imgs/kaceni-sarapatka-logo.png' />" alt="Kácení Šaraptka - logo" class="img-responsive" /></a>
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>
				</div>
				<nav id="navbar" class="collapse navbar-collapse" role="navigation">
					<tiles:insertAttribute name="menu" ignore="true" />
				</nav>
			</div>
		</header>
		<tiles:insertAttribute name="headline" ignore="true" />
		<tiles:insertAttribute name="content" />
		<tiles:insertAttribute name="prefoo" ignore="true" />
	    <footer>
			<div class="container">
				<div class="container-inner">
					<joda:format value="<%= new org.joda.time.DateTime() %>" pattern="yyyy" var="thisYear" />
					<c:set var="originYear">2016</c:set>
					<div><p><img src="<c:url value='/resources/imgs/logo-black.png' />" alt="Kácení Šaraptka - logo" class="img-responsive" /></p></div>
					<div><p><a href="https://www.facebook.com/kaceni.cz/" title="Kácení Šarapatka - Facebook"><i class="fa fa-facebook"></i></a></p></div>
					<div><p><b>Copyright &copy; ${copy} ${originYear} <c:if test="${thisYear != originYear}"> - ${thisYear}</c:if></b><br/>
							Kácení Šarapatka s.r.o.<br/>
							Karásek 2249/1g, Řečkovice, 621 00 Brno</p>
							<p id="gdpr"><a href="<c:url value='/gdpr' />" title="Kácení Šarapatka - GDPR" class="link-text"><b>GDPR</b></a></p>
					</div>
				</div>			
			</div>
		</footer>
	    <asset:IE10ViewportBugWorkaroundJS />
	    <asset:lessCompiler url="/resources/tools/less-2.6.1.min.js" />
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <asset:script url="/resources/min-assets/bootstrap-collapse.min.js"  />
	    <asset:script url="/resources/min-assets/bootstrap-collapse.min.js" isForProductionEnvironment="true" />
	    <asset:script url="/resources/min-assets/jquery.magnific-popup.min.js" />
	    <asset:script url="/resources/min-assets/jquery.magnific-popup.min.js" isForProductionEnvironment="true" />
	    <script type="text/javascript">
		    $(document).ready(function() { $('.photos').magnificPopup({	delegate: 'a', type: 'image', gallery: { enabled: true, navigateByImgClick: true } });	});
	    </script>
	    <tiles:insertAttribute name="javascript" ignore="true" />
	    <tiles:insertAttribute name="ga" ignore="true" />
   </body>
</html>
