<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<div class="container" id="prefoo">
	<section id="prefoo-text"><p>Pečujeme o stromy již více než 20 let</p></section>
	<section id="prefoo-logos">
		<div class="row">
			<p class="col-sm-offset-3 col-sm-3 text-center">
				<a href="http://www.arboristickaakademie.cz" title="Arboristická akademie"><img src="<c:url value='/resources/imgs/logo-arboristicka-akademie.png' />" /></a>	    		
			</p>
			<p class="col-sm-3 text-center">
				<a href="http://www.stromypodkontrolou.cz" title="Stromy pod kontrolou"><img src="<c:url value='/resources/imgs/logo-stromy-pod-kontrolou.png' />" /></a>
			</p>
	  </div>
	</section>
</div>	