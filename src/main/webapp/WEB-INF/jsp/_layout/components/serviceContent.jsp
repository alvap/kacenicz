<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<div class="container container-service">
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-5 container-service-menu">
			<jsp:include page="/WEB-INF/jsp/_layout/components/servicesMenu.jsp" />
    	</div>
    	<div class="col-lg-9 col-md-8 col-sm-7">
    		<tiles:insertAttribute name="serviceContent" />
    	</div>
    </div>
</div>    