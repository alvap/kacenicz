<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<asset:less url="/resources/less/form.less" />
<asset:less url="/resources/less/admin.less" />
<asset:css url="/resources/css/admin.css" isForProductionEnvironment="true" />
<tiles:insertAttribute name="pageCss" ignore="true" />    