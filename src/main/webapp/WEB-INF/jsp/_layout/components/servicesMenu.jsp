<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<ul class="list-unstyled" id="services-menu">
	<li class="${selectedSubItem == 'remove' ? 'active' : ''}"><a href="<c:url value='/sluzby/odstranit-strom' />" title="Kácení Šarapatka - odstranit strom">Odstranit strom</a></li>
	<li class="${selectedSubItem == 'frezovani' ? 'active' : ''}"><a href="<c:url value='/sluzby/frezovani-parezu' />" title="Kácení Šarapatka - frézování pařezů">Frézování pařezů</a></li>
	<li class="${selectedSubItem == 'plant' ? 'active' : ''}"><a href="<c:url value='/sluzby/vysadit-strom' />" title="Kácení Šarapatka - vysadit strom">Vysadit strom</a></li>
	<li class="${selectedSubItem == 'trim' ? 'active' : ''}"><a href="<c:url value='/sluzby/orezat-strom' />" title="Kácení Šarapatka - ořezat strom">Ořezat strom</a></li>
	<li class="${selectedSubItem == 'secure' ? 'active' : ''}"><a href="<c:url value='/sluzby/zabezpecit-strom' />" title="Kácení Šarapatka - zabezpečit strom">Zabezpečit strom</a></li>
	<li class="${selectedSubItem == 'valorize' ? 'active' : ''}"><a href="<c:url value='/sluzby/zhodnotit-strom' />" title="Kácení Šarapatka - zhodnoti strom">Zhodnotit strom</a></li>
	<li class="${selectedSubItem == 'education' ? 'active' : ''}"><a href="<c:url value='/sluzby/skoleni-a-vzdelavani' />" title="Kácení Šarapatka - školení a vzdělávání">Školení a vzdělávání</a></li>
</ul>
<app:inquiry-btn cssClass="block" />