<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<ul class="nav navbar-nav navbar-right theme-inverse">
	<li class="${selectedItem == 'inquiryList' ? 'active' : ''}"><a href="<c:url value='/administrace/poptavky/prehled' />">Přehled poptávek</a></li>
	<li class="${selectedItem == 'news' ? 'active' : ''}"><a href="<c:url value='/administrace/novinky' />">Správa novinek</a></li>
	<li class="${selectedItem == 'newsEducation' ? 'active' : ''}"><a href="<c:url value='/administrace/novinky-vzdelavani' />">Správa novinek - vzdělávání</a></li>
	<li><a href="<c:url value='/security/logout' />">Odhlásit se</a></li>
</ul>