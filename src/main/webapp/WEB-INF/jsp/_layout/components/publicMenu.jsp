<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<ul class="nav navbar-nav navbar-right theme-inverse">
	<li class="${selectedItem == 'aboutUs' ? 'active' : ''}"><a href="<c:url value='/o-nas' />" title="Kácení Šarapatka - O nás">O nás</a></li>
	<li class="${selectedItem == 'services' ? 'active' : ''}"><a href="<c:url value='/sluzby' />" title="Kácení Šarapatka - Služby">Služby</a></li>
	<li class="${selectedItem == 'news' ? 'active' : ''}"><a href="<c:url value='/novinky' />" title="Kácení Šarapatka - Novinky">Novinky</a></li>
	<li class="${selectedItem == 'portfolio' ? 'active' : ''}"><a href="<c:url value='/reference' />" title="Kácení Šarapatka - Reference">Reference</a></li>
	<li class="${selectedItem == 'contact' ? 'active' : ''}"><a href="<c:url value='/kontakt' />" title="Kácení Šarapatka - Kontakt">Kontakt</a></li>
</ul>