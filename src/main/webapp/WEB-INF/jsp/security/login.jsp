<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>

<%-- 
	This login page is displayed if unauthenticated user tries to access a protected resource.
	The page is common to facilities and advertisers and it is responsibility of the
	UserDetailsService implementation to search both facilities and advertisers repository. 
--%>
<spring:message code="page.login" var="h" />
<c:set var="loginPage" value="true" scope="request" />
<tiles:insertDefinition name=".layout.exception">
	<tiles:putAttribute name="title" type="string">${h}</tiles:putAttribute>
	<tiles:putAttribute name="headlineText" type="string" cascade="true">${h}</tiles:putAttribute>
    <tiles:putAttribute name="content" type="string">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-8 col-md-10 col-sm-12">
	    			<p class="alert alert-warning"><spring:message code="page.login.text" /></p>
	    			<spring-sec-ext:loginForm  />
					<p><a href='<c:url value="/" />'><spring:message code="exception.text.orContinueOnHomePage" /></a></p>
				</div>
			</div>		
		</div>	
	</tiles:putAttribute>
</tiles:insertDefinition>
