<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<tiles:insertDefinition name=".layout.public.pages">
	<tiles:putAttribute name="css">
		<asset:less url="/resources/less/form.less" />
		<asset:less url="/resources/less/inquiry.less" />
		<asset:css url="/resources/css/inquiry.css" isForProductionEnvironment="true" />
	</tiles:putAttribute>
	<tiles:putAttribute name="title">Poptávka | Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="headlineText" cascade="true">Poptávka</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<div class="container">
			<ui:message />
			<form:form modelAttribute="inquiry" cssClass="form-horizontal" name="${randomFormName}">
				<div class="row">
					<div class="col-sm-6">
						<fieldset>
							<legend>1. Kontaktní údaje</small></legend>
							<spring-ext:formGroup path="customer.firstName">
								<spring-ext:label path="customer.firstName" cssClass="control-label col-sm-3" />
								<div class="col-sm-9">
									<spring-ext:input path="customer.firstName" cssClass="form-control"/>
								</div>
							</spring-ext:formGroup>
							<spring-ext:formGroup path="customer.lastName">	
								<spring-ext:label path="customer.lastName" cssClass="control-label col-sm-3" />
								<div class="col-sm-9">
									<spring-ext:input path="customer.lastName" cssClass="form-control"/>
								</div>
							</spring-ext:formGroup>
							<spring-ext:formGroup path="customer.companyName">	
								<spring-ext:label path="customer.companyName" cssClass="control-label col-sm-3" />
								<div class="col-sm-9">
									<spring-ext:input path="customer.companyName" cssClass="form-control"/>
								</div>
							</spring-ext:formGroup>
							<spring-ext:formGroup path="customer.phoneNumber">	
								<spring-ext:label path="customer.phoneNumber" cssClass="control-label col-sm-3" />
								<div class="col-sm-9">
									<spring-ext:input path="customer.phoneNumber" cssClass="form-control"/>
								</div>
							</spring-ext:formGroup>
							<spring-ext:formGroup path="customer.userAccount.email">	
								<spring-ext:label path="customer.userAccount.email" cssClass="control-label col-sm-3" />
								<div class="col-sm-9">
									<spring-ext:input path="customer.userAccount.email" cssClass="form-control"/>
								</div>
							</spring-ext:formGroup>
						</fieldset>	
					</div>
					<div class="col-sm-6">	
						<fieldset>
							<legend>2. Adresa <small>(místa provedení požadované služby)</small></legend>
							<spring-ext:formGroup path="address.street">	
								<spring-ext:label path="address.street" cssClass="control-label col-sm-3" />
								<div class="col-sm-9">
									<spring-ext:input path="address.street" cssClass="form-control"/>
								</div>
							</spring-ext:formGroup>
							<spring-ext:formGroup path="address.city">	
								<spring-ext:label path="address.city" cssClass="control-label col-sm-3" />
								<div class="col-sm-9">
									<spring-ext:input path="address.city" cssClass="form-control"/>
								</div>				
							</spring-ext:formGroup>
							<spring-ext:formGroup path="address.zip">	
								<spring-ext:label path="address.zip" cssClass="control-label col-sm-3" />
								<div class="col-sm-9">
									<spring-ext:input path="address.zip" cssClass="form-control"/>
								</div>
							</spring-ext:formGroup>
						</fieldset>
					</div>
				</div>
				<spring-ext:formGroup path="message">	
					<div class="col-sm-6">
						<fieldset>
						<legend class="form-label">3. Předmět</legend>
						<div class="row">
						<div class="col-sm-12">
							<spring-ext:textarea path="message" cssClass="form-control" rows="8"/>
						</div>
						</div>
						</fieldset>
					</div>
				</spring-ext:formGroup>
				<section class="theme">
				<div class="form-group">
					<div class="col-sm-12">	
						<fieldset>
						<legend>4. Vyberte, prosím, o které z nabízených služeb, máte zájem</legend>	
						<c:forEach items="${workTypeList}" var="workType" varStatus="i">
 							<div class="checkbox checkbox-bold">
		   						<spring-ext:checkbox path="workTypeList[${i.index}]" labelMessageKey="workType.${workType.workType}" value="${workType}" data-toggle="collapse" data-target="#${workType.workType}" />
		   						<form:hidden path="workList[${i.index}].workType" value="${workType.id}#${workType.workType}" />
		   					</div>
		   					<c:choose>
		   						<c:when test="${workType.workType == 'SINGLE_FELLING'}">
		   							<jsp:include page="/WEB-INF/jsp/_include/formPartSingleTreeFelling.jsp">
		   								<jsp:param value="${i.index}" name="index"/>
		   								<jsp:param value="${workType.workType}" name="workType"/>
		   							</jsp:include>
		   						</c:when>
		   						<c:when test="${workType.workType == 'SINGLE_RISK_FELLING'}">
		   							<jsp:include page="/WEB-INF/jsp/_include/formPartSingleTreeFelling.jsp">
		   								<jsp:param value="${i.index}" name="index"/>
		   								<jsp:param value="${workType.workType}" name="workType"/>
		   							</jsp:include>
		   						</c:when>
		   						<c:when test="${workType.workType == 'MULTIPLE_FELLING'}">
		   							<jsp:include page="/WEB-INF/jsp/_include/formPartOtherType.jsp">
		   								<jsp:param value="${i.index}" name="index"/>
		   								<jsp:param value="${workType.workType}" name="workType"/>
		   							</jsp:include>
		   						</c:when>
		   						<c:when test="${workType.workType == 'MULTIPLE_RISK_FELLING'}">
		   							<jsp:include page="/WEB-INF/jsp/_include/formPartOtherType.jsp">
		   								<jsp:param value="${i.index}" name="index"/>
		   								<jsp:param value="${workType.workType}" name="workType"/>
		   							</jsp:include>
		   						</c:when>
		   						<c:when test="${workType.workType == 'MILLING_OF_STUMPS'}">
		   							<jsp:include page="/WEB-INF/jsp/_include/formPartOtherType.jsp">
		   								<jsp:param value="${i.index}" name="index"/>
		   								<jsp:param value="${workType.workType}" name="workType"/>
		   							</jsp:include>
		   						</c:when>
		   						<c:when test="${workType.workType == 'TREE_TRIMMING'}">
		   							<jsp:include page="/WEB-INF/jsp/_include/formPartOtherType.jsp">
		   								<jsp:param value="${i.index}" name="index"/>
		   								<jsp:param value="${workType.workType}" name="workType"/>
		   							</jsp:include>
		   						</c:when>
		   						<c:when test="${workType.workType == 'PLANTING'}">
		   							<jsp:include page="/WEB-INF/jsp/_include/formPartOtherType.jsp">
		   								<jsp:param value="${i.index}" name="index"/>
		   								<jsp:param value="${workType.workType}" name="workType"/>
		   							</jsp:include>
		   						</c:when> 
		   						<c:when test="${workType.workType == 'DISPOSAL'}">
		   							<fieldset id="${workType.workType}" class="theme-indent collapse">
			   							<c:forEach items="${disposalList}" var="disposalType" varStatus="j">
			   							    <c:set var="clazz" value="${j.index % 2 == 0 ? 'checkbox checkbox-col-one' : 'checkbox'}" />
			   								<div class="${clazz}">
			   									<spring-ext:checkbox path="workList[${i.index}].disposalList[${j.index}]" labelMessageKey="disposalType.${disposalType.disposal}" value="${disposalType}" />
			   								</div>
			   								<c:if test="${j.index % 2 == 1}"><br/></c:if>
			   							</c:forEach>
			   						</fieldset>	
		   						</c:when>
		   					</c:choose>
		   				</c:forEach>
		   				</fieldset>	
					</div>
				</div>
				</section>
				<fieldset>
					<legend>5. Termín, platba</legend>
					<spring-ext:formGroup path="term">
						<spring-ext:label path="term" cssClass="control-label col-sm-1" />
						<div class="col-sm-9">
							<c:forEach items="${termEnumList}" var="termType" varStatus="i">
   								<div class="radio radio-inline">
   									<spring-ext:radiobutton path="term" labelMessageKey="termType.${termType}" value="${termType}" />
   								</div>
   							</c:forEach>
						</div>
					</spring-ext:formGroup>
					<spring-ext:formGroup path="requiresBill">
						<spring-ext:label path="requiresBill" cssClass="control-label col-sm-1" />
						<div class="col-sm-9">
							<div class="radio radio-inline">
								<spring-ext:radiobutton path="requiresBill" labelMessageKey="requiresBill.no" value="false"  />
							</div>
							<div class="radio radio-inline">
								<spring-ext:radiobutton path="requiresBill" labelMessageKey="requiresBill.yes" value="true"  />
							</div>
						</div>
					</spring-ext:formGroup>
				</fieldset>
				<fieldset id="billing-data">
					<legend>6. Fakturační údaje</legend>	
					<spring-ext:formGroup path="vatPayer">
						<spring-ext:label path="vatPayer" cssClass="control-label col-sm-2" />
						<div class="col-sm-9">
							<div class="checkbox">
								<spring-ext:checkbox path="vatPayer" label="_empty" />
							</div>
						</div>
					</spring-ext:formGroup>
					<spring-ext:formGroup path="customer.billingData.address.name">
						<spring-ext:label path="customer.billingData.address.name" cssClass="control-label col-sm-2" />
						<div class="col-sm-3">
							<spring-ext:input path="customer.billingData.address.name" cssClass="form-control"/>
						</div>
					</spring-ext:formGroup>	
					<spring-ext:formGroup path="customer.billingData.address.street">
						<spring-ext:label path="customer.billingData.address.street" cssClass="control-label col-sm-2" />
						<div class="col-sm-3">
							<spring-ext:input path="customer.billingData.address.street" cssClass="form-control"/>
						</div>
					</spring-ext:formGroup>
					<spring-ext:formGroup path="customer.billingData.address.city">	
						<spring-ext:label path="customer.billingData.address.city" cssClass="control-label col-sm-2" />
						<div class="col-sm-3">
							<spring-ext:input path="customer.billingData.address.city" cssClass="form-control"/>
						</div>				
					</spring-ext:formGroup>
					<spring-ext:formGroup path="customer.billingData.address.zip">	
						<spring-ext:label path="customer.billingData.address.zip" cssClass="control-label col-sm-2" />
						<div class="col-sm-3">
							<spring-ext:input path="customer.billingData.address.zip" cssClass="form-control" />
						</div>
					</spring-ext:formGroup>
					<spring-ext:formGroup path="customer.billingData.regNo">	
						<spring-ext:label path="customer.billingData.regNo" cssClass="control-label col-sm-2" />
						<div class="col-sm-3">
							<spring-ext:input path="customer.billingData.regNo" cssClass="form-control"/>
						</div>
					</spring-ext:formGroup>
					<spring-ext:formGroup path="customer.billingData.vatNo">	
						<spring-ext:label path="customer.billingData.vatNo" cssClass="control-label col-sm-2" />
						<div class="col-sm-3">
							<spring-ext:input path="customer.billingData.vatNo" cssClass="form-control"/>
						</div>
					</spring-ext:formGroup>
				</fieldset>
				<form:hidden path="id" />
				<form:hidden path="version" />
				<form:hidden path="imageFolderId" />
				<p class="form-actions text-center">
					<input type="submit" value="Odeslat" class="btn btn-primary btn-lg" />
				</p>
				<p class="text-center"><small>Odesláním poptávky souhlásíte se <a href="<c:url value='/gdpr' />" target="_blank">zpracováním osobních údajů.</a></small></p>
			</form:form>
		</div>
	</tiles:putAttribute>
	<spring:eval expression="@environment['inquiryImgsFolderId']" var="rootFolderId" />
	<tiles:putAttribute name="javascript">
		<%-- ORDERED !! list of javascript files, a file may depend on previous file(s) --%>
		<asset:script url="/resources/libs/bootstrap-3.3.6/js/modal.js" />
		<asset:script url="/resources/js/app.js" isForProductionEnvironment="true" />
        <asset:script url="/resources/js/namespace.js" />
        <asset:script url="/resources/js/apputils/constants.js" />
		<asset:script url="/resources/js/utils/dto.js" />
		<asset:script url="/resources/js/gui/widgets.js" />
        <asset:script url="/resources/js/apputils/widgets-utils.js" />
        <asset:script url="/resources/js/app/google-picker-file-uploader.js" />
        <asset:script url="/resources/js/app/google-street-view-locator.js" />
        <asset:script url="/resources/js/app/inquiry.js?version=1"  />
        <script type="text/javascript">
   	 		APP.utils.Dto.params = {
   	 			accessToken: "${access_token}",
   	 			rootFolderId: "${rootFolderId}",
				loadingGif: "<c:url value='/resources/imgs/ajax-loader.gif' />",
				imageFolderIdField: $("[name=imageFolderId]")
   	 		}
   	 	</script>
   	 	<script type="text/javascript" src="https://apis.google.com/js/api.js?onload=APP_app_GooglePickerFileUploader_onGapiLoaded"></script>
		<script type="text/javascript" src="https://apis.google.com/js/client.js?onload=APP_app_GooglePickerFileUploader_onGclientLoaded"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7UYx2oC_P2zL4h8yU1_9Q9ngm0DfTKPg&&callback=APP.app.GoogleStreetViewLocator.mapsLoadedCallback" async defer></script>
	</tiles:putAttribute>
</tiles:insertDefinition>