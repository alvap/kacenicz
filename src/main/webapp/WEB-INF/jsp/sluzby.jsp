<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<c:set var="selectedItem" value="services" scope="request" />
<tiles:insertDefinition name=".layout.public.subpages">
	<tiles:putAttribute name="title">Služby - Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="headlineText" cascade="true">Služby</tiles:putAttribute>
	<tiles:putAttribute name="serviceContent" cascade="true">
		<div class="hidden-xs">
			<app:serviceHeadline text="Služby" />	
			<jsp:include page="/WEB-INF/jsp/_include/servicesIndex.jsp">
		    	<jsp:param value="h2" name="h" />
		    </jsp:include>
	    </div>
    </tiles:putAttribute>
</tiles:insertDefinition>