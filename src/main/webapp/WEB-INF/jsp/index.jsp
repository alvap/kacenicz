<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<tiles:insertDefinition name=".layout.public.homepage">
	<tiles:putAttribute name="title">Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<%-- perex --%>
		<section class="jumbotron">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-lg-10">
						<div class="perex-container">
							<h1>Rizikové kácení, údržba<br/> a ořezy stromů<br/>ŠARAPATKA</h1>
							<app:inquiry-btn />
						</div>
					</div>
				</div>
			</div>
		</section>
		<c:set var="headlineCols" value="col-lg-5" />
		<div class="container" id="intro">
			<section class="theme">
			    <div class="row">
			    	<div class="${headlineCols}">
			    		<h2>Rizikové kácení,<br/>ořezy a údržba stromů</h2>
			    	</div>
			    </div>
			    <div class="row">
			    	<div class="col-sm-12">
			    		<p class="text-justify perex">Firma ŠARAPATKA působí na českém trhu <b>od roku 1996</b>. Podnikáme v oboru realizace a údržba zeleně. Naší specializací se stala ARBORISTIKA - péče o vzrostlé dřeviny - kácení stromů, údržba a ořezy stromů. Zaměřujeme se na údržbu dřevin rostoucích v obytných zónách měst a obcí, včetně parků, zámeckých zahrad a památkově chráněných objektů.</p>
			    	</div>
			    </div>
	    	</section>
	    	<section>
			    <div class="row">
			    	<div class="${headlineCols}">
			    		<h2>Potřebuji zařídit</h2>
			    	</div>
			    </div>
			    <jsp:include page="/WEB-INF/jsp/_include/servicesIndex.jsp">
			    	<jsp:param value="h3" name="h"/>
			    </jsp:include>
	    	</section>
		 </div>   
	</tiles:putAttribute>
</tiles:insertDefinition>