<%@tag description="prints lesson start and current occupancy in public schedule display (used when regular places are available)" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<%@attribute name="cssClass" %>
<a href="<c:url value='/poptavka' />"  class="btn btn-primary btn-lg ${cssClass}" id="inquiry-btn" title="Kácení Šarapatka - rizikové kácení a ořezy stromů, údržba zeleně - poptávka">
	<span class="btn-icon"><i class="fa fa-tree"></i></span>
	<span class="btn-text">On.line poptávka</span>
</a>

