<%@tag description="prints lesson start and current occupancy in public schedule display (used when regular places are available)" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<%@attribute name="h" %>
<c:set var="serviceCols" value="col-sm-6 col-md-4" />
<div class="row theme-inverse" id="services-wrapper">
	<div class="${serviceCols}">
		<${h} id="odstranit">
			<a href="<c:url value='/odstranit-strom' />" title="Kácení Šarapatka - odstranění stromu"><span></span></a>
			<a href="<c:url value='/odstranit-strom' />" title="Kácení Šarapatka - odstranění stromu" class="link-text">Odstranit strom</a>
		</${h}>
	</div>
	<div class="${serviceCols}">
		<${h} id="vysadit">
			<a href="<c:url value='/vysadit-strom' />" title="Kácení Šarapatka - vysazení stromu"><span></span></a>
			<a href="<c:url value='/vysadit-strom' />" title="Kácení Šarapatka - vysazení stromu" class="link-text">Vysadit strom</a>
		</${h}>
	</div>
	<div class="${serviceCols}">
		<${h} id="orezat">
			<a href="<c:url value='/orezat-strom' />" title="Kácení Šarapatka - ořez stromu"><span></span></a>
			<a href="<c:url value='/orezat-strom' />" title="Kácení Šarapatka - ořez stromu" class="link-text">Ořezat strom</a>
		</${h}>
	</div>
	<div class="${serviceCols}">
		<${h} id="zabezpecit">
			<a href="<c:url value='/zabezpecit-strom' />" title="Kácení Šarapatka - zabezpečení stromu"><span></span></a>
			<a href="<c:url value='/zabezpecit-strom' />" title="Kácení Šarapatka - zabezpečení stromu" class="link-text">Zabezpečit strom</a>
		</${h}>
	</div>
	<div class="${serviceCols}">
		<${h} id="zhodnotit">
			<a href="<c:url value='/zabezpecit-strom' />" title="Kácení Šarapatka - zhodnocení stromu"><span></span></a>
			<a href="<c:url value='/zabezpecit-strom' />" title="Kácení Šarapatka - zhodnocení stromu" class="link-text">Zhodnotit strom</a>
		</${h}>
	</div>
	<div class="${serviceCols}">
		<${h} id="skoleni">
		<a href="<c:url value='/skoleni-a-vzdelavani' />" title="Kácení Šarapatka - školení a vzdělávání"><span></span></a>
		<a href="<c:url value='/skoleni-a-vzdelavani' />" title="Kácení Šarapatka - školení a vzdělávání" class="link-text">Školení a vzdělávání</a>
		</${h}>
	</div>
</div>
	