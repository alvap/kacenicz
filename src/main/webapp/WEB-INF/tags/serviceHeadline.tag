<%@tag description="prints lesson start and current occupancy in public schedule display (used when regular places are available)" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/_include/taglibs.jsp" %>
<%@attribute name="text" required="true" %>
<div class="row">
	<div class="col-lg-7">
		<h1 class="theme">${text}</h1>
	</div>
</div>	
